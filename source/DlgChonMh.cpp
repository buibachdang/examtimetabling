// DlgChonMh.cpp : implementation file
//

#include "stdafx.h"
#include "XepLichThi.h"
#include "DlgChonMh.h"
#include "khaibao.h"
#include "DlgChonTietUT.h"
#include "Xeplt.h"
#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

//extern int gUTongsoMon;
//extern CUMH UMon;
extern CXeplt Xeplt;
/////////////////////////////////////////////////////////////////////////////
// CDlgChonMh dialog


CDlgChonMh::CDlgChonMh(CWnd* pParent /*=NULL*/)
	: CBitmapDialog(CDlgChonMh::IDD, pParent)
{
	//{{AFX_DATA_INIT(CDlgChonMh)
	//}}AFX_DATA_INIT
}


void CDlgChonMh::DoDataExchange(CDataExchange* pDX)
{
	CBitmapDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDlgChonMh)
	DDX_Control(pDX, IDC_LIST_MON2, m_ctlMonList2);
	DDX_Control(pDX, IDC_LIST_MON1, m_ctlMonList1);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CDlgChonMh, CBitmapDialog)
	//{{AFX_MSG_MAP(CDlgChonMh)
	ON_BN_CLICKED(IDC_BUTTON_QUAPHAI, OnButtonQuaphai)
	ON_BN_CLICKED(IDC_BUTTON_QUATRAI, OnButtonQuatrai)
	ON_BN_CLICKED(IDC_BUTTON_CHITIET2, OnButtonChitiet2)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDlgChonMh message handlers

BOOL CDlgChonMh::OnInitDialog() 
{
	CBitmapDialog::OnInitDialog();	
	SetBitmap(IDB_BITMAP1,BITMAP_TILE);	
	
	for (int i=0; i<Xeplt.gUTongsoMon; i++)	
	{
		if ((Xeplt.UMon[i].khoahoc==khoahoc)&&(Xeplt.UMon[i].tietbd==-1))
		{
			m_ctlMonList1.AddString(Xeplt.UMon[i].mamon);			
		}
	}	
	for (i=0; i<Xeplt.gUTongsoMon; i++)	
	{
		if ((Xeplt.UMon[i].khoahoc==khoahoc)&&(Xeplt.UMon[i].tietbd==0))
		{
			m_ctlMonList2.AddString(Xeplt.UMon[i].mamon);			
		}
	}	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CDlgChonMh::OnButtonQuaphai() 
{
	int count = m_ctlMonList1.GetSelCount();
	CArray<int,int> arrayListSel;
	arrayListSel.SetSize(count);						// make room in array
	m_ctlMonList1.GetSelItems(count, arrayListSel.GetData());	// copy data to array

	for( int i=0; i<count; i++ )
	{
		CString tmp = _T("");
		m_ctlMonList1.GetText( arrayListSel[i], tmp );
		m_ctlMonList2.AddString(tmp); 
		
	}

	for (i=0;i<count;i++)
	{
		m_ctlMonList1.DeleteString (arrayListSel[i]);
		for (int j=i;j<count;j++)
			arrayListSel[j]--;
	}			
}

void CDlgChonMh::OnButtonQuatrai() 
{
	int count = m_ctlMonList2.GetSelCount();
	CArray<int,int> arrayListSel;
	arrayListSel.SetSize(count);						// make room in array
	m_ctlMonList2.GetSelItems(count, arrayListSel.GetData());	// copy data to array
	for( int i=0; i<count; i++ )
	{
		CString tmp = _T("");
		m_ctlMonList2.GetText( arrayListSel[i], tmp );
		m_ctlMonList1.AddString(tmp);
	}
	for (i=0;i<count;i++)
	{
		m_ctlMonList2.DeleteString (arrayListSel[i]);
		for (int j=i;j<count;j++)
			arrayListSel[j]--;
	}		
}

void CDlgChonMh::OnOK() 
{
	sopt2=m_ctlMonList2.GetCount();
	sopt1=m_ctlMonList1.GetCount();
	
	for (int i=0;i<sopt2;i++)	
	{
		CString str;
		m_ctlMonList2.GetText (i,str);
		for (int j=0;j<Xeplt.gUTongsoMon;j++)
		{
			if (Xeplt.UMon[j].mamon ==str) 
			{
				Xeplt.UMon[j].tietbd =0;
				break;
			}
		}		

	}
	for ( i=0;i<sopt1;i++)	
	{
		CString str;
		m_ctlMonList1.GetText (i,str);
		for (int j=0;j<Xeplt.gUTongsoMon;j++)
		{
			if (Xeplt.UMon[j].mamon ==str) 
			{
				Xeplt.UMon[j].tietbd =-1;
				break;
			}
		}
	}	
	

	
	CBitmapDialog::OnOK();
}

void CDlgChonMh::OnButtonChitiet2() 
{
	CDlgChonTietUT dlgChonTietUT;
	int iCursel=m_ctlMonList2.GetCurSel();
	if (iCursel==LB_ERR) MessageBox("sai");
	m_ctlMonList2.GetText(iCursel,dlgChonTietUT.mamon);
	dlgChonTietUT.DoModal();

	
}
