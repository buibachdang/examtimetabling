#if !defined(AFX_SETDAYNHA_H__56D3BDC4_3AEA_49C9_998C_6EB219089C4F__INCLUDED_)
#define AFX_SETDAYNHA_H__56D3BDC4_3AEA_49C9_998C_6EB219089C4F__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// SetDaynha.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CSetDaynha DAO recordset

class CSetDaynha : public CDaoRecordset
{
public:
	CSetDaynha(CDaoDatabase* pDatabase = NULL);
	DECLARE_DYNAMIC(CSetDaynha)

// Field/Param Data
	//{{AFX_FIELD(CSetDaynha, CDaoRecordset)
	CString	m_Daynha;
	short	m_Sophong;
	short	m_Succhuaday;
	//}}AFX_FIELD

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSetDaynha)
	public:
	virtual CString GetDefaultDBName();		// Default database name
	virtual CString GetDefaultSQL();		// Default SQL for Recordset
	virtual void DoFieldExchange(CDaoFieldExchange* pFX);  // RFX support
	//}}AFX_VIRTUAL

// Implementation
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SETDAYNHA_H__56D3BDC4_3AEA_49C9_998C_6EB219089C4F__INCLUDED_)
