#if !defined(AFX_SETBANGDUNGDO_H__D6D1BB0F_ADE9_4CBA_B266_3CD5F6DA699B__INCLUDED_)
#define AFX_SETBANGDUNGDO_H__D6D1BB0F_ADE9_4CBA_B266_3CD5F6DA699B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// SetBangdungdo.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CSetBangdungdo DAO recordset

class CSetBangdungdo : public CDaoRecordset
{
public:
	CSetBangdungdo(CDaoDatabase* pDatabase = NULL);
	DECLARE_DYNAMIC(CSetBangdungdo)

// Field/Param Data
	//{{AFX_FIELD(CSetBangdungdo, CDaoRecordset)
	CString	m_MaMon1;
	CString	m_MaMon2;
	long	m_SoDungDo;
	//}}AFX_FIELD

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSetBangdungdo)
	public:
	virtual CString GetDefaultDBName();		// Default database name
	virtual CString GetDefaultSQL();		// Default SQL for Recordset
	virtual void DoFieldExchange(CDaoFieldExchange* pFX);  // RFX support
	//}}AFX_VIRTUAL

// Implementation
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SETBANGDUNGDO_H__D6D1BB0F_ADE9_4CBA_B266_3CD5F6DA699B__INCLUDED_)
