#if !defined(AFX_SETKHOAHOC_H__3B30EBBD_22CE_45CD_B2EF_0A0D9FF09958__INCLUDED_)
#define AFX_SETKHOAHOC_H__3B30EBBD_22CE_45CD_B2EF_0A0D9FF09958__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// SetKhoahoc.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CSetKhoahoc DAO recordset

class CSetKhoahoc : public CDaoRecordset
{
public:
	CSetKhoahoc(CDaoDatabase* pDatabase = NULL);
	DECLARE_DYNAMIC(CSetKhoahoc)

// Field/Param Data
	//{{AFX_FIELD(CSetKhoahoc, CDaoRecordset)
	CString	m_Khoahoc;
	//}}AFX_FIELD

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSetKhoahoc)
	public:
	virtual CString GetDefaultDBName();		// Default database name
	virtual CString GetDefaultSQL();		// Default SQL for Recordset
	virtual void DoFieldExchange(CDaoFieldExchange* pFX);  // RFX support
	//}}AFX_VIRTUAL

// Implementation
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SETKHOAHOC_H__3B30EBBD_22CE_45CD_B2EF_0A0D9FF09958__INCLUDED_)
