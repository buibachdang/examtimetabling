// XepLichThi.h : main header file for the XEPLICHTHI application
//

#if !defined(AFX_XEPLICHTHI_H__472C6F12_6042_42A8_864E_58F463DEDD8B__INCLUDED_)
#define AFX_XEPLICHTHI_H__472C6F12_6042_42A8_864E_58F463DEDD8B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"       // main symbols

/////////////////////////////////////////////////////////////////////////////
// CXepLichThiApp:
// See XepLichThi.cpp for the implementation of this class
//

class CXepLichThiApp : public CWinApp
{
public:
	CXepLichThiApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CXepLichThiApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation
	//{{AFX_MSG(CXepLichThiApp)
	afx_msg void OnAppAbout();
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_XEPLICHTHI_H__472C6F12_6042_42A8_864E_58F463DEDD8B__INCLUDED_)
