// XepLichThiView.h : interface of the CXepLichThiView class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_XEPLICHTHIVIEW_H__DC8E7E0C_878E_4487_BBE1_922AB09D5E49__INCLUDED_)
#define AFX_XEPLICHTHIVIEW_H__DC8E7E0C_878E_4487_BBE1_922AB09D5E49__INCLUDED_

#include "SetBangdungdo.h"	// Added by ClassView
#include "SetKhoahoc.h"	// Added by ClassView
#include "SetSotay.h"	// Added by ClassView
#include "SetPhong.h"	// Added by ClassView
#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
#ifndef _MEMDC_H_ 
#define _MEMDC_H_ 
////////////////////////////////////////////////// 
// CMemDC - memory DC 
// 
// Author: Keith Rule 
// Email:  keithr@europa.com 
// Copyright 1996-1997, Keith Rule 
// 
// You may freely use or modify this code provided this 
// Copyright is included in all derived versions. 
// 
// History - 10/3/97 Fixed scrolling bug. 
//                   Added print support. 
//	 
// This class implements a memory Device Context  
 
class CMemDC : public CDC { 
private: 
	CBitmap m_bitmap; // Offscreen bitmap 
	CBitmap* m_oldBitmap; // bitmap originally found in CMemDC 
	CDC* m_pDC; // Saves CDC passed in constructor 
	CRect m_rect; // Rectangle of drawing area. 
	BOOL m_bMemDC; // TRUE if CDC really is a Memory DC. 
public: 
	CMemDC(CDC* pDC, CRect &rect) : CDC(), m_oldBitmap(NULL), m_pDC(pDC) 
	{ 
		ASSERT(m_pDC != NULL); // If you asserted here, you passed in a NULL CDC. 
		m_bMemDC = !pDC->IsPrinting(); 
		m_rect=rect; 
		if (m_bMemDC){ 
			// Create a Memory DC 
			CreateCompatibleDC(pDC); 
			pDC->GetClipBox(&m_rect); 
			m_bitmap.CreateCompatibleBitmap(pDC, m_rect.Width(), m_rect.Height()); 
			m_oldBitmap = SelectObject(&m_bitmap); 
			SetWindowOrg(m_rect.left, m_rect.top); 
		} else { 
			// Make a copy of the relevent parts of the current DC for printing 
			m_bPrinting = pDC->m_bPrinting; 
			m_hDC = pDC->m_hDC; 
			m_hAttribDC = pDC->m_hAttribDC; 
		} 
	} 
 
	 
	~CMemDC() 
	{ 
		if (m_bMemDC) { 
			// Copy the offscreen bitmap onto the screen. 
			m_pDC->BitBlt(m_rect.left, m_rect.top, m_rect.Width(), m_rect.Height(), 
				this, m_rect.left, m_rect.top, SRCCOPY); 
			//Swap back the original bitmap. 
			SelectObject(m_oldBitmap); 
		} else { 
			// All we need to do is replace the DC with an illegal value, 
			// this keeps us from accidently deleting the handles associated with 
			// the CDC that was passed to the constructor. 
			m_hDC = m_hAttribDC = NULL; 
		} 
	} 
	// Allow usage as a pointer 
	CMemDC* operator->() {return this;} 
	// Allow usage as a pointer 
	operator CMemDC*() {return this;} 
}; 
 
 
#endif 


class CXepLichThiView : public CFormView
{
protected: // create from serialization only
	CXepLichThiView();
	DECLARE_DYNCREATE(CXepLichThiView)

public:
	CBitmapButton b_Ok;
	CPalette m_palette; 
	CBitmap m_bitmap;
	//{{AFX_DATA(CXepLichThiView)
	enum { IDD = IDD_XEPLICHTHI_FORM };
	CEdit	m_Hthi;
	CCheckListBox	m_ctlKhoahocList;
	CString	m_strDuongdan;
	CString	m_engaybatdau;
	CString	m_engayketthuc;
	//}}AFX_DATA

// Attributes
public:
	CXepLichThiDoc* GetDocument();

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CXepLichThiView)
	public:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual void OnInitialUpdate(); // called first time after construct
	virtual BOOL OnPreparePrinting(CPrintInfo* pInfo);
	virtual void OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnEndPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnPrint(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnDraw(CDC* pDC);
	//}}AFX_VIRTUAL

// Implementation
public:	
	
	int m_iMaxIndex;
	int m_nStrIndex;
	CFont m_font1;
	int m_ichieucaochu;
	void OnChay();
	void OnButtonKhoidong();
	void DrawTheBackground(CView *view, CDC *pDC, CPalette *mp_palette, CBitmap *mp_bitmap);
	BOOL GetBitmapAndPalette(UINT nIDResource, CBitmap &bitmap, CPalette &pal);
	int * checkkhoa;
	CSetBangdungdo m_Dungdo;
	CSetPhong m_Phong;
	CSetSotay m_Sotay;
	CSetKhoahoc m_Khoahoc;
	virtual ~CXepLichThiView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	void Lapbangdungdo();
	//{{AFX_MSG(CXepLichThiView)
	afx_msg void OnButtonChay();
	afx_msg void OnButtonChitiet();
	afx_msg void OnButtonDuongdan();
	afx_msg void OnSelchangeListChonkhoa();
	afx_msg void OnButtonNhaptietban();
	afx_msg void OnButtonNhapthoigian();
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	afx_msg void OnButtonPtchay();
	afx_msg void OnButtonReset();
	afx_msg void OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg void OnChar(UINT nChar, UINT nRepCnt, UINT nFlags);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

#ifndef _DEBUG  // debug version in XepLichThiView.cpp
inline CXepLichThiDoc* CXepLichThiView::GetDocument()
   { return (CXepLichThiDoc*)m_pDocument; }
#endif

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_XEPLICHTHIVIEW_H__DC8E7E0C_878E_4487_BBE1_922AB09D5E49__INCLUDED_)
