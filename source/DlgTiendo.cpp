// DlgTiendo.cpp : implementation file
//

#include "stdafx.h"
#include "xeplichthi.h"
#include "DlgTiendo.h"
#include "Xeplt.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CDlgTiendo dialog

extern CXeplt Xeplt;
CDlgTiendo::CDlgTiendo(CWnd* pParent /*=NULL*/)
	: CBitmapDialog(CDlgTiendo::IDD, pParent)
{
	//{{AFX_DATA_INIT(CDlgTiendo)
	strMon = _T("");
	strDem = _T("");
	//}}AFX_DATA_INIT
}


void CDlgTiendo::DoDataExchange(CDataExchange* pDX)
{
	CBitmapDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDlgTiendo)
	DDX_Control(pDX, IDC_STATIC_STATUS, c_Status);
	DDX_Control(pDX, IDC_STATIC_T, c_T);
	DDX_Control(pDX, IDC_STATIC_M, c_M);
	DDX_Control(pDX, IDC_STATIC_MON, c_Mon);
	DDX_Control(pDX, IDC_STATIC_DEM, c_Dem);
	DDX_Control(pDX, IDC_PROGRESS, c_Tiendo);
	DDX_Text(pDX, IDC_STATIC_MON, strMon);
	DDX_Text(pDX, IDC_STATIC_DEM, strDem);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CDlgTiendo, CBitmapDialog)
	//{{AFX_MSG_MAP(CDlgTiendo)
	ON_WM_CHAR()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDlgTiendo message handlers

BOOL CDlgTiendo::OnInitDialog() 
{
	CBitmapDialog::OnInitDialog();	
	SetBitmap(IDB_BITMAP1,BITMAP_TILE);
	//SetBitmap(IDB_BITMAP1);
	//SetBitmapStyle(StyleTile);
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CDlgTiendo::OnChar(UINT nChar, UINT nRepCnt, UINT nFlags) 
{
	
	CBitmapDialog::OnChar(nChar, nRepCnt, nFlags);
}
