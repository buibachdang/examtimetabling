// DlgChonTietUT.cpp : implementation file
//

#include "stdafx.h"
#include "XepLichThi.h"
#include "DlgChonTietUT.h"
#include "Xeplt.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CDlgChonTietUT dialog
const int TD=0;
const int CD=1;
extern CXeplt Xeplt;
CDlgChonTietUT::CDlgChonTietUT(CWnd* pParent /*=NULL*/)
	: CBitmapDialog(CDlgChonTietUT::IDD, pParent)
{
	//{{AFX_DATA_INIT(CDlgChonTietUT)
	m_strMamon = _T("");
	m_bd = 0;
	m_cd = 0;
	m_kt = 0;
	m_iTiet = 0;
	str_chon = _T("");
	bRangbuoc = FALSE;
	//}}AFX_DATA_INIT
}


void CDlgChonTietUT::DoDataExchange(CDataExchange* pDX)
{
	CBitmapDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDlgChonTietUT)
	DDX_Control(pDX, IDC_DATETIMEPICKER_NGAYKT, c_kt);
	DDX_Control(pDX, IDC_DATETIMEPICKER_NGAYCD, c_cd);
	DDX_Control(pDX, IDC_DATETIMEPICKER_NGAYBD, c_bd);
	DDX_Control(pDX, IDC_RADIO_NGAYCD, bt_ngaycd);
	DDX_Control(pDX, IDC_RADIO_NGAYTD, bt_ngaytd);
	DDX_Control(pDX, IDC_STATIC_CHON, m_ctlchon);
	DDX_Text(pDX, IDC_STATIC_MAMON, m_strMamon);
	DDX_DateTimeCtrl(pDX, IDC_DATETIMEPICKER_NGAYBD, m_bd);
	DDX_DateTimeCtrl(pDX, IDC_DATETIMEPICKER_NGAYCD, m_cd);
	DDX_DateTimeCtrl(pDX, IDC_DATETIMEPICKER_NGAYKT, m_kt);
	DDX_Text(pDX, IDC_EDIT_TIETBATDAU, m_iTiet);
	DDX_Text(pDX, IDC_STATIC_CHONUT, str_chon);
	DDX_Check(pDX, IDC_CHECK_RANGBUOC, bRangbuoc);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CDlgChonTietUT, CBitmapDialog)
	//{{AFX_MSG_MAP(CDlgChonTietUT)
	ON_BN_CLICKED(IDC_RADIO_NGAYTD, OnRadioNgaytd)
	ON_BN_CLICKED(IDC_RADIO_NGAYCD, OnRadioNgaycd)
	ON_NOTIFY(DTN_CLOSEUP, IDC_DATETIMEPICKER_NGAYBD, OnCloseupDatetimepickerNgaybd)
	ON_NOTIFY(DTN_CLOSEUP, IDC_DATETIMEPICKER_NGAYKT, OnCloseupDatetimepickerNgaykt)
	ON_NOTIFY(DTN_CLOSEUP, IDC_DATETIMEPICKER_NGAYCD, OnCloseupDatetimepickerNgaycd)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDlgChonTietUT message handlers

BOOL CDlgChonTietUT::OnInitDialog() 
{
	
	CBitmapDialog::OnInitDialog();
	SetBitmap(IDB_BITMAP1,BITMAP_TILE);
	//CString strtam;
	//strtam.Format("Chon thoi gian uu tien cho mon %s ", mamon);
	//m_ctlchon.SetWindowText(strtam);
	str_chon.Format("Ch�n th�i gian �u ti�n cho m�n :%s ", mamon);
	UpdateData(FALSE);
	bflag=TD;
	bt_ngaytd.SetCheck(1);
	//Set picker
	c_cd.SetTime(&Xeplt.ngaybatdau); 
	c_bd.SetTime(&Xeplt.ngaybatdau);
	c_kt.SetTime(&Xeplt.ngayketthuc);
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CDlgChonTietUT::OnRadioNgaytd() 
{
	bflag=TD;	
}

void CDlgChonTietUT::OnRadioNgaycd() 
{
	bflag=CD;
}

void CDlgChonTietUT::OnOK() 
{
	int songay,vttiet;
	CTimeSpan tsongay;
	int mon=Xeplt.Maraso(UMON,mamon);
	if (bflag==CD)	
	{
		UpdateData(TRUE);
//	
		//int y=m_iTiet;	
		if ((m_iTiet<1)||(m_iTiet>12))
		{
			AfxMessageBox("Nhap tiet bat dau lai !!!");
			goto END;
		}
		tsongay=m_cd-Xeplt.ngaybatdau;
		songay=atoi(tsongay.Format("%D"));
		vttiet=songay*12+m_iTiet-1;
		if (Xeplt.Tietthi[vttiet].ban==FALSE)
		{
			
			Xeplt.UMon[mon].loai=LOAI_NCD;
			Xeplt.UMon[mon].icd=vttiet;			
		}
		else
		{
			AfxMessageBox("Ban da chon uu tien vao tiet ban !!! Vui long chon lai");
			goto END;
		}
	
	}
	else
	{
		UpdateData(TRUE);
//	
		//int y=m_iTiet;		
		Xeplt.UMon[mon].loai=LOAI_NTD;
		CTimeSpan timebd=m_bd-Xeplt.ngaybatdau;
		int songaybd=atoi(timebd.Format("%D"));
		CTimeSpan timekt=m_kt-Xeplt.ngaybatdau;
		int songaykt=atoi(timekt.Format("%D"));		
		Xeplt.UMon[mon].ibd=songaybd*12;
		Xeplt.UMon[mon].ikt=songaykt*12;;
		
	}
	Xeplt.UMon[mon].loairb=!bRangbuoc;
	CBitmapDialog::OnOK();
END:	int k=0; //dummy
}

BOOL CDlgChonTietUT::Create(LPCTSTR lpszClassName, LPCTSTR lpszWindowName, DWORD dwStyle, const RECT& rect, CWnd* pParentWnd, UINT nID, CCreateContext* pContext) 
{
	// TODO: Add your specialized code here and/or call the base class
	

	
	return CWnd::Create(lpszClassName, lpszWindowName, dwStyle, rect, pParentWnd, nID, pContext);
}

void CDlgChonTietUT::OnCloseupDatetimepickerNgaybd(NMHDR* pNMHDR, LRESULT* pResult) 
{
	CTime batdau;
	c_bd.GetTime(batdau);
	if ((batdau<Xeplt.ngaybatdau)||(batdau>Xeplt.ngayketthuc))
		c_bd.SetTime(&Xeplt.ngaybatdau);
	*pResult = 0;
}

void CDlgChonTietUT::OnCloseupDatetimepickerNgaykt(NMHDR* pNMHDR, LRESULT* pResult) 
{
	CTime ketthuc;
	CTime batdau;	
	c_kt.GetTime(ketthuc);
	c_bd.GetTime(batdau);
	if ((ketthuc<Xeplt.ngaybatdau)||(ketthuc>Xeplt.ngayketthuc)||(ketthuc<batdau))
		c_kt.SetTime(&Xeplt.ngayketthuc);
	
	*pResult = 0;
}

void CDlgChonTietUT::OnCloseupDatetimepickerNgaycd(NMHDR* pNMHDR, LRESULT* pResult) 
{
	CTime codinh;
	c_cd.GetTime(codinh);
	if ((codinh<Xeplt.ngaybatdau)||(codinh>Xeplt.ngayketthuc))
		c_cd.SetTime(&Xeplt.ngaybatdau);
	*pResult = 0;
}
