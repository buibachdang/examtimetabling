// SetNhom.cpp : implementation file
//

#include "stdafx.h"
#include "xeplichthi.h"
#include "SetNhom.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CSetNhom
extern CString dgdan;
IMPLEMENT_DYNAMIC(CSetNhom, CDaoRecordset)

CSetNhom::CSetNhom(CDaoDatabase* pdb)
	: CDaoRecordset(pdb)
{
	//{{AFX_FIELD_INIT(CSetNhom)
	m_Mamonhoc = _T("");
	m_Nhom = _T("");
	m_To = _T("");
	m_Soluong = 0;
	m_nFields = 4;
	//}}AFX_FIELD_INIT
	m_nDefaultType = dbOpenDynaset;
}


CString CSetNhom::GetDefaultDBName()
{
	return dgdan;
}

CString CSetNhom::GetDefaultSQL()
{
	return _T("[LayNhomhoc]");
}

void CSetNhom::DoFieldExchange(CDaoFieldExchange* pFX)
{
	//{{AFX_FIELD_MAP(CSetNhom)
	pFX->SetFieldType(CDaoFieldExchange::outputColumn);
	DFX_Text(pFX, _T("[Mamonhoc]"), m_Mamonhoc);
	DFX_Text(pFX, _T("[Nhom]"), m_Nhom);
	DFX_Text(pFX, _T("[To]"), m_To);
	DFX_Long(pFX, _T("[Soluong]"), m_Soluong);
	//}}AFX_FIELD_MAP
}

/////////////////////////////////////////////////////////////////////////////
// CSetNhom diagnostics

#ifdef _DEBUG
void CSetNhom::AssertValid() const
{
	CDaoRecordset::AssertValid();
}

void CSetNhom::Dump(CDumpContext& dc) const
{
	CDaoRecordset::Dump(dc);
}
#endif //_DEBUG
