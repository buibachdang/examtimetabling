// SetPhong.cpp : implementation file
//

#include "stdafx.h"
#include "xeplichthi.h"
#include "SetPhong.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CSetPhong
extern CString dgdan;
IMPLEMENT_DYNAMIC(CSetPhong, CDaoRecordset)

CSetPhong::CSetPhong(CDaoDatabase* pdb)
	: CDaoRecordset(pdb)
{
	//{{AFX_FIELD_INIT(CSetPhong)
	m_Maphong = _T("");
	m_Daynha = _T("");
	m_Succhua = 0;	
	m_nFields = 3;
	//}}AFX_FIELD_INIT
	m_nDefaultType = dbOpenDynaset;
}


CString CSetPhong::GetDefaultDBName()
{
	return dgdan;
}

CString CSetPhong::GetDefaultSQL()
{
	return _T("[Phonghoc]");
}

void CSetPhong::DoFieldExchange(CDaoFieldExchange* pFX)
{
	//{{AFX_FIELD_MAP(CSetPhong)
	pFX->SetFieldType(CDaoFieldExchange::outputColumn);	
	DFX_Text(pFX, _T("[Maphong]"), m_Maphong);
	DFX_Text(pFX, _T("[Daynha]"), m_Daynha);
	DFX_Short(pFX, _T("[Succhua]"), m_Succhua);
	//}}AFX_FIELD_MAP
}

/////////////////////////////////////////////////////////////////////////////
// CSetPhong diagnostics

#ifdef _DEBUG
void CSetPhong::AssertValid() const
{
	CDaoRecordset::AssertValid();
}

void CSetPhong::Dump(CDumpContext& dc) const
{
	CDaoRecordset::Dump(dc);
}
#endif //_DEBUG
