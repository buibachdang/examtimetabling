#if !defined(AFX_SETSOTAY_H__3A9D7699_DDD9_4A3E_AC30_56FDCE0B4679__INCLUDED_)
#define AFX_SETSOTAY_H__3A9D7699_DDD9_4A3E_AC30_56FDCE0B4679__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// SetSotay.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CSetSotay DAO recordset

class CSetSotay : public CDaoRecordset
{
public:
	CSetSotay(CDaoDatabase* pDatabase = NULL);
	DECLARE_DYNAMIC(CSetSotay)

// Field/Param Data
	//{{AFX_FIELD(CSetSotay, CDaoRecordset)
	CString	m_Mamonhoc;
	short	m_Dept;
	CString	m_Khoahoc;
	short	m_Sotietthi;
	short	m_Sosvdk;
	//}}AFX_FIELD

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSetSotay)
	public:
	virtual CString GetDefaultDBName();		// Default database name
	virtual CString GetDefaultSQL();		// Default SQL for Recordset
	virtual void DoFieldExchange(CDaoFieldExchange* pFX);  // RFX support
	//}}AFX_VIRTUAL

// Implementation
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SETSOTAY_H__3A9D7699_DDD9_4A3E_AC30_56FDCE0B4679__INCLUDED_)
