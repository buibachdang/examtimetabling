// DlgCanhbao.cpp : implementation file
//

#include "stdafx.h"
#include "xeplichthi.h"
#include "DlgCanhbao.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CDlgCanhbao dialog


CDlgCanhbao::CDlgCanhbao(CWnd* pParent /*=NULL*/)
	: CDialog(CDlgCanhbao::IDD, pParent)
{
	//{{AFX_DATA_INIT(CDlgCanhbao)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void CDlgCanhbao::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDlgCanhbao)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CDlgCanhbao, CDialog)
	//{{AFX_MSG_MAP(CDlgCanhbao)
		// NOTE: the ClassWizard will add message map macros here
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDlgCanhbao message handlers
