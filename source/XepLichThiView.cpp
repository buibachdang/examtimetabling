// XepLichThiView.cpp : implementation of the CXepLichThiView class
//

#include "stdafx.h"
#include "XepLichThi.h"
#include "DlgPhongban.h"
#include "DlgNhapthoigian.h"

#include "XepLichThiDoc.h"
#include "XepLichThiView.h"
#include "DlgGioithieu.h"
#include "DlgChonMh.h"
#include "khaibao.h"
#include "Xeplt.h"
#include "SetPhong.h"
#include "SetDaynha.h"
#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Bien Global

//	const DWORD BELOW_NORMAL_PRIORITY_CLASS = 4;
//	const int TSMON = 324;
//	const int TSTIET = 144;	
//	const int MAXNHOM = 50;	
	const int HSPHAT = 100; //mon bi phat nhieu nhat la 30000 diem  do mau thuan toi da 300 mon khac
	const int MAXDIEM = 60000; //mon dung do  nhieu nhat la 22000 dung do
	void debugMonchon();
//	int DBug[TSMON][TSTIET];
//	CMh MhocBug[TSMON];
//	CString TenNhomBug[TSMON][MAXNHOM];
//	int LuuVTBug[TSMON];
//	int TrangthaiBug[TSMON][TSMON][TSTIET];
//	int DSttxetBug[TSMON];
//	int stagebug=0,dembug=-1,sofwbug=0,wbug=0;
	int Monchonbug[324];

CXeplt Xeplt;
CString dgdan;
bool bflagKhoidong=FALSE;
bool bflagPhongban=FALSE;





/////////////////////////////////////////////////////////////////////////////
// CXepLichThiView

IMPLEMENT_DYNCREATE(CXepLichThiView, CFormView)

BEGIN_MESSAGE_MAP(CXepLichThiView, CFormView)
	//{{AFX_MSG_MAP(CXepLichThiView)
	ON_BN_CLICKED(IDC_BUTTON_CHAY, OnButtonChay)
	ON_BN_CLICKED(IDC_BUTTON_CHITIET, OnButtonChitiet)
	ON_BN_CLICKED(IDC_BUTTON_DUONGDAN, OnButtonDuongdan)
	ON_LBN_SELCHANGE(IDC_LIST_CHONKHOA, OnSelchangeListChonkhoa)
	ON_BN_CLICKED(IDC_BUTTON_NHAPTIETBAN, OnButtonNhaptietban)
	ON_BN_CLICKED(IDC_BUTTON_NHAPTHOIGIAN, OnButtonNhapthoigian)
	ON_WM_CTLCOLOR()
	ON_BN_CLICKED(IDC_BUTTON_PTCHAY, OnButtonPtchay)
	ON_BN_CLICKED(IDC_BUTTON_RESET, OnButtonReset)
	ON_WM_KEYDOWN()
	ON_COMMAND ( 1, OnChay )
	ON_WM_CHAR()
	//}}AFX_MSG_MAP
	// Standard printing commands
	ON_COMMAND(ID_FILE_PRINT, CFormView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_DIRECT, CFormView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, CFormView::OnFilePrintPreview)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CXepLichThiView construction/destruction

CXepLichThiView::CXepLichThiView()
	: CFormView(CXepLichThiView::IDD)
{
	//{{AFX_DATA_INIT(CXepLichThiView)
	m_strDuongdan = _T("");
	m_engaybatdau = _T("");
	m_engayketthuc = _T("");
	//}}AFX_DATA_INIT
	// TODO: add construction code here

}

CXepLichThiView::~CXepLichThiView()
{
}

void CXepLichThiView::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CXepLichThiView)	
	DDX_Control(pDX, IDC_EDIT1, m_Hthi);
	DDX_Control(pDX, IDC_LIST_CHONKHOA, m_ctlKhoahocList);
	DDX_Text(pDX, IDC_EDIT_DUONGDAN, m_strDuongdan);
	DDX_Text(pDX, IDC_EDIT_NGAYBATDAU, m_engaybatdau);
	DDX_Text(pDX, IDC_EDIT_NGAYKETTHUC, m_engayketthuc);
	//}}AFX_DATA_MAP
}

BOOL CXepLichThiView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return CFormView::PreCreateWindow(cs);
}

void CXepLichThiView::OnInitialUpdate()
{
	CFormView::OnInitialUpdate();
	GetParentFrame()->RecalcLayout();
	ResizeParentToFit();
	CDlgGioithieu dlgGioithieu;
	dlgGioithieu.DoModal();
	RECT rect;
	this->GetClientRect(&rect); 
	//b_Ok.Create ( "", WS_VISIBLE  | BS_OWNERDRAW, CRect ( 670, 426, 760, 451 ), this, 1 ) ;
	b_Ok.Create ( "", WS_VISIBLE  | BS_OWNERDRAW, CRect ( rect.right-90, rect.bottom-25, rect.right, rect.bottom), this, 1 ) ;
	b_Ok.LoadBitmaps ( IDB_CHAYUP, IDB_CHAYDOWN) ;
	// Thay doi mau nen
	GetBitmapAndPalette( IDB_BITMAP_DAYB6, m_bitmap, m_palette );
	//1. Set duong dan
	m_strDuongdan="E:\\Luan van tot nghiep\\database\\newdata.mdb";		
	//2. Tao gia tri ban dau cho tiet thi
	CTime ngay;
	m_engayketthuc=m_engaybatdau=(ngay.GetCurrentTime ().Format ("%d-%m-%y"));
	Xeplt.ngaybatdau=Xeplt.ngayketthuc=ngay.GetCurrentTime();
	UpdateData(FALSE);
	

	char *pStr, szPath[_MAX_PATH]; 
	GetModuleFileName(AfxGetInstanceHandle(),  szPath, _MAX_PATH); 
	pStr = strrchr(szPath, '\\'); 
	if (pStr != NULL) 
	*(++pStr)='\0';
	
	CString dgtam=szPath;
	//CString dgtam2;
	dgtam=dgtam + "newdata.mdb";
	dgdan=dgtam;

	//MessageBox(szPath, "", 0);

	Xeplt.gTongsoTiet=12;
	Xeplt.Tietthi.SetSize(Xeplt.gTongsoTiet,-1);
	bool flag=true;
	
	for (int i=0;i<Xeplt.gTongsoTiet;i++)
	{
		Xeplt.Tietthi[i].ban = FALSE;
		if (flag) 
			Xeplt.Tietthi[i].buoisang=true;
		else	
			Xeplt.Tietthi[i].buoisang=false;
		if ((i +1)% 6 ==0) 
			flag=!(flag);
	}
	// Chon cach chay default:
	Xeplt.Cachchay.mosted.sudung=1;
	Xeplt.Cachchay.mosted.giatri=100;
	Xeplt.Cachchay.mosting.sudung=0;
	Xeplt.Cachchay.moststu.sudung=0;
	Xeplt.m_khoangcach =true;
	Xeplt.m_bestfit =true;
	OnButtonKhoidong();
	int j;
	srand( (unsigned)time( NULL ) );
	 for (i=0;i<15;i++)
		for (j=i;j<15;j++)
		{
			 if (i==j) Xeplt.khoangcach[i][j]=0;
		     else Xeplt.khoangcach[i][j]=Xeplt.khoangcach[j][i]=rand();
		}

}

/////////////////////////////////////////////////////////////////////////////
// CXepLichThiView printing

BOOL CXepLichThiView::OnPreparePrinting(CPrintInfo* pInfo)
{
	// default preparation
	return DoPreparePrinting(pInfo);
}

void CXepLichThiView::OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo)
{
	int bienduoi=150;
	CRect rcClient;
	GetClientRect(&rcClient);
	m_ichieucaochu=pDC->GetDeviceCaps(VERTRES);
	m_ichieucaochu-=bienduoi;
	//cai font
	LOGFONT lf;
	memset(&lf,0,sizeof LOGFONT);
	lf.lfHeight = 100;
    lf.lfWeight = FW_BOLD;	   
	strcpy(lf.lfFaceName,"Times New Roman");	
	m_font1.CreateFontIndirect(&lf);
	m_nStrIndex=0;
	m_iMaxIndex=Xeplt.in.GetLength();
}

void CXepLichThiView::OnEndPrinting(CDC* pDC, CPrintInfo* pInfo)
{
	// TODO: add cleanup after printing
	m_font1.DeleteObject();
}

void CXepLichThiView::OnPrint(CDC* pDC, CPrintInfo* pInfo)
{
	int ichieucaochu;	
	int nKcfont;
	CRect strRect(0,0,0,0);
	CRect rectText(0,0,0,0);
	pDC->SetMapMode(MM_TEXT);
	int nCurPage = (pInfo->m_nCurPage);
	CString strTam;
	TEXTMETRIC tm;
	//inbug++;
	CString r; 
//	m_nStrIndex=0;
	for ( int c=nCurPage; c <= nCurPage,m_nStrIndex<m_iMaxIndex; c++)
	{		
			
			
			int iY=300;
			for (;iY<m_ichieucaochu;)
			{
			//	pDC->DrawText(strTmp, rcCalcText, DT_CALCRECT);
			//	nStringheight =rcCalcText.Height();
			pDC->GetTextMetrics(&tm); 					
			nKcfont = tm.tmExternalLeading;
				
				int indexbd=m_nStrIndex;
				m_nStrIndex=Xeplt.in.Find('\n',m_nStrIndex);
				strTam=Xeplt.in.Mid(indexbd,m_nStrIndex-indexbd);
				
				m_nStrIndex++;
				if (m_nStrIndex>=m_iMaxIndex)
				{
					m_nStrIndex=m_iMaxIndex+100;
					break;
				}
				if (m_nStrIndex==-1)
					m_nStrIndex=m_iMaxIndex+100;

				pDC->DrawText(strTam, rectText, DT_CALCRECT);
				ichieucaochu=rectText.Height();

				strRect.left=200;
				strRect.right = pDC->GetDeviceCaps(HORZRES);
				strRect.top=iY;
				strRect.bottom = strRect.top + ichieucaochu;

				
				pDC->SelectObject(&m_font1);
		
				pDC->DrawText(strTam, strRect, DT_WORDBREAK | DT_EXPANDTABS);		
				
				iY=strRect.bottom + nKcfont;				
			}
			r.Format("nCurPage= %d , m_nStrIndex= %d / %d, strTam=%s",nCurPage,m_nStrIndex,m_iMaxIndex,strTam);
				//MessageBox(r);
			if (((iY > m_ichieucaochu) && nCurPage == c)/*||(m_nStrIndex>=m_iMaxIndex)*/) //can than them dk
					break;
	}
}

/////////////////////////////////////////////////////////////////////////////
// CXepLichThiView diagnostics

#ifdef _DEBUG
void CXepLichThiView::AssertValid() const
{
	CFormView::AssertValid();
}

void CXepLichThiView::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}

CXepLichThiDoc* CXepLichThiView::GetDocument() // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CXepLichThiDoc)));
	return (CXepLichThiDoc*)m_pDocument;
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CXepLichThiView message handlers

void CXepLichThiView::OnButtonKhoidong() 
/////////////////////////////////////////////////////////////////
// 1. Mo bang Khoahoc, 
// 2. Lay Monhoc: SotaySV->Monhoc,
// 2a. Lay Phong, Day
// 3. Hien thi Khoa hoc
// 3a. Khoi dong phong
// 4. Lap bang dung do lon
/////////////////////////////////////////////////////////////////
{
	//KHAI BAO BIEN
	int i;	
	CString strtam;
	CSetPhong Phong;
	CSetDaynha day;
	BeginWaitCursor();
	//0. Nut khoi dong da duoc nhan
	bflagKhoidong=TRUE;	
	
	//1. Lay Khoa hoc
	try
	{
		m_Khoahoc.Open();
	}
	catch (CDaoException *e)
	{
		CString message = _T("Couldn't open database--Exception: ");
		message += e->m_pErrorInfo->m_strDescription;
		MessageBox(message);
		goto END;
	}
	m_Khoahoc.MoveLast();
	Xeplt.gTongsoKhoahoc=m_Khoahoc.GetRecordCount();
	Xeplt.Khoahoc.SetSize(Xeplt.gTongsoKhoahoc,-1);
	i=0;
	m_Khoahoc.MoveFirst();
	while(!m_Khoahoc.IsEOF())
	{
		
		Xeplt.Khoahoc[i].khoa=m_Khoahoc.m_Khoahoc;
		Xeplt.Khoahoc[i].dagan=FALSE;	
		i++;
		m_Khoahoc.MoveNext();
	}	
	m_Khoahoc.Close();	
	//2. Lay Mon hoc
	m_Sotay.m_strSort="Mamonhoc";
	m_Sotay.Open();
	m_Sotay.MoveLast();
	Xeplt.gUTongsoMon=m_Sotay.GetRecordCount();
	Xeplt.UMon.SetSize(Xeplt.gUTongsoMon,-1);	
	m_Sotay.MoveFirst();	
	i=0;
	while(!m_Sotay.IsEOF())
	{	
		Xeplt.UMon[i].mamon=m_Sotay.m_Mamonhoc;
		Xeplt.UMon[i].khoahoc=m_Sotay.m_Khoahoc;
		Xeplt.UMon[i].sotietthi=m_Sotay.m_Sotietthi;
		Xeplt.UMon[i].sosvdk=m_Sotay.m_Sosvdk;
		Xeplt.UMon[i].tietbd=0; //can gan
		Xeplt.UMon[i].loai=LOAI_KRB; //ko rang buoc	
		Xeplt.UMon[i].loairb=CUNG;		
		m_Sotay.MoveNext();			
		i++;
	}
	m_Sotay.Close();
	
	//2a. Phong hoc + day

	//Khoi dong day nha

	day.Open();
	i=0;	
	day.MoveFirst();
	while(!day.IsEOF())
	{
		Xeplt.Daynha[i].daynha=day.m_Daynha;
		Xeplt.Daynha[i].sophong=(BYTE) day.m_Sophong;
		Xeplt.Daynha[i].succhua=(USHORT) day.m_Succhuaday;		
		i++;
		day.MoveNext();
	}
	day.Close();	

	//3a.Khoi dong Phong hoc
	
	Phong.Open();	
	Phong.MoveLast();
	Xeplt.gTongsoPhong=(int)Phong.GetRecordCount();
	Xeplt.Phonghoc.SetSize(Xeplt.gTongsoPhong,-1);	
	Phong.MoveFirst();
	i=0;
	while (!Phong.IsEOF())
	{
		Xeplt.Phonghoc[i].tenphong=Phong.m_Maphong;
		Xeplt.Phonghoc[i].daynha=Phong.m_Daynha;
		Xeplt.Phonghoc[i].succhua=Phong.m_Succhua;
		Xeplt.Phonghoc[i].succhuacon =Phong.m_Succhua;
		Xeplt.Phonghoc[i].tiet.SetSize(Xeplt.gTongsoTiet,-1);		
		for(int k=0;k<Xeplt.gTongsoTiet;k++)
			Xeplt.Phonghoc[i].tiet[k]=1; //ranh
		Phong.MoveNext();	
		i++;
	}
	Phong.Close();



	//3. Hien thi Khoa hoc
	m_ctlKhoahocList.SetCheckStyle (BS_AUTO3STATE);
	checkkhoa = new int[Xeplt.gTongsoKhoahoc];
	for (i=0; i<Xeplt.gTongsoKhoahoc; i++)
	{		
		m_ctlKhoahocList.AddString(" Kh�a " + Xeplt.Khoahoc[i].khoa);
		m_ctlKhoahocList.SetCheck(i,1);
		m_ctlKhoahocList.Enable(i,TRUE);
		checkkhoa[i]=TRUE;
	} 
	m_ctlKhoahocList.UpdateWindow();


		
	//4. Lap bang dung do
	Lapbangdungdo();
	/*
	CString rr[324];
		for (i=0; i<324; i++)
			rr[i]=Xeplt.UMon[i].mamon;

	*/
END:
	EndWaitCursor();

}

void CXepLichThiView::OnButtonChay() 
{
		int i=0;
	//4. khoi dong Phong
	/*
	if (!bflagPhongban)
	{
		m_Phong.Open();
		m_Phong.MoveLast();
		Xeplt.gTongsoPhong=(int)m_Phong.GetRecordCount();
		Xeplt.Phonghoc.SetSize(Xeplt.gTongsoPhong,-1);	
		m_Phong.MoveFirst();
		i=0;
		while (!m_Phong.IsEOF())
		{
			Xeplt.Phonghoc[i].tenphong=m_Phong.m_Maphong;
			Xeplt.Phonghoc[i].daynha=m_Phong.m_Daynha;
			Xeplt.Phonghoc[i].succhua=m_Phong.m_Succhua;
			Xeplt.Phonghoc[i].tiet.SetSize(Xeplt.gTongsoTiet,-1);		
			for(int k=0;k<Xeplt.gTongsoTiet;k++)
				Xeplt.Phonghoc[i].tiet[k]=1; //ranh
			m_Phong.MoveNext();	
			i++;
		}
		m_Phong.Close();
	}	
	*/
	BeginWaitCursor();
	if (Xeplt.ChayGT(&m_Hthi)==1)  //co nghiem
	{
		// Set disable cac khoa da chon xep
		for (i=0; i<Xeplt.gTongsoKhoahoc; i++)
		{
			int iCheck=m_ctlKhoahocList.GetCheck(i);
			if (iCheck==1) 
			{
				m_ctlKhoahocList.SetCheck(i,0);
				m_ctlKhoahocList.Enable(i,FALSE);				
			}

			//if (checkkhoa[i]==1)
			//	m_ctlKhoahocList.Enable(i,FALSE);
		}
	}
	else	// vo nghiem
	{		
	}
	EndWaitCursor();
}
void CXepLichThiView::OnButtonChitiet() 
{
	CDlgChonMh dlgChonMh;
	if (bflagKhoidong)
	{
		int icursel= m_ctlKhoahocList.GetCurSel();
		if (m_ctlKhoahocList.GetCheck(icursel)>=1)
		{
			CString strtam;			
			m_ctlKhoahocList.GetText(icursel,strtam);
			dlgChonMh.khoahoc=strtam.Right(2);
			dlgChonMh.DoModal();					
			if ((dlgChonMh.sopt1!=0)&&(dlgChonMh.sopt2!=0)) 
				m_ctlKhoahocList.SetCheck(icursel,2);
			else if (dlgChonMh.sopt1==0)
			{
				m_ctlKhoahocList.SetCheck(icursel,1);
				checkkhoa[icursel]=TRUE;
			}
			else if (dlgChonMh.sopt2==0)
			{
				m_ctlKhoahocList.SetCheck(icursel,0);
				checkkhoa[icursel]=FALSE;
			}

		}	
		
	}	
}

void CXepLichThiView::OnButtonDuongdan() 
{
	CFileDialog filedlg( TRUE, _T( "TSG" ), NULL, OFN_HIDEREADONLY|
	  OFN_OVERWRITEPROMPT, _T( "MDB files (*.MDB)|*.MDB||" ) );
	int iKq=filedlg.DoModal();
	 
   if( iKq!= IDOK )
   {
	  return;
   }
	CString strduongdan=filedlg.GetPathName();	
	
	m_strDuongdan=strduongdan;
	UpdateData(FALSE);
	dgdan=strduongdan;
	OnButtonKhoidong();

}

void CXepLichThiView::OnSelchangeListChonkhoa() 
{
	int i;
	CString strKhoahoc;
	int iCursel=m_ctlKhoahocList.GetCurSel();
if (m_ctlKhoahocList.GetCheck(iCursel)!=checkkhoa[iCursel])
{
	if (m_ctlKhoahocList.GetCheck(iCursel)==1)
	{
		//gan cac mon thanh can gan =0
		m_ctlKhoahocList.GetText(iCursel,strKhoahoc);
		for (i=0; i<Xeplt.gUTongsoMon; i++)
		{
			CString strtam;
			strtam=" Kh�a " + Xeplt.UMon[i].khoahoc;
			if (strtam==strKhoahoc)
				if(Xeplt.UMon[i].tietbd==-1)
					Xeplt.UMon[i].tietbd=0;
		}
		checkkhoa[iCursel]=m_ctlKhoahocList.GetCheck(iCursel);
	//	debugMonchon();
	}
	else if (m_ctlKhoahocList.GetCheck(iCursel)==0)
	{
		//gan cac mon thanh khong gan =-1
		m_ctlKhoahocList.GetText(iCursel,strKhoahoc);
		for (i=0; i<Xeplt.gUTongsoMon; i++)		
		{
			CString strtam;
			strtam=" Kh�a " + Xeplt.UMon[i].khoahoc;
			if (strtam==strKhoahoc)
				if(Xeplt.UMon[i].tietbd==0)
					Xeplt.UMon[i].tietbd=-1;
		checkkhoa[iCursel]=m_ctlKhoahocList.GetCheck(iCursel);
		}
	//	debugMonchon();
	}	
	else // =2
	{
		//gan cac mon thanh khong gan =-1
		m_ctlKhoahocList.SetCheck(iCursel,0);
		m_ctlKhoahocList.GetText(iCursel,strKhoahoc);
		for (i=0; i<Xeplt.gUTongsoMon; i++)		
		{
			CString strtam;
			strtam=" Kh�a " + Xeplt.UMon[i].khoahoc;
			if (strtam==strKhoahoc)
				if(Xeplt.UMon[i].tietbd==0)
					Xeplt.UMon[i].tietbd=-1;
		}
		checkkhoa[iCursel]=m_ctlKhoahocList.GetCheck(iCursel);
	}
}
	
}

void CXepLichThiView::OnButtonNhaptietban() 
{
	
	CDlgPhongban dlgPhongban;
	dlgPhongban.DoModal ();
}

void CXepLichThiView::OnButtonNhapthoigian() 
{
	
	CDlgNhapthoigian dlgThoigianthi;
	dlgThoigianthi.DoModal ();
	m_engaybatdau=Xeplt.ngaybatdau.Format("%d-%m-%Y");
	m_engayketthuc=Xeplt.ngayketthuc.Format("%d-%m-%Y");
	UpdateData(FALSE);
}

void CXepLichThiView::Lapbangdungdo()
{
	DWORD bd,kt;	
	CString szThu[61];
	bd=GetTickCount();
	m_Dungdo.Open();
	
//Remind: nen dam bao bang nay co it nhat 2 phan tu
	m_Dungdo.MoveFirst();	
	CString szMoc;
	STRDungdo *p,*q,*r;
	szMoc=m_Dungdo.m_MaMon1;	
	p=new STRDungdo;
	/*int ifind=0;
	while(Monhoc[ifind]!=szmoc)
	{
		ifind++;
	}*/

	while(!m_Dungdo.IsEOF())
	{		
		int ma=Xeplt.Maraso(UMON, m_Dungdo.m_MaMon1);
		if (Xeplt.UMon[ma].dungdo==NULL)
		{
			p=new STRDungdo;
			Xeplt.UMon[ma].somondungdo=1;
			Xeplt.UMon[ma].dungdo=p;
			p->mon=&(Xeplt.UMon[Xeplt.Maraso(UMON, m_Dungdo.m_MaMon2)]);
			p->sosvdungdo=(USHORT) m_Dungdo.m_SoDungDo;
			p->ketiep=NULL;			

		}
		else
		{
			Xeplt.UMon[ma].somondungdo++;
			p=Xeplt.UMon[ma].dungdo;
			while(p!=NULL)
			{	
				q=p;
				p=p->ketiep;
			}
			r=new STRDungdo;
			q->ketiep=r;
			q=q->ketiep;
			q->mon=&(Xeplt.UMon[Xeplt.Maraso(UMON,m_Dungdo.m_MaMon2)]);
			q->sosvdungdo=(USHORT) m_Dungdo.m_SoDungDo;
			q->ketiep=NULL;
		}	
		m_Dungdo.MoveNext();
	}	
	m_Dungdo.Close();
	kt=GetTickCount();
	CString tickcount;
	tickcount.Format("%ldms",kt-bd);
//	MessageBox(tickcount);
}

//DEL int CXepLichThiView::Maraso(CString const &ma)
//DEL {
//DEL //////////////////////////////////////////////////////////
//DEL // Ham phu
//DEL // In: ma Mon hoc, 
//DEL // Inexplicit parameter: UMon
//DEL // Out: 
//DEL //////////////////////////////////////////////////////////
//DEL 	for(int i=0;i<Xeplt.UMon.GetSize();i++)
//DEL 	{
//DEL 		if (ma==Xeplt.UMon[i].mamon) return i;
//DEL 	}
//DEL 	ASSERT(i<Xeplt.UMon.GetSize());
//DEL 	return  ERROR_MARASO;
//DEL }

void debugMonchon()
{
	for (int i=0; i<Xeplt.gUTongsoMon; i++)
		Monchonbug[i]=Xeplt.UMon[i].tietbd;
}

void CXepLichThiView::OnDraw(CDC* pDC) 
{
	// TODO: Add your specialized code here and/or call the base class

	DrawTheBackground(this,pDC,&m_palette,&m_bitmap);
	
}

HBRUSH CXepLichThiView::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor) 
{
	HBRUSH hbr = CFormView::OnCtlColor(pDC, pWnd, nCtlColor);
	
		if(nCtlColor==CTLCOLOR_STATIC){ 
		//this is only an example - you may have to modify here the code in order to work properly for you 
		if(pWnd->GetDlgCtrlID()==IDC_STATIC){ 
			pDC->SetBkMode(TRANSPARENT); 
			//pDC->SetTextColor(RGB(255,255,255)); //you can change here the color of static text 
			return (HBRUSH)::GetStockObject(NULL_BRUSH); 
		} 
	}else pDC->SetBkMode(OPAQUE);	
	// TODO: Return a different brush if the default is not desired
	return hbr;
}

BOOL CXepLichThiView::GetBitmapAndPalette(UINT nIDResource, CBitmap &bitmap, CPalette &pal)
{
	LPCTSTR lpszResourceName = (LPCTSTR)nIDResource; 
	HBITMAP hBmp = (HBITMAP)::LoadImage( AfxGetInstanceHandle(),  
		lpszResourceName, IMAGE_BITMAP, 0,0, LR_CREATEDIBSECTION ); 
	if( hBmp == NULL )return FALSE; 
	bitmap.Attach( hBmp ); 
	// Create a logical palette for the bitmap 
	DIBSECTION ds; 
	BITMAPINFOHEADER &bmInfo = ds.dsBmih; 
	bitmap.GetObject( sizeof(ds), &ds ); 
	int nColors = bmInfo.biClrUsed ? bmInfo.biClrUsed : 1 << bmInfo.biBitCount; 
	// Create a halftone palette if colors > 256.  
	CClientDC dc(NULL);			// Desktop DC 
	if( nColors > 256 )	pal.CreateHalftonePalette( &dc ); 
	else{ 
		// Create the palette 
		RGBQUAD *pRGB = new RGBQUAD[nColors]; 
		CDC memDC; 
		memDC.CreateCompatibleDC(&dc); 
		memDC.SelectObject( &bitmap ); 
		::GetDIBColorTable( memDC, 0, nColors, pRGB ); 
		UINT nSize = sizeof(LOGPALETTE) + (sizeof(PALETTEENTRY) * nColors); 
		LOGPALETTE *pLP = (LOGPALETTE *) new BYTE[nSize]; 
		pLP->palVersion = 0x300; 
		pLP->palNumEntries = nColors; 
		for( int i=0; i < nColors; i++){ 
			pLP->palPalEntry[i].peRed = pRGB[i].rgbRed; 
			pLP->palPalEntry[i].peGreen = pRGB[i].rgbGreen; 
			pLP->palPalEntry[i].peBlue = pRGB[i].rgbBlue; 
			pLP->palPalEntry[i].peFlags = 0; 
		} 
		pal.CreatePalette( pLP ); 
		delete[] pLP; 
		delete[] pRGB; 
	} 
	return TRUE; 

}

void CXepLichThiView::DrawTheBackground(CView *view, CDC *pDC, CPalette *mp_palette, CBitmap *mp_bitmap)
{	
	if(pDC->IsPrinting())return; 
	CRect rect; 
	CPalette *old_palette=NULL; 
	// Select and realize the palette 
	if( pDC->GetDeviceCaps(RASTERCAPS) & RC_PALETTE && mp_palette->m_hObject != NULL ){ 
		old_palette=pDC->SelectPalette( mp_palette, FALSE ); 
		pDC->RealizePalette(); 
	} 
	view->GetClientRect(rect); 
	pDC->DPtoLP(rect); 
	CMemDC DC(pDC,rect); 
	CDC dcImage; 
	if (!dcImage.CreateCompatibleDC(pDC))return; 
	BITMAP bm; 
	mp_bitmap->GetBitmap(&bm); 
	// Paint the image. 
	CBitmap* pOldBitmap = dcImage.SelectObject(mp_bitmap); 
	for(int i=((int)((double)rect.left/bm.bmWidth))*bm.bmWidth;i<=rect.right/*rect.Width()*/;i+=bm.bmWidth) 
	     for(int j=((int)((double)rect.top/bm.bmHeight))*bm.bmHeight;j<=rect.bottom/*rect.Height()*/;j+=bm.bmHeight) 
		DC->BitBlt(i, j, bm.bmWidth, bm.bmHeight, &dcImage, 0, 0, SRCCOPY); 
	dcImage.SelectObject(pOldBitmap);	 
	pDC->SelectPalette(old_palette,FALSE); 
	pDC->RealizePalette();
}



void CXepLichThiView::OnChay()
{
OnButtonChay();
}
#include "DlgCachchay.h"
void CXepLichThiView::OnButtonPtchay() 
{
	CDlgCachchay dlgCachchay;
	dlgCachchay.DoModal();
}

void CXepLichThiView::OnButtonReset() 
{
	//1. Xoa dau check listbox (nho bien checkkhoa[])		
	//2. Set Khoahoc thanh chua gan
	//3. Nap lai bien thoi gian
	//4. Xoa cac phan tu trong UMon
	//5. Set phuong phap chay : default
	//6. Set cac phong thanh ranh
/////////////////////////////////////////////////
	//1.
	int i;
	for (i=0; i<Xeplt.gTongsoKhoahoc; i++)
	{
		m_ctlKhoahocList.SetCheck(i,1);
		m_ctlKhoahocList.Enable(i,TRUE);
		checkkhoa[i]=TRUE;
	}
	//2.
	for (i=0; i<Xeplt.gTongsoKhoahoc; i++)
		Xeplt.Khoahoc[i].dagan=FALSE;
	Xeplt.gTongsoTiet=12;
	Xeplt.Tietthi.SetSize(Xeplt.gTongsoTiet,-1);
	//3.
	CTime ngay;
	m_engayketthuc=m_engaybatdau=(ngay.GetCurrentTime ().Format ("%d-%m-%y"));
	Xeplt.ngaybatdau=Xeplt.ngayketthuc=ngay.GetCurrentTime();
	UpdateData(FALSE);
	//4.
	for (i=0; i<Xeplt.gUTongsoMon; i++)
	{
		Xeplt.UMon[i].tietbd=0;
		//UMon[i].ngaythi= ko can
		Xeplt.UMon[i].loai=LOAI_KRB;
		Xeplt.UMon[i].loairb=CUNG;				
	}
	//5. 	
	Xeplt.Cachchay.mosted.sudung=1;
	Xeplt.Cachchay.mosted.giatri=100;
	Xeplt.Cachchay.mosting.sudung=0;
	Xeplt.Cachchay.moststu.sudung=0;
	Xeplt.Cachchay.kctiet=0;
	Xeplt.m_khoangcach =true;
	Xeplt.m_bestfit =true;
	//6.
	for (int u=0; u<Xeplt.gTongsoPhong; u++)
		{
			Xeplt.Phonghoc[u].tiet.SetSize(Xeplt.gTongsoTiet);
			{
				for(int k=0; k<Xeplt.gTongsoTiet; k++)
					Xeplt.Phonghoc[u].tiet[k]=1;
			}
		}	
	
	Xeplt.in="";
}

void CXepLichThiView::OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags) 
{
	// TODO: Add your message handler code here and/or call default
	
	CFormView::OnKeyDown(nChar, nRepCnt, nFlags);
}

void CXepLichThiView::OnChar(UINT nChar, UINT nRepCnt, UINT nFlags) 
{
	// TODO: Add your message handler code here and/or call default
	
	CFormView::OnChar(nChar, nRepCnt, nFlags);
}
