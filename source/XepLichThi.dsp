# Microsoft Developer Studio Project File - Name="XepLichThi" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Application" 0x0101

CFG=XepLichThi - Win32 Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "XepLichThi.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "XepLichThi.mak" CFG="XepLichThi - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "XepLichThi - Win32 Release" (based on "Win32 (x86) Application")
!MESSAGE "XepLichThi - Win32 Debug" (based on "Win32 (x86) Application")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""
CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "XepLichThi - Win32 Release"

# PROP BASE Use_MFC 6
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 6
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "Release"
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MD /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_AFXDLL" /Yu"stdafx.h" /FD /c
# ADD CPP /nologo /MD /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_AFXDLL" /D "_MBCS" /Yu"stdafx.h" /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x409 /d "NDEBUG" /d "_AFXDLL"
# ADD RSC /l 0x409 /d "NDEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 /nologo /subsystem:windows /machine:I386
# ADD LINK32 /nologo /subsystem:windows /machine:I386

!ELSEIF  "$(CFG)" == "XepLichThi - Win32 Debug"

# PROP BASE Use_MFC 6
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 6
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Debug"
# PROP Intermediate_Dir "Debug"
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MDd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_AFXDLL" /Yu"stdafx.h" /FD /GZ /c
# ADD CPP /nologo /MDd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_AFXDLL" /D "_MBCS" /FR /Yu"stdafx.h" /FD /GZ /c
# ADD BASE MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x409 /d "_DEBUG" /d "_AFXDLL"
# ADD RSC /l 0x409 /d "_DEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 /nologo /subsystem:windows /debug /machine:I386 /pdbtype:sept
# ADD LINK32 /nologo /subsystem:windows /debug /machine:I386 /pdbtype:sept

!ENDIF 

# Begin Target

# Name "XepLichThi - Win32 Release"
# Name "XepLichThi - Win32 Debug"
# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Source File

SOURCE=.\BDialog.cpp
# End Source File
# Begin Source File

SOURCE=.\BitmapDialog.cpp
# End Source File
# Begin Source File

SOURCE=.\dib256.cpp
# End Source File
# Begin Source File

SOURCE=.\dibpal.cpp
# End Source File
# Begin Source File

SOURCE=.\DlgCachchay.cpp
# End Source File
# Begin Source File

SOURCE=.\DlgCanhbao.cpp
# End Source File
# Begin Source File

SOURCE=.\DlgChonMh.cpp
# End Source File
# Begin Source File

SOURCE=.\DlgChonTietUT.cpp
# End Source File
# Begin Source File

SOURCE=.\DlgGioithieu.cpp
# End Source File
# Begin Source File

SOURCE=.\DlgHienthikq.cpp
# End Source File
# Begin Source File

SOURCE=.\DlgNhapthoigian.cpp
# End Source File
# Begin Source File

SOURCE=.\DlgNhaptietban.cpp
# End Source File
# Begin Source File

SOURCE=.\DlgPhongban.cpp
# End Source File
# Begin Source File

SOURCE=.\DlgThongtinPhong.cpp
# End Source File
# Begin Source File

SOURCE=.\DlgTiendo.cpp
# End Source File
# Begin Source File

SOURCE=.\font.cpp
# End Source File
# Begin Source File

SOURCE=.\MainFrm.cpp
# End Source File
# Begin Source File

SOURCE=.\msflexgrid.cpp
# End Source File
# Begin Source File

SOURCE=.\MySliderControl.cpp
# End Source File
# Begin Source File

SOURCE=.\picture.cpp
# End Source File
# Begin Source File

SOURCE=.\rowcursor.cpp
# End Source File
# Begin Source File

SOURCE=.\SetBangdungdo.cpp
# End Source File
# Begin Source File

SOURCE=.\SetDaynha.cpp
# End Source File
# Begin Source File

SOURCE=.\SetKhoahoc.cpp
# End Source File
# Begin Source File

SOURCE=.\SetNhom.cpp
# End Source File
# Begin Source File

SOURCE=.\SetPhong.cpp
# End Source File
# Begin Source File

SOURCE=.\SetSotay.cpp
# End Source File
# Begin Source File

SOURCE=.\StdAfx.cpp
# ADD CPP /Yc"stdafx.h"
# End Source File
# Begin Source File

SOURCE=.\ToolTipWnd.cpp
# End Source File
# Begin Source File

SOURCE=.\XepLichThi.cpp
# End Source File
# Begin Source File

SOURCE=.\XepLichThi.rc
# End Source File
# Begin Source File

SOURCE=.\XepLichThiDoc.cpp
# End Source File
# Begin Source File

SOURCE=.\XepLichThiView.cpp
# End Source File
# Begin Source File

SOURCE=.\Xeplt.cpp
# End Source File
# End Group
# Begin Group "Header Files"

# PROP Default_Filter "h;hpp;hxx;hm;inl"
# Begin Source File

SOURCE=.\BDialog.h
# End Source File
# Begin Source File

SOURCE=.\BitmapDialog.h
# End Source File
# Begin Source File

SOURCE=.\dib256.h
# End Source File
# Begin Source File

SOURCE=.\dibpal.h
# End Source File
# Begin Source File

SOURCE=.\DlgCachchay.h
# End Source File
# Begin Source File

SOURCE=.\DlgCanhbao.h
# End Source File
# Begin Source File

SOURCE=.\DlgChonMh.h
# End Source File
# Begin Source File

SOURCE=.\DlgChonTietUT.h
# End Source File
# Begin Source File

SOURCE=.\DlgGioithieu.h
# End Source File
# Begin Source File

SOURCE=.\DlgHienthikq.h
# End Source File
# Begin Source File

SOURCE=.\DlgNhapthoigian.h
# End Source File
# Begin Source File

SOURCE=.\DlgNhaptietban.h
# End Source File
# Begin Source File

SOURCE=.\DlgPhongban.h
# End Source File
# Begin Source File

SOURCE=.\DlgThongtinPhong.h
# End Source File
# Begin Source File

SOURCE=.\DlgTiendo.h
# End Source File
# Begin Source File

SOURCE=.\font.h
# End Source File
# Begin Source File

SOURCE=.\khaibao.h
# End Source File
# Begin Source File

SOURCE=.\MainFrm.h
# End Source File
# Begin Source File

SOURCE=.\msflexgrid.h
# End Source File
# Begin Source File

SOURCE=.\MySliderControl.h
# End Source File
# Begin Source File

SOURCE=.\picture.h
# End Source File
# Begin Source File

SOURCE=.\Resource.h
# End Source File
# Begin Source File

SOURCE=.\rowcursor.h
# End Source File
# Begin Source File

SOURCE=.\SetBangdungdo.h
# End Source File
# Begin Source File

SOURCE=.\SetDaynha.h
# End Source File
# Begin Source File

SOURCE=.\SetKhoahoc.h
# End Source File
# Begin Source File

SOURCE=.\SetNhom.h
# End Source File
# Begin Source File

SOURCE=.\SetPhong.h
# End Source File
# Begin Source File

SOURCE=.\SetSotay.h
# End Source File
# Begin Source File

SOURCE=.\StdAfx.h
# End Source File
# Begin Source File

SOURCE=.\ToolTipWnd.h
# End Source File
# Begin Source File

SOURCE=.\XepLichThi.h
# End Source File
# Begin Source File

SOURCE=.\XepLichThiDoc.h
# End Source File
# Begin Source File

SOURCE=.\XepLichThiView.h
# End Source File
# Begin Source File

SOURCE=.\Xeplt.h
# End Source File
# End Group
# Begin Group "Resource Files"

# PROP Default_Filter "ico;cur;bmp;dlg;rc2;rct;bin;rgs;gif;jpg;jpeg;jpe"
# Begin Source File

SOURCE=.\res\BlurMetalDa.bmp
# End Source File
# Begin Source File

SOURCE=.\res\BlurMetalDh.bmp
# End Source File
# Begin Source File

SOURCE=.\res\CT_XLT.bmp
# End Source File
# Begin Source File

SOURCE=.\res\CT_XLT2.bmp
# End Source File
# Begin Source File

SOURCE=.\res\CT_XLT_FORM.bmp
# End Source File
# Begin Source File

SOURCE=.\res\FeatherTexture.bmp
# End Source File
# Begin Source File

SOURCE=.\res\Gioithieu2.bmp
# End Source File
# Begin Source File

SOURCE=.\res\hinh1e.bmp
# End Source File
# Begin Source File

SOURCE=.\res\hinh4.bmp
# End Source File
# Begin Source File

SOURCE=.\res\hinh5.bmp
# End Source File
# Begin Source File

SOURCE=.\res\NUTDOWN_CHAY.bmp
# End Source File
# Begin Source File

SOURCE=.\res\NUTUP_CHAY_.bmp
# End Source File
# Begin Source File

SOURCE=.\res\pushdown.bmp
# End Source File
# Begin Source File

SOURCE=.\res\pushup.bmp
# End Source File
# Begin Source File

SOURCE=".\res\Soap Bubbles.bmp"
# End Source File
# Begin Source File

SOURCE=.\res\Toolbar.bmp
# End Source File
# Begin Source File

SOURCE=.\res\XepLichThi.ico
# End Source File
# Begin Source File

SOURCE=.\res\XepLichThi.rc2
# End Source File
# Begin Source File

SOURCE=.\res\XepLichThiDoc.ico
# End Source File
# End Group
# Begin Source File

SOURCE=.\ReadMe.txt
# End Source File
# End Target
# End Project
# Section XepLichThi : {9F6AA700-D188-11CD-AD48-00AA003C9CB6}
# 	2:5:Class:CRowCursor
# 	2:10:HeaderFile:rowcursor.h
# 	2:8:ImplFile:rowcursor.cpp
# End Section
# Section XepLichThi : {5F4DF280-531B-11CF-91F6-C2863C385E30}
# 	2:5:Class:CMSFlexGrid
# 	2:10:HeaderFile:msflexgrid.h
# 	2:8:ImplFile:msflexgrid.cpp
# End Section
# Section XepLichThi : {6262D3A0-531B-11CF-91F6-C2863C385E30}
# 	2:21:DefaultSinkHeaderFile:msflexgrid.h
# 	2:16:DefaultSinkClass:CMSFlexGrid
# End Section
# Section XepLichThi : {BEF6E003-A874-101A-8BBA-00AA00300CAB}
# 	2:5:Class:COleFont
# 	2:10:HeaderFile:font.h
# 	2:8:ImplFile:font.cpp
# End Section
# Section XepLichThi : {7BF80981-BF32-101A-8BBB-00AA00300CAB}
# 	2:5:Class:CPicture
# 	2:10:HeaderFile:picture.h
# 	2:8:ImplFile:picture.cpp
# End Section
