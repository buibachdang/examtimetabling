//{{AFX_INCLUDES()
#include "msflexgrid.h"
//}}AFX_INCLUDES
#if !defined(AFX_DLGHIENTHIKQ_H__4DBE54F5_1DCC_4B48_839B_C4837AED3CB8__INCLUDED_)
#define AFX_DLGHIENTHIKQ_H__4DBE54F5_1DCC_4B48_839B_C4837AED3CB8__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DlgHienthikq.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CDlgHienthikq dialog
#include "BDialog.h"
class CDlgHienthikq : public CBDialog
{
// Construction
public:
	CDlgHienthikq(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CDlgHienthikq)
	enum { IDD = IDD_DLGHIENTHIKQ };
	CListBox	m_ctlkqlist;
	CMSFlexGrid	c_Grid;
	CString	strTgchay;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDlgHienthikq)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CDlgHienthikq)
	virtual BOOL OnInitDialog();
	afx_msg void OnHaophi();
	virtual void OnOK();
	afx_msg void OnHaophi2();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DLGHIENTHIKQ_H__4DBE54F5_1DCC_4B48_839B_C4837AED3CB8__INCLUDED_)
