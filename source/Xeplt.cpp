// Xeplt.cpp: implementation of the CXeplt class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "xeplichthi.h"
#include "Xeplt.h"
#include "DlgHienthikq.h"
#include "DlgCachchay.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif


const int HSPHAT = 100; //mon bi phat nhieu nhat la 30000 diem  do mau thuan toi da 300 mon khac
	const int MAXDIEM = 60000; //mon dung do  nhieu nhat la 22000 dung do
int dembug=-1;
int stage=0;
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CXeplt::CXeplt()
{

}

CXeplt::~CXeplt()
{

}
#include "DlgTiendo.h"
#include "BitmapDialog.h"

int CXeplt::ChayGT(CEdit * ht)
//////////////////////////////////////////
// 6. Chay GT
//
/////////////////////////////////////////
{
	
	Khoidong();
	int i,j;
	int sofwbug=0,stagebug=0,wbug=0,dembug=-1;
	
	int soquaylai=0;
	int cptb=0,bct=0;
	STRMauthuan * pMt;
	DWORD tdbatdau;
	tdbatdau=GetTickCount();
	CDlgTiendo dlgTiendo;
	dlgTiendo.Create(IDD_DLGTIENDO);
	dlgTiendo.ShowWindow(SW_SHOW);
	dlgTiendo.c_Tiendo.SetRange(1,gTongsoMon);
	stage=0;
	while (stage<gTongsoMon)
	{	
		// Co nhan nut dung khong?
		if (bDungct==TRUE)
			exit(0);
		// Luu trang thai:		
	
		for (i=0;i<gTongsoMon;i++)
			for(j=0;j<gTongsoTiet;j++)
			{
				Trangthai[stage].D[i].tiet[j]=D[i].tiet[j];
			}
		for (i=0;i<gTongsoMon;i++)
			TrangthaiSopt[stage].mon[i]=Monhoc[i].soptcon;
//
//		if (dembug>=100)
//		{
//			debug_Monhoc(); WM_KEYDOWN
//			debug_D();
//			debug_DSttxet();
//		}
		
		TinhdiemUT(Monhoc,stage);
		qSortttxet(DSttxet,stage,gTongsoMon-1);
		int mondx=DSttxet[stage];		
		for (i=0; i<gTongsoMon; i++)
			TrangthaiDSttxet[stage].tt[i]=DSttxet[i];
		Backtrack:		
		dembug++;
		dlgTiendo.c_Tiendo.SetPos(stage);
		//dlgTiendo.strMon="M�n : " + Monhoc[mondx].mamon;
		//dlgTiendo.strDem.Format("Ch�n ti�t l�n th� : %d",dembug);
		
		dlgTiendo.c_T.ShowWindow(SW_HIDE);
		dlgTiendo.c_T.SetWindowText("L�n l�y ti�t th�");
		dlgTiendo.c_T.ShowWindow(SW_SHOW);
		dlgTiendo.c_T.RedrawWindow();

		CString tam;
		tam.Format("%d",dembug);
		dlgTiendo.c_Dem.ShowWindow(SW_HIDE);
		dlgTiendo.c_Dem.SetWindowText(tam);
		dlgTiendo.c_Dem.ShowWindow(SW_SHOW);
		dlgTiendo.c_Dem.RedrawWindow(); 

		//CRect rect;
		//dlgTiendo.c_Dem.GetClientRect(&rect);
		//dlgTiendo.Invalidate();
		dlgTiendo.c_M.ShowWindow(SW_HIDE);
		dlgTiendo.c_M.SetWindowText("M�n");
		dlgTiendo.c_M.ShowWindow(SW_SHOW);
		dlgTiendo.c_M.RedrawWindow();
		
		tam=Monhoc[mondx].mamon;
		dlgTiendo.c_Mon.ShowWindow(SW_HIDE);
		dlgTiendo.c_Mon.SetWindowText(tam);
		dlgTiendo.c_Mon.ShowWindow(SW_SHOW);
		dlgTiendo.c_Mon.RedrawWindow();
	//	dlgTiendo.UpdateData(FALSE);
	//	dlgTiendo.Invalidate();
	//	dlgTiendo.SetBitmap(IDB_BITMAP1,BITMAP_TILE);
//		if (dembug==198)
//			i=9;
	
		CString tdo;
		tdo.Format("%d, %d   %d, %d, %d ",stage,dembug,bct,cptb,sofwbug);
//		pWnd->SetWindowText(tdo);
//		pWnd->UpdateWindow();
		Sleep(10);
		//ht->SetWindowText(tdo);
		//ht->UpdateWindow();
	//	m_Tdo.Format("%d",dembug);
	//	UpdateData(FALSE);
	//	DoEvent();
	//	Sleep(10);
	//	RedrawWindow();
		
		int soptcon=Monhoc[mondx].soptcon;
RBMEM:
		if(Monhoc[mondx].soptcon>0)
		{

//		debug	
	//	if(Dempt(mondx,Monhoc[mondx].sotietthi,0,gTongsoTiet-1)!=Monhoc[mondx].soptcon)
	//	AfxMessageBox("bao loi");
			//lay tiet
		
			Laytiet(mondx,stage);
//
		//MhocBug[stage].soptcon=Monhoc[stage].soptcon;
		//if (dembug==265000)
		//{
		//	debug_Monhoc();
		//	debug_D();			
		//	i=9;
		//}
		}
		else  //rang buoc mem 
		if ((Monhoc[mondx].loairb==MEM)&&(Monhoc[mondx].vtthemtiet<gTongsoTiet))
		{
			//AfxMessageBox("RBM");
			for (j=Monhoc[mondx].vtthemtiet; j<Monhoc[mondx].vtthemtiet + 6; j++) //moi lan them 6 tiet trong
				if (j<gTongsoTiet)
					D[mondx].tiet[j]=1;
			Monhoc[mondx].vtthemtiet+=6;
			Monhoc[mondx].soptcon=Dempt(mondx,Monhoc[mondx].sotietthi,0,gTongsoTiet-1);										
			goto RBMEM;
		}
		else
		{
			dlgTiendo.c_Status.ShowWindow(SW_HIDE);
			dlgTiendo.c_Status.SetWindowText("Quay lui");
			dlgTiendo.c_Status.ShowWindow(SW_SHOW);
			dlgTiendo.c_Status.RedrawWindow();
			
			//backtrack
			bct++;
			//gan lai tinh trang chua gan tiet bat dau cho mon stage
			Monhoc[mondx].tietbd=-1;
//het suc du thua ko de lam gi ca
//			for (i=0;i<gTongsoTiet;i++)
//				Trangthai[stage].D[stage].tiet[i]=D[stage].tiet[i];			
			stage--;stagebug=stage;			
		//	min=stage;		

			if (stage==0)
				soquaylai++;
			//Xet VNghiem

			if (stage<0)
			{
				AfxMessageBox("Bai toan vo nghiem");
				//m_ctlKq.ResetContent();				
				return -1;
			}
//REMIND: khong can load het tat ca
			for(i=0;i<gTongsoMon;i++)
				for(j=0;j<gTongsoTiet;j++)
					D[i].tiet[j]=Trangthai[stage].D[i].tiet[j];							
//
			//debug_D();
			for (i=0; i<gTongsoMon; i++)
			{
				Monhoc[i].soptcon=TrangthaiSopt[stage].mon[i];
				DSttxet[i]=TrangthaiDSttxet[stage].tt[i];
			}

			mondx=DSttxet[stage];

//
		//	debug_Monhoc();
		//	MessageBox("khong lay tiet duoc");
		//	if(dembug==690000)
		//	i=7;

			goto Backtrack;
		}
					
			
		
		if (!Chonphong(mondx,Monhoc[mondx].tietbd,Monhoc[mondx].sotietthi))
		{
			//backtrack
			
			cptb++;
			for(i=0;i<gTongsoMon;i++)
				for(j=0;j<gTongsoTiet;j++)
					if (i!=mondx)
					D[i].tiet[j]=Trangthai[stage].D[i].tiet[j];							

			for (i=0; i<gTongsoMon; i++)
				// khong load lai sopt con cua stage dang gan
				if (i!=mondx)
					Monhoc[i].soptcon=TrangthaiSopt[stage].mon[i];
			//MessageBox("khong chon phong duoc");

			goto Backtrack;
		}

			
//		debug_LuuVT();
//		debug_Trangthai();
//		debug_D();
//		debug_Monhoc();

		if(stage==(gTongsoMon-1)) //ko can forward checking nua
			break;
		
		//forward checking		
	
		pMt=Monhoc[mondx].mauthuan;		
		wbug=0;
		while(pMt!=NULL)
		{	
//
			wbug++;	
			
			if( ((pMt->mon)->tietbd)==-1)
			if (Cachchay.kctiet==0)
			{			
				if(!FwChecking(pMt->mon,pMt->somt,Monhoc[mondx].tietbd,Monhoc[mondx].sotietthi))
				{	
					
					sofwbug++;					
		//			MessageBox("asda");
					UnloadPhong(mondx);
					
					//load trang thai
					for (i=0;i<gTongsoMon;i++)
						for (j=0;j<gTongsoTiet;j++)
						{	
							//khong load lai mien tri cua stage dang gan
//DEADLY: sai lam chet nguoi o day
							if (i!=mondx/*stage*/)							
								D[i].tiet[j]=Trangthai[stage].D[i].tiet[j];
						}
					//load lai sopt con 
					for (i=0; i<gTongsoMon; i++)
							// khong load lai sopt con cua stage dang gan
						if (i!=mondx)
							Monhoc[i].soptcon=TrangthaiSopt[stage].mon[i];
					i=9;
				//	debug_Monhoc();
				//	debug_D();
					goto Backtrack;
				}
			}
			else
			{
				if(!FwChecking2(Cachchay.kctiet,pMt->mon,pMt->somt,Monhoc[mondx].tietbd,Monhoc[mondx].sotietthi))
				{	
					
					sofwbug++;					
		//			MessageBox("asda");
					UnloadPhong(mondx);
					
					//load trang thai
					for (i=0;i<gTongsoMon;i++)
						for (j=0;j<gTongsoTiet;j++)
						{	
							//khong load lai mien tri cua stage dang gan
//DEADLY: sai lam chet nguoi o day
							if (i!=mondx/*stage*/)							
								D[i].tiet[j]=Trangthai[stage].D[i].tiet[j];
						}
					//load lai sopt con 
					for (i=0; i<gTongsoMon; i++)
							// khong load lai sopt con cua stage dang gan
						if (i!=mondx)
							Monhoc[i].soptcon=TrangthaiSopt[stage].mon[i];
					i=9;
				//	debug_Monhoc();
				//	debug_D();
					goto Backtrack;
				}
			}			
			pMt=pMt->ketiep;
		}
//			debug_Trangthai();			
		for (i=0; i<gTongsoTiet; i++)			
			Trangthai[stage].D[mondx].tiet[i]=D[mondx].tiet[i];
		// luu soptcon cua stage dang gan
		TrangthaiSopt[stage].mon[mondx]=Monhoc[mondx].soptcon;		
//
//		if(stage>=36000)
//		{
//		debug_Monhoc();
//		debug_D();
//		}
		stage++;
//
		stagebug=stage;
//		AfxGetApp()->PumpMessage();

//		Sleep(10);
//		while(PeekMessage(&msg,NULL, 0, 0, PM_REMOVE))
//		{
//			TranslateMessage(&msg);
//			DispatchMessage(&msg);
//		}
	}
	//CString t;
	//t.Format("so vong lap la: %d",dembug);
	//m_ctlMthuan.AddString(t);
	dlgTiendo.DestroyWindow();
	delete []arr;	
	CDlgHienthikq *dlgHienthi;
	dlgHienthi=new CDlgHienthikq ;
//	if (dlgHienthi==NULL)		
		DWORD tdketthuc;
	tdketthuc=GetTickCount();
	CString Tgchay;
	Tgchay.Format("%ld ms",tdketthuc-tdbatdau);
	dlgHienthi->strTgchay=Tgchay;
	dlgHienthi->Create(IDD_DLGHIENTHIKQ);
	

	dlgHienthi->ShowWindow(SW_SHOW);
	
	
	//AfxMessageBox(strTgchay);
	
	

	
	// Buoc Hau xu ly: xoa toan bo cac bien da dung xong
	//	Hauxuly(); 



//REMIND: Nho giai phong vung nho cua Monhoc.mauthuan
	
return 1; 
}

int CXeplt::Dempt(int mon, int sotietthi, int btrai, int bphai)
	// tinh so phan tu giua btrai va bphai truoc khi gan xuong am
//REMIND: Nho them chuc nang don dep rac 
{
	int vttrai,vtphai,tam[6];float sodudau,soducuoi;
	float fdau=(float)(btrai)/6;
	float fcuoi=(float)(bphai)/6;
	int khoidau=(int) fdau;
	int khoicuoi=(int) fcuoi;
	int i,j,sopt=0;
	if (khoidau==khoicuoi)
		{
			sodudau=fdau-(int)fdau+(float)0.01;  
			vttrai=(int)(sodudau*6);				
			soducuoi=fcuoi-(int)fcuoi+(float)0.01;
			vtphai=(int)(soducuoi*6);				
			for(i=0;i<vttrai;i++)
			{
				tam[i]=0;
			}
			for(i=vttrai;i<=vtphai;i++)
			{
				tam[i]=D[mon].tiet[khoidau*6+i];
			}
			for(i=vtphai+1;i<6;i++)
			{
				tam[i]=0;
			}
			
			sopt+=Demsopt.Dem(sotietthi,tam[0],tam[1],tam[2],tam[3],tam[4],tam[5]);
		}
	else
		for(i=khoidau;i<=khoicuoi;i++)
		{
			if (i==khoidau)
			{
				sodudau=fdau-(int)fdau+(float)0.01;
				vttrai=(int)(sodudau*6);				
				for(j=vttrai;j<6;j++)
				{
					tam[j]=D[mon].tiet[khoidau*6+j];
				}
				for(j=0;j<vttrai;j++)
					tam[j]=0;
				sopt+=Demsopt.Dem(sotietthi,tam[0],tam[1],tam[2],\
					tam[3],tam[4],tam[5]);
			}
			else if(i==khoicuoi)
			{
				soducuoi=fcuoi-(int)fcuoi+(float)0.01;
				vtphai=(int)(soducuoi*6);			
				for(j=0;j<=vtphai;j++)
				{
					tam[j]=D[mon].tiet[khoicuoi*6+j];
				}
				for(j=vtphai+1;j<6;j++)
					tam[j]=0;
				sopt+=Demsopt.Dem(sotietthi,tam[0],tam[1],tam[2],\
					tam[3],tam[4],tam[5]);
			}
			else
			sopt+=Demsopt.Dem(sotietthi,D[mon].tiet[i*6],\
			D[mon].tiet[i*6+1],D[mon].tiet[i*6+2],D[mon].\
			tiet[i*6+3],D[mon].tiet[i*6+4],D[mon].tiet[i*6+5]);
		}
	return sopt;

}

int CXeplt::Maraso(int loai,const CString &ma)
{
//////////////////////////////////////////////////////////
// Ham phu
// In: ma Mon hoc, 
// Inexplicit parameter: UMon
// Out: 
//////////////////////////////////////////////////////////
	if (loai==MONHOC)
	{
		for(int i=0;i<gTongsoMon;i++)
		{
			if (ma==Monhoc[i].mamon) return i;
		}	
		return  ERROR_MARASO;
	}
	else if (loai==UMON)
		for(int i=0;i<gUTongsoMon;i++)
		{
			if (ma==UMon[i].mamon) return i;
		}	
		return  ERROR_MARASO;
}

int CXeplt::Timvttietbd(CTime ngaybd, CTime ngaykt, CTime ngaycx, int tietcx)
{	
	CTimeSpan deltabdkt;
	CTimeSpan deltabdcx;
	int kcngay;
	deltabdkt=ngaykt-ngaybd;
	deltabdcx=ngaycx-ngaybd;	
	if (deltabdcx<=deltabdkt)
	{
		kcngay=atoi(deltabdcx.Format("%D"));
		return (kcngay*12+tietcx);
	}
	else return -1;	
}

bool CXeplt::Laytiet(int mon,int phu)
///////////////////////////////
// in:
// out:
// inout:
// dsktm: Monhoc,gTongsoTiet,gTongsoMon,D
// hul: Monhoc[mon].tbd, D   
// Ham: qsortex();
// Chucnang: 
//dk: so tiet thi phai >0 va luon dam bao chon duoc va neu chon 
// khong duoc thi nguyen do la trong mien tri co so 0(backtrack hoac
// lay tiet khong duoc, phai lay lai o mon truoc)
// 
{	//debug
	//int mtam[60];
	int i=0,j,vtmax=-1,sotietthi=Monhoc[mon].sotietthi;;
	bool btimra=false;	

	for(i=0;i<gTongsoTiet;i++)
		arr[i]=i;
	/*
	for (i=0;i<gTongsoTiet;i++)
	{
		D[mon].tiet[i]=i;
		thu[i]=i;
	}*/
	qSortex(arr,mon,0,gTongsoTiet-1);
//

	/*
	for(i=1;i<gTongsoTiet;i++)
		if (thu[arr[i]]>thu[arr[i-1]])
			AfxMessageBox("toiloi toiloi");
	*/
	/*
	CString tam;tam="";
	for (i=0;i<gTongsoTiet;i++)
	{
		CString t;
		t.Format("%d",D[mon].tiet[arr[i]]);
		tam=tam + t + " "; 
	}
	myFile.Write(tam,lstrlen(tam));
	myFile.Write("\r\n",2);
	*/
//debug
	
	//if(mon>=59000)
	//{
	//	for(i=0;i<60;i++)
	//		mtam[i]=arr[i];
	//}
	
	i=0;
	//int MT[129][60];
	//if (dembug==17)
	//	for(i=0;i<gTongsoMon;i++)
	//		for(j=0;j<gTongsoTiet;j++)
	//			MT[i][j]=D[i].tiet[j];
	while (!btimra)
	{		
//DEADLY: sai lam chet nguoi o cho nay (sai:xet arr[i]<=0)
		if (D[mon].tiet[arr[i]]<=0)
		{
			btimra=false;
			break;
		}
		
		int khoisau=arr[i]/6+1;
		if (arr[i] <= (khoisau*6-sotietthi))
		{
			for(j=1;j<=sotietthi-1;j++)  //tung sai
			{				
				if(D[mon].tiet[arr[i]+j]<=0)
					break;	
				
			}		
			if (j==sotietthi)
			{
				btimra=true;
				vtmax=arr[i];
			}
		}
		i++;
	}
	if (!btimra) 
		return false;
	else 
	{		
		Monhoc[mon].tietbd=vtmax;
		// loai bo rac va gan lai soptcon
		int khoigan= vtmax/6;
		int sopttrongkhoidau=Demsopt.Dem(sotietthi,D[mon].tiet[khoigan*6],\
		D[mon].tiet[khoigan*6+1],D[mon].tiet[khoigan*6+2],\
		D[mon].tiet[khoigan*6+3],D[mon].tiet[khoigan*6+4],\
		D[mon].tiet[khoigan*6+5]);
				//gan xuong 0
//SAI
		//for(i=vtmax;i<vtmax+sotietthi;i++)
		//		D[mon].tiet[i]=0;
//SUA LAI
		D[mon].tiet[vtmax]=0;
	
		int sopttrongkhoisau=Demsopt.Dem(sotietthi,D[mon].tiet[khoigan*6],\
		D[mon].tiet[khoigan*6+1],D[mon].tiet[khoigan*6+2],\
		D[mon].tiet[khoigan*6+3],D[mon].tiet[khoigan*6+4],\
		D[mon].tiet[khoigan*6+5]);
		if (sopttrongkhoisau==0)		
			for (i=khoigan*6;i<khoigan*6+6;i++)
				D[mon].tiet[i]=0;

		Monhoc[mon].soptcon-=sopttrongkhoidau-sopttrongkhoisau;		
//
		/*
		if(Dempt(mon,Monhoc[mon].sotietthi,0,gTongsoTiet-1)!=Monhoc[mon].soptcon)
		{
			AfxMessageBox("bao loi");
			int mu[150][100];
			for (i=0;i<gTongsoMon;i++)
				for(j=0;j<gTongsoTiet;j++)
					mu[i][j]=D[i].tiet[j];
		}
		*/
		return true;
		
	}

}





BOOL CXeplt::FwChecking(CMh *mon, int somt, int tietbd, int sotietthi)
	// in: 
// out:
// dsktm:gTongsoTiet
// hul:
// chucnang:
// dk:
// ngay: 4/12/2002
{

//
#ifdef DEBUG
	if (mon->soptcon==0)
		{	
			AfxMessageBox("soptcon = 0 trong FWCHECK");								
		}
#endif

	int i;
	i=tietbd-1;
	int ttmon=mon->sott;
	while ((i>=0)&&(D[ttmon].tiet[i]>0))
	{		
		i--;
	}
	int btrai;
	if (i<0) 
		btrai=0;	
	else
		btrai=i+1;	
	i=tietbd+sotietthi;
	while ((i<gTongsoTiet)&&(D[ttmon].tiet[i]>0))
	{	
		i++;
	}
	int bphai;		
	if (i>gTongsoTiet-1) 
		bphai=gTongsoTiet-1;
	else
		bphai=i-1;


	// thu giam mien phai dem soptdau, soptcuoi
	i=tietbd;
	float fthuong=(float)i/6;
	while ((fthuong - (int) fthuong)>0)
	{
		i--;
		fthuong=(float)(i)/6;
	}
	int ivtbdkhoi=i;
		
	int	soptdau=Demsopt.Dem(Monhoc[ttmon].sotietthi,D[ttmon].tiet[ivtbdkhoi],\
				D[ttmon].tiet[ivtbdkhoi+1],D[ttmon].tiet[ivtbdkhoi+2],\
				D[ttmon].tiet[ivtbdkhoi+3],D[ttmon].tiet[ivtbdkhoi+4],\
				D[ttmon].tiet[ivtbdkhoi+5]);
	
		
	//bo:int soptdau=Dempt(ttmon,Monhoc[ttmon].sotietthi,btrai,bphai);
	//gan xuong diem am
	for (i=tietbd;i<tietbd+sotietthi;i++)
	{
//debug TH mientri co gt =0
	//		if(D[ttmon].tiet[i]==0)
	//	MessageBox("tata");
//REMIND: neu tai vt do la so 0 thi khong tru, neu dang la duong thi lay 0 - so do.
		if (D[ttmon].tiet[i]>0)
			D[ttmon].tiet[i]=-somt;
		else if( D[ttmon].tiet[i]<0)
	//		if(D[ttmon].tiet==0) thi ko tru else tru
		D[ttmon].tiet[i]-=(somt+ HSPHAT); //- them 1 la do khoi tao ban dau =1
//REMIND: nen co he so phat neu dung do nhieu mon
	}
	//if ()
	//Lay so pt con lai
//
//	debug_D();
	int soptsau=Demsopt.Dem(Monhoc[ttmon].sotietthi,D[ttmon].tiet[ivtbdkhoi],\
				D[ttmon].tiet[ivtbdkhoi+1],D[ttmon].tiet[ivtbdkhoi+2],\
				D[ttmon].tiet[ivtbdkhoi+3],D[ttmon].tiet[ivtbdkhoi+4],\
				D[ttmon].tiet[ivtbdkhoi+5]);
	int soptmat;	
	soptmat=soptdau-soptsau;
//REMIND: coi lai ASSERT
	//ASSERT(soptmat);
	if (soptmat==Monhoc[ttmon].soptcon)  //neu  soptmat > ptcon thi co van de
		return false;
	
	Monhoc[ttmon].soptcon-=soptmat; 
	int soptcon=Monhoc[ttmon].soptcon;


	//BAT DAU GAN DIEM CHO TOAN BO
	int itr;
	int pivot;		
	float tletrai,fsoptt,fsoptp;



	//A.TINH BEN TRAI	
	int tspttrai,congsaitrai;	

	// Phan bo lai diem ben trai vung dang gan	
	if (btrai!=tietbd)
	{

		//A.1 Tinh tong diem ben trai
		int tdlbtrai=0;

		i=6;
		itr=btrai-1;
		if (itr>=0)
			while ((D[ttmon].tiet[itr]<0)&&(i>0))
			{		
				tdlbtrai+=abs(D[ttmon].tiet[itr]);
				itr--;
				i--;
				if (itr<0) 			
					break;							
			}

		// tinh tong diem ben phai, >=tietbd
		int tdrtietbd=0;

		itr=tietbd;
		i=6;
		while ((D[ttmon].tiet[itr]<0)&&(i>0))
		{	
			tdrtietbd+=abs(D[ttmon].tiet[itr]);
			itr++;
			i--;
			if (itr>(gTongsoTiet-1)) 			
				break;							
		}				

		//A.2 Gan diem ben trai

											//ASSERT(btrai+1) , phai dam bao phan tu ben trai cua btrai <=0 hoac khong co phan tu nao
//REMIND: coi chung cho xet dk nay
		if ((btrai==0) || (D[ttmon].tiet[btrai-1]==0)) 
		{			
			for (i=1; i<=tietbd-btrai; i++)
			{				
				D[ttmon].tiet[tietbd-i]=MAXDIEM-tdrtietbd+i-1;
			}
		}
		else //phan tu btrai la am			
		{			
//REMIND: hien tai chon 6 tiet lien tiep de tinh diem								

			tspttrai=tietbd-btrai; 
			tletrai= (float)abs(tdlbtrai)/abs(tdlbtrai+tdrtietbd);						
			if (tletrai<0.5) //xet ben phai										
			{
				fsoptp= (1-tletrai)*tspttrai; // tu btrai cho den tietbd-1
				if ((fsoptp - (int) fsoptp)>=0.5)
					pivot = ((int) fsoptp +1);
				else
					pivot = (int) fsoptp;
//REMIND: neu xay ra dk sau thi vo ly
#ifdef DEBUG
				if (pivot==0) {pivot=1;AfxMessageBox("neu xay ra thi vo ly");}
#endif
//REMIND: TH tspt==0: xet qua loa chu chua chinh xac
				//gan ben phai
				for (i=1; i<=pivot; i++)						//MAXDIEM
					D[ttmon].tiet[tietbd-i]=MAXDIEM-tdlbtrai-tdrtietbd+i-1;
				
				if (pivot<tspttrai)
				{				
					float ftam= (float) pivot/(tspttrai-pivot);					
					if ((ftam-(int)ftam)>0.5)
						congsaitrai= (int) ftam +1;
					else congsaitrai= (int) ftam;					
					D[ttmon].tiet[btrai]=MAXDIEM-tdlbtrai-tdrtietbd;
					for (i=1; i<=tspttrai-pivot-1;i++)
						D[ttmon].tiet[btrai+i]=D[ttmon].tiet[btrai+i-1] + congsaitrai;
				}
				
			}				
			else //ti le trai > 0.5: lam nguoc lai
			{				
				fsoptt= tletrai*tspttrai; 
				if ((fsoptt - (int) fsoptt)>=0.5)
					pivot = ((int) fsoptt +1);
				else
					pivot = (int) fsoptt;
				if (pivot==0) pivot=1; 				
				for (i=1; i<=pivot; i++)						
				{					
					D[ttmon].tiet[btrai+i-1]=MAXDIEM-tdlbtrai-tdrtietbd+i-1;
				}

				//gan phan ben kia pivot
				if (pivot<tspttrai)
				{					
					float ftam= (float) pivot/(tspttrai-pivot);					
					if ((ftam-(int)ftam)>0.5)
						congsaitrai= (int) ftam +1;
					else congsaitrai= (int) ftam;
					congsaitrai= pivot/(tspttrai-pivot);
					D[ttmon].tiet[tietbd-1]=MAXDIEM-tdlbtrai-tdrtietbd;
					for (i=1; i<=tspttrai-pivot-1;i++)
					{						
						D[ttmon].tiet[tietbd-1-i]=D[ttmon].tiet[tietbd-i] + congsaitrai;
					}
				}
			}
		}
	}

	//B. TINH DIEM BEN PHAI
	

	

	

	int tietkt=tietbd+sotietthi-1;	
	if (tietkt!=bphai)
	{
		
		//B1. Tinh tong diem ben phai
		// tinh tong diem phai cua tietkt	
		
		int tdltietkt=0;
		i=6;
		itr=tietkt;
		while ((D[ttmon].tiet[itr]<0)&&(i>0))
		{	
			tdltietkt+=abs(D[ttmon].tiet[itr]);
			itr--;
			i--;
			if (itr<0) 			
				break;							
		}	
	
		// tinh tong diem phai bphai
		int tdrbphai=0;
		i=6;
		itr=bphai+1;
		if (itr<=(gTongsoTiet-1))
			while ((D[ttmon].tiet[itr]<0)&&(i>0))
			{	
				tdrbphai+=abs(D[ttmon].tiet[itr]);
				itr++;
				i--;
				if (itr>(gTongsoTiet-1)) 			
					break;							
			}				

		// B2. GAN DIEM BEN PHAI

			int congsaiphai,tsptphai;	
			tsptphai=bphai-tietkt; 
			tletrai= (float)abs(tdltietkt)/abs(tdltietkt+tdrbphai);			
		if ((bphai==gTongsoTiet-1) || (D[ttmon].tiet[bphai+1]==0)) 
		{			
			for (i=1; i<=bphai-tietkt; i++)
			{			
				D[ttmon].tiet[tietkt+i]=MAXDIEM-tdltietkt+i-1;
			}
		}
		else //phan tu bphai la am			
		{					
			if (tletrai<0.5) //xet ben phai
			{							
				fsoptp= (1-tletrai)*tsptphai; 
				if ((fsoptp - (int) fsoptp)>=0.5)
					pivot = ((int) fsoptp +1);
				else
					pivot = (int) fsoptp;
				
				for (i=1; i<=pivot; i++)						//MAXDIEM
					D[ttmon].tiet[bphai+1-i]=MAXDIEM-tdltietkt-tdrbphai+i-1;
				
				if (pivot<tsptphai)
				{
					
					float ftam= (float) pivot/(tsptphai-pivot);					
					if ((ftam-(int)ftam)>0.5)
						congsaiphai= (int) ftam +1;
					else congsaiphai= (int) ftam;

					D[ttmon].tiet[tietkt+1]=MAXDIEM-tdltietkt-tdrbphai;
					for (i=1; i<=tsptphai-pivot-1;i++)
						D[ttmon].tiet[tietkt+1+i]=D[ttmon].tiet[tietkt+i] + congsaiphai;
				}
			}				
			else //ti le trai > 0.5: lam nguoc lai
			{				
				fsoptt= tletrai*tsptphai; 
				if ((fsoptt - (int) fsoptt)>=0.5)
					pivot = ((int) fsoptt +1);
				else
					pivot = (int) fsoptt;
				if (pivot==0) pivot=1;

				for (i=1; i<=pivot; i++)						
				{					
					D[ttmon].tiet[tietkt+i]=MAXDIEM-tdltietkt-tdrbphai+i-1;
				}
				//gan phan ben kia pivot
				if (pivot<tsptphai)
				{
					float ftam= (float) pivot/(tsptphai-pivot);					
					if ((ftam-(int)ftam)>0.5)
						congsaiphai= (int) ftam +1;
					else congsaiphai= (int) ftam;					
					D[ttmon].tiet[bphai]=MAXDIEM-tdltietkt-tdrbphai;
					for (i=1; i<=tsptphai-pivot-1;i++)
					{						
						D[ttmon].tiet[bphai-i]=D[ttmon].tiet[bphai-i-1] + congsaiphai;
					}
				}
			}
		}
	}

//	debug_D();
//	if(Dempt(ttmon,Monhoc[ttmon].sotietthi,0,gTongsoTiet-1)!=Monhoc[ttmon].soptcon)
//				MessageBox("bao loi trong fwcheck");
/*	
	if (dembug>=30600) 
	{
		debug_Monhoc();
		debug_D();
	}
*/

	return true;
}



void CXeplt::qSortex(int a[], int stage, int lo, int hi)
{
	
	 int left, right, median, temp;

   if( hi > lo ) 
    { left=lo; right=hi;
      median=D[stage].tiet[a[lo]];  

      while(right >= left)      
       { 
		  
		 while(D[stage].tiet[a[left]] > median) left++;         

         while(D[stage].tiet[a[right]] < median) right--;        

         if(left > right) break;

         temp=a[left]; a[left]=a[right]; a[right]=temp; 
         left++; right--;
       }
      qSortex(a,stage, lo,right);
      qSortex(a,stage, left,hi);
    }
	
	/*
   int left, right, median, temp;

   if( hi > lo ) 
    { left=lo; right=hi;
      median=thu[a[lo]];  

      while(right >= left)      
       { 
		  
		 while(thu[a[left]] > median) left++;         

         while(thu[a[right]]< median) right--;        

         if(left > right) break;

         temp=a[left]; a[left]=a[right]; a[right]=temp; 
         left++; right--;
       }
      qSortex(a,stage, lo,right);
      qSortex(a,stage, left,hi);
    }
	*/
}




void CXeplt::Khoidong()

// 1. Duyet DS UMon -> lay gTongsoMon -> SetSize Monhoc, DSttxet,
//   Gan ten mon hoc, nhom
//	,Trang thai,Trangthaisoptcon,TrangthaiDSttxet, D
// 2. Monhoc[i].tietbd=-1;
// 3. Khoi dong D, Monhoc[i].soptcon:SAI phai dem sau khi lap lai bang dung do
//   , Monhoc[i].Nhom
//     ,new mang arr (danh cho qsort)
// 4. Lap bang dung do.
// 6. In 
{
//KHAI BAO
	int i,j;
	//0.
		bDungct=FALSE;
	//1.a Duyet DS UMon	
	gTongsoMon=0;	
	for (i=0; i<gUTongsoMon; i++)	
		if (UMon[i].tietbd==0)
		{		
			gTongsoMon++;						
		}
	// gan ten mon hoc
	Monhoc.SetSize(gTongsoMon,-1);
	j=0;
	for (i=0; i<gUTongsoMon; i++)	
		if (UMon[i].tietbd==0)
		{
			Monhoc[j].mamon=UMon[i].mamon;
			Monhoc[j].sotietthi=UMon[i].sotietthi;
			Monhoc[j].sosvdk=UMon[i].sosvdk;
			Monhoc[j].sodungdo=UMon[i].somondungdo;
			Monhoc[j].sott=j;
			Monhoc[j].loairb=UMon[i].loairb;
			Monhoc[j].vtthemtiet=0;
			j++;	
		}
//
	//CString t;
	//t.Format("so mon %d",gTongsoMon);
	//AfxMessageBox(t);

	//gan Nhom
	gTongsoNhom=0;
	i=0;
	STRNhom nhomtam;
	// De chay giai thuat nay dung doi hoi phai Sort truoc ca 2 thu
	if (gTongsoMon>0)
	{
		m_Nhom.Open();
		m_Nhom.MoveFirst();
		while (!m_Nhom.IsEOF())
		{		
			if (m_Nhom.m_Mamonhoc==Monhoc[i].mamon)
			{
				nhomtam.manhom=m_Nhom.m_Nhom+m_Nhom.m_To;
				nhomtam.siso=(USHORT)m_Nhom.m_Soluong;
				nhomtam.tenphong="";
				Monhoc[i].nhom.Add(nhomtam);
				m_Nhom.MoveNext();
				if (m_Nhom.m_Mamonhoc!=Monhoc[i].mamon)
					i++;
				if (i==gTongsoMon)
					break;
				m_Nhom.MovePrev();
			}
			gTongsoNhom++;
			m_Nhom.MoveNext();
		}
		m_Nhom.Close();
	}
		


	//1.b Setsize Monhoc
	Monhoc.SetSize(gTongsoMon,-1);
	//1.c Setsize DSttxet, khoi dong
	DSttxet.SetSize(gTongsoMon,-1);
	for (i=0; i<gTongsoMon; i++)
		DSttxet[i]=i;
	// Khoi dong bien Trang thai
		Trangthai.SetSize(gTongsoMon,-1);
	for(i=0;i<gTongsoMon;i++)
		Trangthai[i].D.SetSize(gTongsoMon,-1);
	for(i=0;i<gTongsoMon;i++)
		for(int k=0;k<gTongsoMon;k++)
			Trangthai[i].D[k].tiet.SetSize(gTongsoTiet,-1);	
	//1.d Setsize TrangthaiDSttxet
	TrangthaiDSttxet.SetSize(gTongsoMon,-1);
	for (i=0; i<gTongsoMon; i++)
		TrangthaiDSttxet[i].tt.SetSize(gTongsoMon,-1);
	//1.e SetSize Trangthaisopt
	TrangthaiSopt.SetSize(gTongsoMon,-1);
	for (i=0; i<gTongsoMon; i++)
		TrangthaiSopt[i].mon.SetSize(gTongsoMon,-1);	

	//2. Monhoc.tietbd=-1
	for (i=0; i<gTongsoMon; i++)
		Monhoc[i].tietbd=-1;

	//3.a Khoi dong D (mien tri)
	D.SetSize(gTongsoMon,-1);
	for (i=0; i<gTongsoMon; i++)
		D[i].tiet.SetSize(gTongsoTiet,-1);

	for(i=0;i<gTongsoMon;i++)
		for(j=0;j<gTongsoTiet;j++)
		{
			D[i].tiet[j]=(int)(!Tietthi[j].ban);			
			int tt=Maraso(UMON,Monhoc[i].mamon);
			if (UMon[tt].loai==LOAI_NCD)
			{		
				int k;
				for (k=0; k<UMon[tt].icd; k++)				
					D[i].tiet[k]=0;
				for (k=UMon[tt].icd+UMon[tt].sotietthi; k<gTongsoTiet; k++)				
					D[i].tiet[k]=0;
				for (k=UMon[tt].icd; k<UMon[tt].icd+UMon[tt].sotietthi;k++)
					D[i].tiet[k]=1;								}
			if (UMon[tt].loai==LOAI_NTD)
			{
				int k;
				for (k=0; k<UMon[tt].ibd; k++)
					D[i].tiet[k]=0;
				for (k=UMon[tt].ikt+12; k<gTongsoTiet; k++)
					D[i].tiet[k]=0;
				for (k=UMon[tt].ibd; k<UMon[tt].ikt+12; k++)
					D[i].tiet[k]=1;
				
			}	
		}


//
		/*
	for(i=0;i<gTongsoMon;i++)
		
		{
			if (UMon[i].loai==LOAI_NCD)
			{
				CString r;
				r.Format("%s",UMon[i].mamon);
				AfxMessageBox(r);
			}
		}*/
//debug
	/*
	int ww[324][12];
	for (i=0;i<324;i++)
		for (j=0;j<12;j++)
			ww[i][j]=D[i].tiet[j];
	*/

//
	/*
	int ee[324];
		for(i=0; i<324; i++)
			ee[i]=Monhoc[i].soptcon;
	*/
	//3.c new arr
	arr=new int[gTongsoTiet];
	//4. Gan ten mon hoc	
			
	//5. Copy bang mau thuan vao mon hoc
	int k=0;int vtdd;int bug=0;
	STRMauthuan *p,*prevp;
	STRDungdo *q;
	for (i=0; i<gTongsoMon; i++)
	{		
		CString mondx;
		mondx=Monhoc[i].mamon;
		Monhoc[i].mauthuan = new STRMauthuan;		
		p=Monhoc[i].mauthuan;		
		prevp=p;
		while (UMon[k].mamon!=Monhoc[i].mamon)
		{
			k++;
		}
		q=UMon[k].dungdo;
		int dem=0;
		while (q!=NULL)
		{
			q=q->ketiep;
			dem++;
		}
		q=UMon[k].dungdo;
		dem=0;
		while (q!=NULL)
		{	
			if (((q->mon)->tietbd)==0) //can gan: them vao 
			{
				p->mon=&Monhoc[Maraso(MONHOC, (q->mon)->mamon)];
				p->somt=q->sosvdungdo;
				p->ketiep=new STRMauthuan;
				prevp=p;
				p=p->ketiep;
			}
			else if(((q->mon)->tietbd)==1) //da gan xoa mien tri
			{				
				if((vtdd=Timvttietbd(ngaybatdau,ngayketthuc,(q->mon)->ngaythi,(q->mon)->tietbd))!=-1)
				{
					for (int t=vtdd; t<vtdd+(q->mon)->sotietthi;t++)
						D[i].tiet[t]=0;
				}
			}
			q=q->ketiep;
			dem++;
			if (dem==121)
				vtdd=0;
		}
		if (prevp==p) //khong co mauthuan
		{
			//delete Monhoc[i].mauthuan;
			Monhoc[i].mauthuan=NULL;
		}
		else
		{
			delete p;
			prevp->ketiep=NULL; //gan vt cuoi cung trong chuoi mauthuan=NULL		
		}
	}

		//3.b gan so pt con cho tung mon
	//hFile = CreateFile(_T("C:\\FOO.txt"),
      //   GENERIC_WRITE, FILE_SHARE_READ,
        // NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);
	//CFile myFile((int) hFile);
#ifdef FILE
	myFile.Open("c:\\dang.txt",CFile::modeCreate||CFile::modeWrite);
#endif


	for(i=0;i<gTongsoMon;i++)
	{
		Monhoc[i].soptcon=Dempt(i,Monhoc[i].sotietthi,0,gTongsoTiet-1);
		//CString tam;
		//tam.Format("mon hoc %s co %d va so phan tu con la %d \r\n",Monhoc[i].mamon,Monhoc[i].sotietthi,Monhoc[i].soptcon);
		//myFile.Write(tam,lstrlen(tam));
	}
	/*CFile file;
	char *filename="dangx.txt";
	char dd[]="ta ten la dang";
	file.Open(filename,CFile::modeWrite);
	file.Write(dd,10);
	file.Close();*/
	
      //CString sz;sz= "Cai gi!";      
     // myFile.Write(sz, lstrlen(sz));
     // myFile.Close();

//
	/*
	CString ss[324][324];
	STRMauthuan *y;
	for (i=0; i<324; i++)
	{
		y=Monhoc[i].mauthuan;
		j=0;
		if (y==NULL) 
		{
			AfxMessageBox("NULL");			
		}
		while (y!=NULL)
		{
			ss[i][j]=Monhoc[i].mamon + (y->mon)->mamon;
			j++;
			y=y->ketiep;
		}
	}
	*/
	//6. In
	in="";
	//7. Khoi dong cho giai thuat chon phong
	
}

void CXeplt::qSortttxet(CUIntArray &a, int lo, int hi)
{
	 int left, right,  temp;
	 float median;
   if( hi > lo ) 
    { left=lo; right=hi;
      median=Monhoc[a[lo]].diemut; 

      while(right >= left)      
       { 
		  while(Monhoc[a[left]].diemut > median) left++;   

         while(Monhoc[a[right]].diemut < median) right--;         

         if(left > right) break;

         temp=a[left]; a[left]=a[right]; a[right]=temp; 
         left++; right--;
       }    
        qSortttxet(a, lo,right);
	    qSortttxet(a, left,hi);
    }
}

void CXeplt::TinhdiemUT(CMonhoc &Mh, int bd)
{
	for (int i=bd; i<gTongsoMon; i++)
	{
#ifdef DEBUG		
		if (Mh[DSttxet[i]].soptcon ==0) 
			AfxMessageBox("chia 0");
#endif
		Mh[DSttxet[i]].diemut= ( 100/(float)Mh[DSttxet[i]].soptcon*(Cachchay.mosted.giatri/100)*(Cachchay.mosted.sudung) //+ Mh[DSttxet[i]].sodungdo*3;//Mh[i].sosvdk/20;
			+ Mh[DSttxet[i]].sodungdo/(float)2.6*(Cachchay.mosting.giatri/100)*(Cachchay.mosting.sudung)			
			+ Mh[i].sosvdk/20*(Cachchay.moststu.giatri/100)*(Cachchay.moststu.sudung) );		
	}
#ifdef FILE
	CString tam;tam="";
	for (i=0;i<gTongsoMon;i++)
	{
		CString t;
		t.Format("%.1f",Mh[DSttxet[i]].diemut);
		if (i==stage)
			tam=tam+ "  ";
		tam=tam + t + " "; 
	}
	myFile.Write(tam,lstrlen(tam));
	myFile.Write("\r\n",2);
#endif
}

CString CXeplt::Tietrangay(CTime ngaybd, int tiet, CTime &ngay)
{
	CTime tmp;
	CString strtam;
	tmp=ngaybd;
	CTime t1( 1999, 3, 19, 22, 15, 0 );
    CTime t2( 1999, 3, 20, 22, 15, 0 );
    CTimeSpan ts = t2 - t1;
	int d=tiet/12;
	if (d==0) 
		strtam=tmp.Format("%d/%m"); 
	else
		for (int i=1;i<=d;i++)
			tmp=tmp+ts;
	ngay=tmp;
	strtam=tmp.Format ("%d/%m/%y");	
	return strtam;
}

void CXeplt::Hauxuly()
////////////////////////////////////////////////////////////
//0.Gan cac UMon.tietbd=1;
//1. Xoa cac mau thuan trong Monhoc.mauthuan
//2. Trangthaisoptcon
//3. D
//4. Trangthai
//5. TrangthaiDSttxet
//6.Monhoc
////////////////////////////////////////////////////////////
{
	int i,j,dem=0;
	//0. Set cac mon thanh da gan
	for (i=0; i<gTongsoMon; i++)	
		{
			UMon[Maraso(UMON, Monhoc[i].mamon)].tietbd=1;
		}
	//1.Mauthuan
	STRMauthuan *p,*q;		
	for (i=0; i<gTongsoMon; i++)
	{
		p=Monhoc[i].mauthuan;		
		while (p!=NULL)
		{
			q=p;
			p=p->ketiep;
			delete q;			
			dem++;
		}
		Monhoc[i].mauthuan=NULL;
	}
	CString str;
	str.Format("so dung do: %d",dem);
	//AfxMessageBox(str);
	//2. TrangthaiSopt
	for (i=0; i<gTongsoMon; i++)
		TrangthaiSopt[i].mon.RemoveAll();
	TrangthaiSopt.RemoveAll();
	//3. D
	for (i=0; i<gTongsoMon; i++)
		D[i].tiet.RemoveAll();
	D.RemoveAll();
	//4. Trangthai
	for (i=0; i<gTongsoMon; i++)
		for (j=0;j<gTongsoMon; j++)
			Trangthai[i].D[j].tiet.RemoveAll();
	for (i=0; i<gTongsoMon; i++)
			Trangthai[i].D.RemoveAll();
	Trangthai.RemoveAll();
	//5. TrangthaiDSttxet
	for (i=0; i<gTongsoMon; i++)
		TrangthaiDSttxet[i].tt.RemoveAll();
	TrangthaiDSttxet.RemoveAll();
	//6.Monhoc
	for (i=0; i<gTongsoMon; i++)
		Monhoc[i].nhom.RemoveAll();
	Monhoc.RemoveAll();
}

bool CXeplt::Ban(int stt, int tbd, int sotiet)
{
	int i,k=0;
	for (i=tbd;i<tbd+sotiet;i++)
		if (Phonghoc[stt].tiet [i]==1) k++;
	if (k==sotiet) return false;
	else return true;
}

int CXeplt::Bestfit(int sosv, int tbd, int sotiet)
{
	int m=-1000;
	int i,kq;
	
	for (i=0;i<gTongsoPhong;i++)
	{
		if ((Phonghoc[i].succhuacon-sosv>=0)&&!Ban(i,tbd,sotiet))
		{
			m=Phonghoc[i].succhuacon-sosv;
			break;
		}
	}
	if (m==-1000) return -1;
	else
	{
		for (i=0;i<gTongsoPhong;i++)
		{
			if ((Phonghoc[i].succhuacon-sosv>=0)&&!Ban(i,tbd,sotiet))
			{
				
				if (Phonghoc[i].succhuacon-sosv<=m)
				{
					m=Phonghoc[i].succhuacon-sosv;
					kq=i;
				}
			}
		}
		return kq ;
	}
}

int CXeplt::Bestfit(int stt, int sosv, int tbd, int sotiet)
{
	int m=-1000;
	int i,kq;
	
	for (i=0;i<gTongsoPhong;i++)
	{
		if ((Phonghoc[i].daynha==Daynha[stt].daynha)&&(Phonghoc[i].succhuacon-sosv>=0)&&!Ban(i,tbd,sotiet))
		{
			m=Phonghoc[i].succhuacon-sosv;
			break;
		}
	}
	if (m==-1000) return -1;
	else
	{
		for (i=0;i<gTongsoPhong;i++)
		{
			if ((Phonghoc[i].daynha==Daynha[stt].daynha)&&(Phonghoc[i].succhuacon-sosv>=0)&&!Ban(i,tbd,sotiet))
			{
				
				if (Phonghoc[i].succhuacon-sosv<=m)
				{
					m=Phonghoc[i].succhuacon-sosv;
					kq=i;
				}
			}
		}
		return kq ;
	}
}

bool CXeplt::Chonphong(int stt, int tietbd, int sotiet)
{
	CDlgCachchay dlg;
	int i,j,num,k,sosv;
	int sisonhom=0;
	int kq;
	int sonhom=Monhoc[stt].nhom.GetSize();
	int p1=0,p2=0;
	Sort(stt);
	sosv=Monhoc[stt].nhom [0].siso ;
	
	//Truong hop chay co khoang cach
	if (m_khoangcach==1)
	{
	//Truong hop chay co khoang cach cho BestFit
	if (m_bestfit==1)
	{
	num=Next(0,Monhoc[stt].nhom[0].siso ,tietbd,sotiet);
	if (num!=-1)
	{
		while (!Empty(stt))
		{
			if (Empty(num,tietbd,sotiet)||Bestfit(num,Monhoc[stt].nhom [p1].siso ,tietbd,sotiet)==-1) 
				num=Next(num,Monhoc[stt].nhom [p1].siso,tietbd,sotiet);
			if (num!=-1)
			{	
				
				for(i=p1;i<sonhom;i++)
				{
					while (kq=Bestfit(num,sosv,tietbd,sotiet) != -1)
					{
						p2++;
						if (p2 < sonhom)
							sosv+=Monhoc[stt].nhom[p2].siso;
						else 
							break;
					}
					if (p2==sonhom)
					{
						kq=Bestfit(num,sosv,tietbd,sotiet);
						for (j=p1;j<p2;j++)
						{
							Monhoc[stt].nhom[j].tenphong=Phonghoc[kq].tenphong;
							sisonhom+=Monhoc[stt].nhom [j].siso ;
						}
						Phonghoc[kq].succhuacon=Phonghoc[kq].succhuacon-sisonhom;
						sisonhom=0;
						//for (k=tietbd;k<tietbd+sotiet;k++)
						//	Phonghoc[kq].tiet[k]=0;

						break;
					}
					else
					{
						kq=Bestfit(num,sosv-Monhoc[stt].nhom [p2].siso ,tietbd,sotiet);
						for (j=p1;j<p2;j++)
						{
							Monhoc[stt].nhom[j].tenphong=Phonghoc[kq].tenphong;
							sisonhom+=Monhoc[stt].nhom [j].siso ;
						}
						Phonghoc[kq].succhuacon=Phonghoc[kq].succhuacon-sisonhom;
						sisonhom=0;
						//for (k=tietbd;k<tietbd+sotiet;k++)
						//	Phonghoc[kq].tiet[k]=0;
						p1=p2;
						sosv=Monhoc[stt].nhom [p1].siso ;				
						break;
					}
				}
			}
			else
			{
				ResetPhong();
				Reset(stt,tietbd,sotiet);
				return false;
			}
		}
		for (i=0;i<sonhom;i++)
		{
			CString phong=Monhoc[stt].nhom [i].tenphong;
			for(j=0;j<gTongsoPhong;j++)
			{
				if (Phonghoc[j].tenphong ==phong)
					for(k=tietbd;k<tietbd+sotiet;k++)
						Phonghoc[j].tiet [k]=0;
			}
		}
		ResetPhong();
		return true;
	}
	else
	{
		ResetPhong();
		Reset(stt,tietbd,sotiet);
		return false;
	}
	}
	//Truong hop chay co khoang cach cho First Fit
	else
	{
	num=Next(0,Monhoc[stt].nhom[0].siso ,tietbd,sotiet);
	if (num!=-1)
	{
		while (!Empty(stt))
		{
			if (Empty(num,tietbd,sotiet)||Firstfit(num,Monhoc[stt].nhom [p1].siso ,tietbd,sotiet)==-1) 
				num=Next(num,Monhoc[stt].nhom [p1].siso,tietbd,sotiet);
			if (num!=-1)
			{	
				
				for(i=p1;i<sonhom;i++)
				{
					while (kq=Firstfit(num,sosv,tietbd,sotiet) != -1)
					{
						p2++;
						if (p2 < sonhom)
							sosv+=Monhoc[stt].nhom[p2].siso;
						else 
							break;
					}
					if (p2==sonhom)
					{
						kq=Firstfit(num,sosv,tietbd,sotiet);
						for (j=p1;j<p2;j++)
						{
							Monhoc[stt].nhom[j].tenphong=Phonghoc[kq].tenphong;
							sisonhom+=Monhoc[stt].nhom [j].siso ;
						}
						Phonghoc[kq].succhuacon=Phonghoc[kq].succhuacon-sisonhom;
						sisonhom=0;
						//for (k=tietbd;k<tietbd+sotiet;k++)
						//	Phonghoc[kq].tiet[k]=0;

						break;
					}
					else
					{
						kq=Firstfit(num,sosv-Monhoc[stt].nhom [p2].siso ,tietbd,sotiet);
						for (j=p1;j<p2;j++)
						{
							Monhoc[stt].nhom[j].tenphong=Phonghoc[kq].tenphong;
							sisonhom+=Monhoc[stt].nhom [j].siso ;
						}
						Phonghoc[kq].succhuacon=Phonghoc[kq].succhuacon-sisonhom;
						sisonhom=0;
						//for (k=tietbd;k<tietbd+sotiet;k++)
						//	Phonghoc[kq].tiet[k]=0;
						p1=p2;
						sosv=Monhoc[stt].nhom [p1].siso ;				
						break;
					}
				}
			}
			else
			{
				ResetPhong();
				Reset(stt,tietbd,sotiet);
				return false;
			}
		}
		for (i=0;i<sonhom;i++)
		{
			CString phong=Monhoc[stt].nhom [i].tenphong;
			for(j=0;j<gTongsoPhong;j++)
			{
				if (Phonghoc[j].tenphong ==phong)
					for(k=tietbd;k<tietbd+sotiet;k++)
						Phonghoc[j].tiet [k]=0;
			}
		}
		ResetPhong();
		return true;
	}
	else
	{
		ResetPhong();
		Reset(stt,tietbd,sotiet);
		return false;
	}	
	}
	}
	// Truong hop sau day la chay khong khoang cach
	else
	{
	// Truong hop sau day la chay khong khoang cach cho Best Fit
	if (m_bestfit==1)
	{
	num=Bestfit(Monhoc[stt].nhom[0].siso ,tietbd,sotiet);
	if (num!=-1)
	{
		while (!Empty(stt))
		{
			if (Bestfit(Monhoc[stt].nhom [p1].siso ,tietbd,sotiet)==-1)
			{
//Xem lai		ResetPhong();
				Reset(stt,tietbd,sotiet);
				return false;
			}
			for(i=p1;i<sonhom;i++)
			{
				while (kq=Bestfit(sosv,tietbd,sotiet) != -1)
				{
					p2++;
					if (p2 < sonhom)
						sosv+=Monhoc[stt].nhom[p2].siso;
					else 
						break;
				}
				if (p2==sonhom)
				{
					kq=Bestfit(sosv,tietbd,sotiet);
					for (j=p1;j<p2;j++)
					{
						Monhoc[stt].nhom[j].tenphong=Phonghoc[kq].tenphong;
						sisonhom+=Monhoc[stt].nhom [j].siso ;
					}
					Phonghoc[kq].succhuacon=Phonghoc[kq].succhuacon-sisonhom;
					sisonhom=0;
					//for (k=tietbd;k<tietbd+sotiet;k++)
					//	Phonghoc[kq].tiet[k]=0;
					break;
				}
				else
				{
					kq=Bestfit(sosv-Monhoc[stt].nhom [p2].siso ,tietbd,sotiet);
					for (j=p1;j<p2;j++)
					{
						Monhoc[stt].nhom[j].tenphong=Phonghoc[kq].tenphong;
						sisonhom+=Monhoc[stt].nhom [j].siso ;
					}
					Phonghoc[kq].succhuacon=Phonghoc[kq].succhuacon-sisonhom;
					sisonhom=0;
					//for (k=tietbd;k<tietbd+sotiet;k++)
					//	Phonghoc[kq].tiet[k]=0;
					p1=p2;
					sosv=Monhoc[stt].nhom [p1].siso ;				
					break;
				}
			}
		}
		for (i=0;i<sonhom;i++)
		{
			CString phong=Monhoc[stt].nhom [i].tenphong;
			for(j=0;j<gTongsoPhong;j++)
			{
				if (Phonghoc[j].tenphong ==phong)
					for(k=tietbd;k<tietbd+sotiet;k++)
						Phonghoc[j].tiet [k]=0;
			}
		}
		ResetPhong();
		return true;
	}//End if cua truong hop if (num!=-1)
	else //else cua truong hop if (num!=-1)
	{
		ResetPhong();
		Reset(stt,tietbd,sotiet);
		return false;
	}
	
	}//end if cua truong hop chay khong khoang cach cho Best fit

	// Truong hop sau day la chay khong khoang cach cho First Fit
	else
	{
	num=Firstfit(Monhoc[stt].nhom[0].siso ,tietbd,sotiet);
	if (num!=-1)
	{
		while (!Empty(stt))
		{
			if (Firstfit(Monhoc[stt].nhom [p1].siso ,tietbd,sotiet)==-1)
			{
				ResetPhong();
				Reset(stt,tietbd,sotiet);
				return false;
			}
			for(i=p1;i<sonhom;i++)
			{
				while (kq=Firstfit(sosv,tietbd,sotiet) != -1)
				{
					p2++;
					if (p2 < sonhom)
						sosv+=Monhoc[stt].nhom[p2].siso;
					else 
						break;
				}
				if (p2==sonhom)
				{
					kq=Firstfit(sosv,tietbd,sotiet);
					for (j=p1;j<p2;j++)
					{
						Monhoc[stt].nhom[j].tenphong=Phonghoc[kq].tenphong;
						sisonhom+=Monhoc[stt].nhom [j].siso ;
					}
					Phonghoc[kq].succhuacon=Phonghoc[kq].succhuacon-sisonhom;
					sisonhom=0;
					//for (k=tietbd;k<tietbd+sotiet;k++)
					//	Phonghoc[kq].tiet[k]=0;
					break;
				}
				else
				{
					kq=Firstfit(sosv-Monhoc[stt].nhom [p2].siso ,tietbd,sotiet);
					for (j=p1;j<p2;j++)
					{
						Monhoc[stt].nhom[j].tenphong=Phonghoc[kq].tenphong;
						sisonhom+=Monhoc[stt].nhom [j].siso ;
					}
					Phonghoc[kq].succhuacon=Phonghoc[kq].succhuacon-sisonhom;
					sisonhom=0;
					//for (k=tietbd;k<tietbd+sotiet;k++)
					//	Phonghoc[kq].tiet[k]=0;
					p1=p2;
					sosv=Monhoc[stt].nhom [p1].siso ;				
					break;
				}
			}
		}
		for (i=0;i<sonhom;i++)
		{
			CString phong=Monhoc[stt].nhom [i].tenphong;
			for(j=0;j<gTongsoPhong;j++)
			{
				if (Phonghoc[j].tenphong ==phong)
					for(k=tietbd;k<tietbd+sotiet;k++)
						Phonghoc[j].tiet [k]=0;
			}
		}
		ResetPhong();
		return true;
	}//End if cua truong hop if (num!=-1)
	else //else cua truong hop if (num!=-1)
	{
		ResetPhong();
		Reset(stt,tietbd,sotiet);
		return false;
	}

	}//end else cua truong hop chay khong khoang cach cho First Fit
	}// end else cua truong hop chay khong khoang cach
}

bool CXeplt::Empty(int stt)
{
	int k=0;
	int i;
	int sonhom=Monhoc[stt].nhom.GetSize();
	for (i=0;i<sonhom;i++)
		if (Monhoc[stt].nhom [i].tenphong !="") k++;
	if (k==sonhom) return true;
	else return false;
}

bool CXeplt::Empty(int stt, int tbd, int sotiet)
{
	int i,m,j;
	int k=0;
	for(i=0;i<gTongsoPhong;i++)
	{
		if (Phonghoc[i].daynha ==Daynha[stt].daynha )
		{
			m=0;
			for(j=tbd;j<tbd+sotiet;j++)
				if (Phonghoc[i].tiet [j]==0) m++;
			if (m!=0) k++;
		}
			
	}
	if (k==Daynha[stt].sophong ) return true;
	else return false;
}

int CXeplt::Firstfit(int stt, int sosv, int tbd, int sotiet)
{
	for (int i=0;i<gTongsoPhong;i++)
	{
		if ((Phonghoc[i].daynha==Daynha[stt].daynha)&&(Phonghoc[i].succhuacon-sosv>=0)&&!Ban(i,tbd,sotiet))
		{
			return i;			
		}
	}
	return -1;
}

int CXeplt::Firstfit(int sosv, int tbd, int sotiet)
{
	for (int i=0;i<gTongsoPhong;i++)
	{
		if ((Phonghoc[i].succhuacon-sosv>=0)&&!Ban(i,tbd,sotiet))
		{
			return i;			
		}
	}
	return -1;
}

int CXeplt::Next(int current, int nhommax, int tbd, int sotiet)
{
	int i,j,tmp;
	int tam[15];
	int sodaynha=15;	
	for (i=0;i<sodaynha;i++)	tam[i]=i;
	for (i=0;i<sodaynha-1;i++)
		for (j=i+1;j<sodaynha;j++)
			if (khoangcach[current][i] > khoangcach[current][j])
			{
				tmp=tam[i];
				tam[i]=tam[j];
				tam[j]=tmp;
			}
	if (m_bestfit==1)
	{
	for(i=0;i<sodaynha;i++)
		if(!Empty(tam[i],tbd,sotiet)&&Bestfit(tam[i],nhommax,tbd,sotiet)!=-1) 
		{ 
			return tam[i];
			break;
		}
	}
	else
	{
	for(i=0;i<sodaynha;i++)
		if(!Empty(tam[i],tbd,sotiet)&&Firstfit(tam[i],nhommax,tbd,sotiet)!=-1) 
		{ 
			return tam[i];
			break;
		}
	}
	return -1; 
}

void CXeplt::Reset(int stt, int tbd, int sotiet)
{
	int i,j,k;
	CString phong;
	int sonhom=Monhoc[stt].nhom.GetSize();
	for (i=0;i<sonhom;i++)
	{
		if ((phong=Monhoc[stt].nhom [i].tenphong )!="")
		{
			for(j=0;j<gTongsoPhong;j++)
			{
				if (Phonghoc[j].tenphong ==phong)
					for(k=tbd;k<tbd+sotiet;k++)
						Phonghoc[j].tiet [k]=1;
			}

			Monhoc[stt].nhom [i].tenphong ="";
		}
	}
}

void CXeplt::ResetPhong()
{
	for (int i=0; i<gTongsoPhong; i++)
		Phonghoc[i].succhuacon=Phonghoc[i].succhua ;
}

void CXeplt::Sort(int stt)
{
	STRNhom tmp;
	int sonhom=Monhoc[stt].nhom.GetSize();
	int i,j;
	for (i=0;i<sonhom-1;i++)
		for (j=i+1;j<sonhom;j++)
		{
			if (Monhoc[stt].nhom [i].siso <Monhoc[stt].nhom [j].siso )
			{
				
				tmp=Monhoc[stt].nhom [i];
				Monhoc[stt].nhom [i]=Monhoc[stt].nhom [j];
				Monhoc[stt].nhom [j]=tmp;
			}
		}
}

void CXeplt::UnloadPhong(int mon)
{
	for(int i=0;i<Monhoc[mon].nhom.GetSize();i++)
	{
		int j;				
		for(j=0;j<gTongsoPhong;j++)			
			if (Phonghoc[j].tenphong==Monhoc[mon].nhom[i].tenphong)
				break;					
	
		for(int u=Monhoc[mon].tietbd;u<Monhoc[mon].tietbd+Monhoc[mon].sotietthi;u++)
		{
			Phonghoc[j].tiet[u]=0;
		}						
	}
}

BOOL CXeplt::FwChecking2(int kctiet, CMh *mon, int somt, int tietbd, int sotietthi)
{
#ifdef DEBUG
	if (mon->soptcon==0)
		{	
			AfxMessageBox("soptcon = 0 trong FWCHECK");								
		}
#endif

	
	int i;
	tietbd=tietbd-kctiet;
	if (tietbd<0)
		tietbd=0;
	sotietthi+=kctiet*2;	
	i=tietbd-1;
	int ttmon=mon->sott;
	while ((i>=0)&&(D[ttmon].tiet[i]>0))
	{		
		i--;
	}
	int btrai;
	if (i<0) 
		btrai=0;	
	else
		btrai=i+1;	
	i=tietbd+sotietthi;
	while ((i<gTongsoTiet)&&(D[ttmon].tiet[i]>0))
	{	
		i++;
	}
	int bphai;		
	if (i>gTongsoTiet-1) 
		bphai=gTongsoTiet-1;
	else
		bphai=i-1;

	
	int	soptdau=Dempt(ttmon,Monhoc[ttmon].sotietthi,btrai,bphai);

		
	//bo:int soptdau=Dempt(ttmon,Monhoc[ttmon].sotietthi,btrai,bphai);
	//gan xuong diem am
	int  sautietcc=tietbd+sotietthi;
	if (sautietcc>gTongsoTiet-1)
		sautietcc=gTongsoTiet;
	for (i=tietbd;i<sautietcc;i++)
	{
//debug TH mientri co gt =0
	//		if(D[ttmon].tiet[i]==0)
	//	MessageBox("tata");
//REMIND: neu tai vt do la so 0 thi khong tru, neu dang la duong thi lay 0 - so do.
		if (D[ttmon].tiet[i]>0)
			D[ttmon].tiet[i]=-somt;
		else if( D[ttmon].tiet[i]<0)
	//		if(D[ttmon].tiet==0) thi ko tru else tru
		D[ttmon].tiet[i]-=(somt+ HSPHAT); //- them 1 la do khoi tao ban dau =1
//REMIND: nen co he so phat neu dung do nhieu mon
	}
	//if ()
	//Lay so pt con lai
//
//	debug_D();
	/*
	int soptsau=Demsopt.Dem(Monhoc[ttmon].sotietthi,D[ttmon].tiet[ivtbdkhoi],\
				D[ttmon].tiet[ivtbdkhoi+1],D[ttmon].tiet[ivtbdkhoi+2],\
				D[ttmon].tiet[ivtbdkhoi+3],D[ttmon].tiet[ivtbdkhoi+4],\
				D[ttmon].tiet[ivtbdkhoi+5]);
	*/
	int	soptsau=Dempt(ttmon,Monhoc[ttmon].sotietthi,btrai,bphai);
	int soptmat;
//	CString tam;
//	tam.Format("tietbd la %d va sotietthi la %d", tietbd, sotietthi);
//	AfxMessageBox(tam);
	soptmat=soptdau-soptsau;
//REMIND: coi lai ASSERT
	//ASSERT(soptmat);
	if (soptmat==Monhoc[ttmon].soptcon)  //neu  soptmat > ptcon thi co van de
	{
		
		//AfxMessageBox("soptmat");
		return FALSE;
	}
	
	Monhoc[ttmon].soptcon-=soptmat; 
	int soptcon=Monhoc[ttmon].soptcon;
	return TRUE;
}
