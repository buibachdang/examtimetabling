// DlgNhaptietban.cpp : implementation file
//

#include "stdafx.h"
#include "xeplichthi.h"
#include "DlgNhaptietban.h"
#include "khaibao.h"
#include "Xeplt.h"
//extern CTime ngaybatdau,ngayketthuc;
//extern CTietthi Tietthi;


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CDlgNhaptietban dialog
extern CXeplt Xeplt;

CDlgNhaptietban::CDlgNhaptietban(CWnd* pParent /*=NULL*/)
	: CBitmapDialog(CDlgNhaptietban::IDD, pParent)
{
	//{{AFX_DATA_INIT(CDlgNhaptietban)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void CDlgNhaptietban::DoDataExchange(CDataExchange* pDX)
{
	CBitmapDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDlgNhaptietban)
	DDX_Control(pDX, IDC_TATCATIET, m_tatcatiet);
	//}}AFX_DATA_MAP
	for (int i=0;i<12;i++)
	{
		//DDX_Check(pDX,IDC_TIET1+i,m_tiet[i]);
		DDX_Control(pDX,IDC_TIET1+i,m_tiet[i]);
	}
}


BEGIN_MESSAGE_MAP(CDlgNhaptietban, CBitmapDialog)
	//{{AFX_MSG_MAP(CDlgNhaptietban)
	//}}AFX_MSG_MAP
	ON_CONTROL_RANGE(BN_CLICKED,IDC_TIET1,IDC_TATCATIET,OnTiet)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDlgNhaptietban message handlers

BOOL CDlgNhaptietban::OnInitDialog() 
{
	CBitmapDialog::OnInitDialog();
	SetBitmap(IDB_BITMAP1,BITMAP_TILE);
	int i,tam,dem=0;
	if (select==true)
	{
		SetWindowText("Nhap tiet ban cho ngay  "+title);
		if (ngaythiban==1) 
		{
			for (i=0;i<12;i++)
			{	
				if(Xeplt.Tietthi[i].ban == TRUE) 
				{
					m_tiet[i].SetCheck(1);
					dem++;
				}
				else
					m_tiet[i].SetCheck(0);
			}
			if (dem==12) m_tatcatiet.SetCheck(1);
			else m_tatcatiet.SetCheck(0);
		}
		
		else 
		{
			for  (i=(ngaythiban-1)*12;i<((int)ngaythiban-1)*12+12;i++)
			{
				tam=i%12;
				if (Xeplt.Tietthi[i].ban ==TRUE)
				{
					dem++;
					m_tiet[tam].SetCheck(1);
				}
				else m_tiet[tam].SetCheck(0);
			}
			if (dem==12) m_tatcatiet.SetCheck(1);
			else m_tatcatiet.SetCheck(0);
		}
	}
	else
	{
		SetWindowText("Nhap tiet ban cho tat ca cac ngay");
	}
	UpdateData(FALSE);
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}
void CDlgNhaptietban::OnTiet(UINT nID) 
{
	//int index=nID-1005; //Search xem gia tri cua cac IDC_TIET la hieu vi sao o day la 1005
	
	if (nID==IDC_TATCATIET) 
		if (m_tatcatiet.GetCheck()==0)
			for(int i=0;i<12;i++)
				m_tiet[i].SetCheck(0);
		else 
			for(int i=0;i<12;i++)
				m_tiet[i].SetCheck(1);
}

void CDlgNhaptietban::OnOK() 
{
	int i,j,tietorder;
	if (select==true)
	{
		for (i=0;i<12;i++)
		{
			if (m_tiet[i].GetCheck()==1)
			{
				tietorder=(ngaythiban-1)*12+i;
				Xeplt.Tietthi[tietorder].ban= TRUE;
			}
			else
			{
				tietorder=(ngaythiban-1)*12+i;
				Xeplt.Tietthi[tietorder].ban=FALSE;	
			}
	
		}
	}
	else
	{
		CTimeSpan tam=Xeplt.ngayketthuc-Xeplt.ngaybatdau;
		int songay=atoi(tam.Format("%D")) +1;
		for (i=0;i<songay;i++)
		{
			for (j=0;j<12;j++)
			{
				if (m_tiet[j].GetCheck()==1)
				{
					tietorder=i*12+j;
					Xeplt.Tietthi[tietorder].ban=TRUE;
				}
				/*
				else
				{
					tietorder=i*12+j;
					Xeplt.Tietthi[tietorder].ban=FALSE;	
				}
				*/
			}
		}

	}
	
	
	UpdateData(FALSE);	
	CBitmapDialog::OnOK();
}
