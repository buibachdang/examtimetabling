// SetSotay.cpp : implementation file
//

#include "stdafx.h"
#include "XepLichThi.h"
#include "SetSotay.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

extern CString dgdan;
/////////////////////////////////////////////////////////////////////////////
// CSetSotay

IMPLEMENT_DYNAMIC(CSetSotay, CDaoRecordset)

CSetSotay::CSetSotay(CDaoDatabase* pdb)
	: CDaoRecordset(pdb)
{
	//{{AFX_FIELD_INIT(CSetSotay)
	m_Mamonhoc = _T("");
	m_Dept = 0;
	m_Khoahoc = _T("");
	m_Sotietthi = 0;
	m_Sosvdk = 0;
	m_nFields = 5;
	//}}AFX_FIELD_INIT
	m_nDefaultType = dbOpenDynaset;
}


CString CSetSotay::GetDefaultDBName()
{
	return dgdan;
}

CString CSetSotay::GetDefaultSQL()
{
	return _T("[SotaySV]");
}

void CSetSotay::DoFieldExchange(CDaoFieldExchange* pFX)
{
	//{{AFX_FIELD_MAP(CSetSotay)
	pFX->SetFieldType(CDaoFieldExchange::outputColumn);
	DFX_Text(pFX, _T("[Mamonhoc]"), m_Mamonhoc);
	DFX_Short(pFX, _T("[Dept]"), m_Dept);
	DFX_Text(pFX, _T("[Khoahoc]"), m_Khoahoc);
	DFX_Short(pFX, _T("[Sotietthi]"), m_Sotietthi);
	DFX_Short(pFX, _T("[Sosvdk]"), m_Sosvdk);
	//}}AFX_FIELD_MAP
}

/////////////////////////////////////////////////////////////////////////////
// CSetSotay diagnostics

#ifdef _DEBUG
void CSetSotay::AssertValid() const
{
	CDaoRecordset::AssertValid();
}

void CSetSotay::Dump(CDumpContext& dc) const
{
	CDaoRecordset::Dump(dc);
}
#endif //_DEBUG
