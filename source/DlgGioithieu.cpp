// DlgGioithieu.cpp : implementation file
//

#include "stdafx.h"
#include "XepLichThi.h"
#include "DlgGioithieu.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CDlgGioithieu dialog


CDlgGioithieu::CDlgGioithieu(CWnd* pParent /*=NULL*/)
	: CBitmapDialog(CDlgGioithieu::IDD, pParent)
{
	//{{AFX_DATA_INIT(CDlgGioithieu)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void CDlgGioithieu::DoDataExchange(CDataExchange* pDX)
{
	CBitmapDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDlgGioithieu)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CDlgGioithieu, CBitmapDialog)
	//{{AFX_MSG_MAP(CDlgGioithieu)	
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDlgGioithieu message handlers

BOOL CDlgGioithieu::OnInitDialog() 
{
//	b_Ok=new CBitmapButton;
	CBitmapDialog::OnInitDialog();
	
	b_Ok.Create ( "", WS_VISIBLE  | BS_OWNERDRAW, CRect ( 370, 275, 462, 345), this, 1 ) ;
	b_Ok.LoadBitmaps ( IDB_PUSHUP, IDB_PUSHDOWN) ; 

	SetBitmap(IDB_BITMAP1,BITMAP_TILE);
	
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}
