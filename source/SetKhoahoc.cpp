// SetKhoahoc.cpp : implementation file
//

#include "stdafx.h"
#include "XepLichThi.h"
#include "SetKhoahoc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

extern CString dgdan;
/////////////////////////////////////////////////////////////////////////////
// CSetKhoahoc

IMPLEMENT_DYNAMIC(CSetKhoahoc, CDaoRecordset)

CSetKhoahoc::CSetKhoahoc(CDaoDatabase* pdb)
	: CDaoRecordset(pdb)
{
	//{{AFX_FIELD_INIT(CSetKhoahoc)
	m_Khoahoc = _T("");
	m_nFields = 1;
	//}}AFX_FIELD_INIT
	m_nDefaultType = dbOpenDynaset;
}


CString CSetKhoahoc::GetDefaultDBName()
{
	return dgdan;
}

CString CSetKhoahoc::GetDefaultSQL()
{
	return _T("[KhoahocQuery]");
}

void CSetKhoahoc::DoFieldExchange(CDaoFieldExchange* pFX)
{
	//{{AFX_FIELD_MAP(CSetKhoahoc)
	pFX->SetFieldType(CDaoFieldExchange::outputColumn);
	DFX_Text(pFX, _T("[Khoahoc]"), m_Khoahoc);
	//}}AFX_FIELD_MAP
}

/////////////////////////////////////////////////////////////////////////////
// CSetKhoahoc diagnostics

#ifdef _DEBUG
void CSetKhoahoc::AssertValid() const
{
	CDaoRecordset::AssertValid();
}

void CSetKhoahoc::Dump(CDumpContext& dc) const
{
	CDaoRecordset::Dump(dc);
}
#endif //_DEBUG
