// SetBangdungdo.cpp : implementation file
//

#include "stdafx.h"
#include "xeplichthi.h"
#include "SetBangdungdo.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CSetBangdungdo
extern CString dgdan;
IMPLEMENT_DYNAMIC(CSetBangdungdo, CDaoRecordset)

CSetBangdungdo::CSetBangdungdo(CDaoDatabase* pdb)
	: CDaoRecordset(pdb)
{
	//{{AFX_FIELD_INIT(CSetBangdungdo)
	m_MaMon1 = _T("");
	m_MaMon2 = _T("");
	m_SoDungDo = 0;
	m_nFields = 3;
	//}}AFX_FIELD_INIT
	m_nDefaultType = dbOpenDynaset;
}


CString CSetBangdungdo::GetDefaultDBName()
{
	return dgdan;
}

CString CSetBangdungdo::GetDefaultSQL()
{
	return _T("[BangDungDo]");
//REMIND: Sua thanh BangdungdoQuery
}

void CSetBangdungdo::DoFieldExchange(CDaoFieldExchange* pFX)
{
	//{{AFX_FIELD_MAP(CSetBangdungdo)
	pFX->SetFieldType(CDaoFieldExchange::outputColumn);
	DFX_Text(pFX, _T("[MaMon1]"), m_MaMon1);
	DFX_Text(pFX, _T("[MaMon2]"), m_MaMon2);
	DFX_Long(pFX, _T("[SoDungDo]"), m_SoDungDo);
	//}}AFX_FIELD_MAP
}

/////////////////////////////////////////////////////////////////////////////
// CSetBangdungdo diagnostics

#ifdef _DEBUG
void CSetBangdungdo::AssertValid() const
{
	CDaoRecordset::AssertValid();
}

void CSetBangdungdo::Dump(CDumpContext& dc) const
{
	CDaoRecordset::Dump(dc);
}
#endif //_DEBUG
