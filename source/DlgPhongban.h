#if !defined(AFX_DLGPHONGBAN_H__8DD04124_9692_4BB4_B82C_DAE4D2A1A550__INCLUDED_)
#define AFX_DLGPHONGBAN_H__8DD04124_9692_4BB4_B82C_DAE4D2A1A550__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DlgPhongban.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CDlgPhongban dialog
//#include "BitmapDialog.h"
#include "BDialog.h"

class CDlgPhongban : public CBDialog
{
// Construction
public:
	~CDlgPhongban();
	CToolTipCtrl * tooltip;
	CDlgPhongban(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CDlgPhongban)
	enum { IDD = IDD_DLGPHONGBAN };
	CTreeCtrl	m_tree;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDlgPhongban)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CDlgPhongban)
	virtual BOOL OnInitDialog();
	afx_msg void OnDblclkTree(NMHDR* pNMHDR, LRESULT* pResult);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DLGPHONGBAN_H__8DD04124_9692_4BB4_B82C_DAE4D2A1A550__INCLUDED_)
