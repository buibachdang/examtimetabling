; CLW file contains information for the MFC ClassWizard

[General Info]
Version=1
LastClass=CDlgTiendo
LastTemplate=CDialog
NewFileInclude1=#include "stdafx.h"
NewFileInclude2=#include "xeplichthi.h"
LastPage=0

ClassCount=22
Class1=CDlgChonMh
Class2=CDlgChonTietUT
Class3=CDlgGioithieu
Class4=CMainFrame
Class5=CSetKhoahoc
Class6=CSetPhong
Class7=CSetSotay
Class8=CXepLichThiApp
Class9=CAboutDlg
Class10=CXepLichThiDoc
Class11=CXepLichThiView

ResourceCount=24
Resource1=IDD_DLGPHONGBAN
Resource2=IDD_DLGCHONMH
Resource3=IDD_ABOUTBOX
Resource4=IDD_DLGGIOITHIEU
Resource5=IDD_DLGNHAPTHOIGIAN
Resource6=IDD_DLGCHONTIETUT
Class12=CDlgPhongban
Class13=CSetDaynha
Resource7=IDD_XEPLICHTHI_FORM
Class14=CDlgThongtinPhong
Resource8=IDD_DLGTHONGTINPHONG
Class15=CDlgNhapthoigian
Resource9=IDR_MAINFRAME
Class16=CDlgNhaptietban
Resource10=IDD_DLGNHAPTIETBAN
Resource11=IDD_DLGCANHBAO
Resource12=IDD_DLGGIOITHIEU (English (U.S.))
Resource13=IDD_DLGNHAPTIETBAN (English (U.S.))
Resource14=IDD_DLGPHONGBAN (English (U.S.))
Resource15=IDD_DLGTHONGTINPHONG (English (U.S.))
Resource16=IDD_XEPLICHTHI_FORM (English (U.S.))
Resource17=IDD_DLGCHONTIETUT (English (U.S.))
Resource18=IDD_DLGHIENTHIKQ
Resource19=IDD_DLGNHAPTHOIGIAN (English (U.S.))
Class17=CSetBangdungdo
Class18=CSetNhom
Resource20=IDD_DLGCACHCHAY
Class19=CDlgHienthikq
Resource21=IDR_MAINFRAME (English (U.S.))
Class20=CDlgCanhbao
Resource22=IDD_DLGCHONMH (English (U.S.))
Class21=CDlgCachchay
Resource23=IDD_ABOUTBOX (English (U.S.))
Class22=CDlgTiendo
Resource24=IDD_DLGTIENDO

[CLS:CDlgChonMh]
Type=0
BaseClass=CDialog
HeaderFile=DlgChonMh.h
ImplementationFile=DlgChonMh.cpp
LastObject=CDlgChonMh

[CLS:CDlgChonTietUT]
Type=0
BaseClass=CBitmapDialog
HeaderFile=DlgChonTietUT.h
ImplementationFile=DlgChonTietUT.cpp
Filter=D
VirtualFilter=dWC
LastObject=IDC_CHECK_RANGBUOC

[CLS:CDlgGioithieu]
Type=0
BaseClass=CBitmapDialog
HeaderFile=DlgGioithieu.h
ImplementationFile=DlgGioithieu.cpp
Filter=D
VirtualFilter=dWC
LastObject=CDlgGioithieu

[CLS:CMainFrame]
Type=0
BaseClass=CFrameWnd
HeaderFile=MainFrm.h
ImplementationFile=MainFrm.cpp

[CLS:CSetKhoahoc]
Type=0
HeaderFile=SetKhoahoc.h
ImplementationFile=SetKhoahoc.cpp

[CLS:CSetPhong]
Type=0
HeaderFile=SetPhong.h
ImplementationFile=SetPhong.cpp
LastObject=CSetPhong

[CLS:CSetSotay]
Type=0
HeaderFile=SetSotay.h
ImplementationFile=SetSotay.cpp

[CLS:CXepLichThiApp]
Type=0
BaseClass=CWinApp
HeaderFile=XepLichThi.h
ImplementationFile=XepLichThi.cpp
LastObject=CXepLichThiApp

[CLS:CAboutDlg]
Type=0
BaseClass=CDialog
HeaderFile=XepLichThi.cpp
ImplementationFile=XepLichThi.cpp
LastObject=CAboutDlg
Filter=D
VirtualFilter=dWC

[CLS:CXepLichThiDoc]
Type=0
BaseClass=CDocument
HeaderFile=XepLichThiDoc.h
ImplementationFile=XepLichThiDoc.cpp

[CLS:CXepLichThiView]
Type=0
BaseClass=CFormView
HeaderFile=XepLichThiView.h
ImplementationFile=XepLichThiView.cpp
Filter=D
VirtualFilter=VWC
LastObject=CXepLichThiView

[DB:CSetKhoahoc]
DB=1

[DB:CSetPhong]
DB=1

[DB:CSetSotay]
DB=1

[DLG:IDD_DLGCHONMH]
Type=1
Class=CDlgChonMh
ControlCount=9
Control1=IDOK,button,1342242817
Control2=IDC_LIST_MON1,listbox,1352730883
Control3=IDC_LIST_MON2,listbox,1352730883
Control4=IDC_BUTTON_QUAPHAI,button,1342242816
Control5=IDC_BUTTON_QUATRAI,button,1342242816
Control6=IDC_STATIC,button,1342177287
Control7=IDC_BUTTON_CHITIET2,button,1342242816
Control8=IDC_STATIC,static,1342308352
Control9=IDC_STATIC,static,1342308352

[DLG:IDD_DLGCHONTIETUT]
Type=1
Class=CDlgChonTietUT
ControlCount=13
Control1=IDOK,button,1342242817
Control2=IDC_RADIO1,button,1342308361
Control3=IDC_RADIO_NGAYCD,button,1342308361
Control4=IDC_STATIC,button,1342177287
Control5=IDC_DATETIMEPICKER_NGAYBD,SysDateTimePick32,1342242848
Control6=IDC_DATETIMEPICKER_NGAYKT,SysDateTimePick32,1342242848
Control7=IDC_STATIC_NGAYBD,static,1342308352
Control8=IDC_STATIC_NGAYKT,static,1342308352
Control9=IDC_STATIC_NGAYCD,static,1342308352
Control10=IDC_DATETIMEPICKER_NGAYCD,SysDateTimePick32,1342242848
Control11=IDC_STATIC_TIETBD,static,1342308352
Control12=IDC_EDIT_TIETBATDAU,edit,1350631552
Control13=IDC_STATIC_MAMON,static,1342308353

[DLG:IDD_DLGGIOITHIEU]
Type=1
Class=CDlgGioithieu
ControlCount=1
Control1=IDOK,button,1342242817

[DLG:IDD_ABOUTBOX]
Type=1
Class=CAboutDlg
ControlCount=4
Control1=IDC_STATIC,static,1342177283
Control2=IDC_STATIC,static,1342308480
Control3=IDC_STATIC,static,1342308352
Control4=IDOK,button,1342373889

[DLG:IDD_XEPLICHTHI_FORM]
Type=1
Class=CXepLichThiView
ControlCount=13
Control1=IDC_BUTTON_DUONGDAN,button,1342242816
Control2=IDC_EDIT_DUONGDAN,edit,1484849280
Control3=IDC_LIST_CHONKHOA,listbox,1352728915
Control4=IDC_STATIC_CHONKHOA,button,1342177287
Control5=IDC_BUTTON_KHOIDONG,button,1342242816
Control6=IDC_BUTTON_CHAY,button,1342242816
Control7=IDC_BUTTON_CHITIET,button,1342242816
Control8=IDC_BUTTON_NHAPTIETBAN,button,1342242816
Control9=IDC_STATIC,static,1342308352
Control10=IDC_STATIC,static,1342308352
Control11=IDC_EDIT_NGAYBATDAU,edit,1484849280
Control12=IDC_EDIT_NGAYKETTHUC,edit,1484849280
Control13=IDC_BUTTON_NHAPTHOIGIAN,button,1342242816

[MNU:IDR_MAINFRAME]
Type=1
Class=?
Command1=ID_FILE_OPEN
Command2=ID_FILE_SAVE
Command3=ID_FILE_SAVE_AS
Command4=ID_FILE_PRINT
Command5=ID_FILE_PRINT_PREVIEW
Command6=ID_FILE_PRINT_SETUP
Command7=ID_FILE_MRU_FILE1
Command8=ID_APP_EXIT
Command9=ID_APP_ABOUT
CommandCount=9

[TB:IDR_MAINFRAME]
Type=1
Class=?
Command1=ID_FILE_OPEN
Command2=ID_FILE_SAVE
Command3=ID_FILE_PRINT
CommandCount=3

[ACL:IDR_MAINFRAME]
Type=1
Class=?
Command1=ID_FILE_NEW
Command2=ID_FILE_OPEN
Command3=ID_FILE_SAVE
Command4=ID_FILE_PRINT
Command5=ID_EDIT_UNDO
Command6=ID_EDIT_CUT
Command7=ID_EDIT_COPY
Command8=ID_EDIT_PASTE
Command9=ID_EDIT_UNDO
Command10=ID_EDIT_CUT
Command11=ID_EDIT_COPY
Command12=ID_EDIT_PASTE
Command13=ID_NEXT_PANE
Command14=ID_PREV_PANE
CommandCount=14

[DLG:IDD_DLGPHONGBAN]
Type=1
Class=CDlgPhongban
ControlCount=3
Control1=IDOK,button,1342242817
Control2=IDCANCEL,button,1342242816
Control3=IDC_TREE,SysTreeView32,1350631431

[CLS:CDlgPhongban]
Type=0
HeaderFile=DlgPhongban.h
ImplementationFile=DlgPhongban.cpp
BaseClass=CDialog
Filter=D
VirtualFilter=dWC
LastObject=IDC_TREE

[CLS:CSetDaynha]
Type=0
HeaderFile=SetDaynha.h
ImplementationFile=SetDaynha.cpp
BaseClass=CDaoRecordset
Filter=N
VirtualFilter=x

[DB:CSetDaynha]
DB=1
DBType=DAO
ColumnCount=3
Column1=[Daynha], 12, 50
Column2=[Sophong], 5, 2
Column3=[Succhuaday], 5, 2

[DLG:IDD_DLGTHONGTINPHONG]
Type=1
Class=CDlgThongtinPhong
ControlCount=23
Control1=IDOK,button,1342242817
Control2=IDCANCEL,button,1342242816
Control3=IDC_STATIC,static,1342308352
Control4=IDC_EDAYNHA,edit,1484849280
Control5=IDC_STATIC,static,1342308353
Control6=IDC_EPHONG,edit,1484849280
Control7=IDC_STATIC,static,1342308352
Control8=IDC_ESUCCHUA,edit,1350631552
Control9=IDC_PDANHSACHNGAYTHI,listbox,1352728835
Control10=IDC_PTIET1,button,1342242819
Control11=IDC_PTIET2,button,1342242819
Control12=IDC_PTIET3,button,1342242819
Control13=IDC_PTIET4,button,1342242819
Control14=IDC_PTIET5,button,1342242819
Control15=IDC_PTIET6,button,1342242819
Control16=IDC_PTIET7,button,1342242819
Control17=IDC_PTIET8,button,1342242819
Control18=IDC_PTIET9,button,1342242819
Control19=IDC_PTIET10,button,1342242819
Control20=IDC_PTIET11,button,1342242819
Control21=IDC_PTIET12,button,1342242819
Control22=IDC_STATICNGAYTHI,button,1342177287
Control23=IDC_PTATCATIET,button,1342242819

[CLS:CDlgThongtinPhong]
Type=0
HeaderFile=DlgThongtinPhong.h
ImplementationFile=DlgThongtinPhong.cpp
BaseClass=CBitmapDialog
Filter=D
LastObject=CDlgThongtinPhong
VirtualFilter=dWC

[DLG:IDD_DLGNHAPTHOIGIAN]
Type=1
Class=CDlgNhapthoigian
ControlCount=8
Control1=IDOK,button,1342242817
Control2=IDCANCEL,button,1342242816
Control3=IDC_DATETIMEPICKER1,SysDateTimePick32,1342242848
Control4=IDC_STATIC,static,1342308352
Control5=IDC_STATIC,static,1342308352
Control6=IDC_DATETIMEPICKER2,SysDateTimePick32,1342242848
Control7=IDC_DANHSACHNGAYTHI,listbox,1352728835
Control8=IDC_ALLDAY,button,1342242819

[CLS:CDlgNhapthoigian]
Type=0
HeaderFile=DlgNhapthoigian.h
ImplementationFile=DlgNhapthoigian.cpp
BaseClass=CBitmapDialog
Filter=D
LastObject=CDlgNhapthoigian
VirtualFilter=dWC

[DLG:IDD_DLGNHAPTIETBAN]
Type=1
Class=CDlgNhaptietban
ControlCount=16
Control1=IDOK,button,1342242817
Control2=IDCANCEL,button,1342242816
Control3=IDC_TIET1,button,1342242819
Control4=IDC_TIET2,button,1342242819
Control5=IDC_TIET3,button,1342242819
Control6=IDC_TIET4,button,1342242819
Control7=IDC_TIET5,button,1342242819
Control8=IDC_TIET6,button,1342242819
Control9=IDC_TIET7,button,1342242819
Control10=IDC_TIET8,button,1342242819
Control11=IDC_TIET9,button,1342242819
Control12=IDC_TIET10,button,1342242819
Control13=IDC_TIET11,button,1342242819
Control14=IDC_TIET12,button,1342242819
Control15=IDC_STATIC,button,1342177287
Control16=IDC_TATCATIET,button,1342242819

[CLS:CDlgNhaptietban]
Type=0
HeaderFile=DlgNhaptietban.h
ImplementationFile=DlgNhaptietban.cpp
BaseClass=CDialog
Filter=D
LastObject=IDC_TATCATIET
VirtualFilter=dWC

[MNU:IDR_MAINFRAME (English (U.S.))]
Type=1
Class=?
Command1=ID_FILE_OPEN
Command2=ID_FILE_SAVE
Command3=ID_FILE_SAVE_AS
Command4=ID_FILE_PRINT
Command5=ID_FILE_PRINT_PREVIEW
Command6=ID_FILE_PRINT_SETUP
Command7=ID_FILE_MRU_FILE1
Command8=ID_APP_EXIT
Command9=ID_APP_ABOUT
CommandCount=9

[DLG:IDD_ABOUTBOX (English (U.S.))]
Type=1
Class=CAboutDlg
ControlCount=4
Control1=IDC_STATIC,static,1342177283
Control2=IDC_STATIC_S,static,1342308480
Control3=IDC_STATIC_A,static,1342308352
Control4=IDOK,button,1342373889

[DLG:IDD_DLGCHONMH (English (U.S.))]
Type=1
Class=CDlgChonMh
ControlCount=10
Control1=IDOK,button,1342242817
Control2=IDC_LIST_MON1,listbox,1352730883
Control3=IDC_LIST_MON2,listbox,1352730883
Control4=IDC_BUTTON_QUAPHAI,button,1342242816
Control5=IDC_BUTTON_QUATRAI,button,1342242816
Control6=IDC_STATIC,button,1342423047
Control7=IDC_BUTTON_CHITIET2,button,1342242816
Control8=IDC_STATIC,static,1342308352
Control9=IDC_STATIC,static,1342308352
Control10=IDC_STATIC,static,1342308353

[DLG:IDD_DLGCHONTIETUT (English (U.S.))]
Type=1
Class=CDlgChonTietUT
ControlCount=15
Control1=IDOK,button,1342242817
Control2=IDC_RADIO_NGAYTD,button,1342177289
Control3=IDC_RADIO_NGAYCD,button,1342177289
Control4=IDC_STATIC_CHON,button,1342226439
Control5=IDC_DATETIMEPICKER_NGAYBD,SysDateTimePick32,1342242848
Control6=IDC_DATETIMEPICKER_NGAYKT,SysDateTimePick32,1342242848
Control7=IDC_STATIC_NGAYBD,static,1342308352
Control8=IDC_STATIC_NGAYKT,static,1342308352
Control9=IDC_STATIC_NGAYCD,static,1342308352
Control10=IDC_DATETIMEPICKER_NGAYCD,SysDateTimePick32,1342242848
Control11=IDC_STATIC_TIETBD,static,1342308352
Control12=IDC_EDIT_TIETBATDAU,edit,1350631552
Control13=IDC_STATIC_MAMON,static,1342308353
Control14=IDC_CHECK_RANGBUOC,button,1342242819
Control15=IDC_STATIC_CHONUT,static,1342308352

[DLG:IDD_DLGGIOITHIEU (English (U.S.))]
Type=1
Class=CDlgGioithieu
ControlCount=3
Control1=IDOK,button,1073807361
Control2=IDC_STATIC,static,1342177294
Control3=IDC_STATIC,static,1342177294

[DLG:IDD_DLGNHAPTIETBAN (English (U.S.))]
Type=1
Class=CDlgNhaptietban
ControlCount=17
Control1=IDOK,button,1342242817
Control2=IDCANCEL,button,1342242816
Control3=IDC_TIET1,button,1342242819
Control4=IDC_TIET2,button,1342242819
Control5=IDC_TIET3,button,1342242819
Control6=IDC_TIET4,button,1342242819
Control7=IDC_TIET5,button,1342242819
Control8=IDC_TIET6,button,1342242819
Control9=IDC_TIET7,button,1342242819
Control10=IDC_TIET8,button,1342242819
Control11=IDC_TIET9,button,1342242819
Control12=IDC_TIET10,button,1342242819
Control13=IDC_TIET11,button,1342242819
Control14=IDC_TIET12,button,1342242819
Control15=IDC_STATIC,button,1342177287
Control16=IDC_TATCATIET,button,1342242819
Control17=IDC_STATIC,static,1342308352

[DLG:IDD_DLGNHAPTHOIGIAN (English (U.S.))]
Type=1
Class=CDlgNhapthoigian
ControlCount=9
Control1=IDOK,button,1342242817
Control2=IDC_DATETIMEPICKER1,SysDateTimePick32,1342242848
Control3=IDC_STATIC,static,1342308352
Control4=IDC_STATIC,static,1342308352
Control5=IDC_DATETIMEPICKER2,SysDateTimePick32,1342242848
Control6=IDC_DANHSACHNGAYTHI,listbox,1352728835
Control7=IDC_ALLDAY,button,1342242819
Control8=IDC_THUBAY,button,1342242819
Control9=IDC_CHUNHAT,button,1342242819

[DLG:IDD_XEPLICHTHI_FORM (English (U.S.))]
Type=1
Class=CXepLichThiView
ControlCount=20
Control1=IDC_BUTTON_DUONGDAN,button,1342242816
Control2=IDC_EDIT_DUONGDAN,edit,1484849281
Control3=IDC_LIST_CHONKHOA,listbox,1352728913
Control4=IDC_STATIC_CHONKHOA,button,1342226439
Control5=IDC_BUTTON_KHOIDONG,button,1073807360
Control6=IDC_BUTTON_CHAY,button,1073807360
Control7=IDC_BUTTON_CHITIET,button,1342242816
Control8=IDC_BUTTON_NHAPTIETBAN,button,1342242816
Control9=IDC_STATIC,static,1342308352
Control10=IDC_STATIC,static,1342308352
Control11=IDC_EDIT_NGAYBATDAU,edit,1484849281
Control12=IDC_EDIT_NGAYKETTHUC,edit,1484849281
Control13=IDC_BUTTON_NHAPTHOIGIAN,button,1342242816
Control14=IDC_STATIC,static,1342308352
Control15=IDC_STATIC,button,1342226439
Control16=IDC_STATIC,static,1342308352
Control17=IDC_BUTTON_PTCHAY,button,1342242816
Control18=IDC_STATIC,static,1073741838
Control19=IDC_BUTTON_RESET,button,1342242816
Control20=IDC_EDIT1,edit,1082196096

[DLG:IDD_DLGTHONGTINPHONG (English (U.S.))]
Type=1
Class=CDlgThongtinPhong
ControlCount=23
Control1=IDC_STATIC,static,1342308352
Control2=IDC_EDAYNHA,edit,1484849280
Control3=IDC_STATIC,static,1342308353
Control4=IDC_EPHONG,edit,1484849280
Control5=IDC_STATIC,static,1342308352
Control6=IDC_ESUCCHUA,edit,1350631552
Control7=IDC_PDANHSACHNGAYTHI,listbox,1352728835
Control8=IDC_PTIET1,button,1342242819
Control9=IDC_PTIET2,button,1342242819
Control10=IDC_PTIET3,button,1342242819
Control11=IDC_PTIET4,button,1342242819
Control12=IDC_PTIET5,button,1342242819
Control13=IDC_PTIET6,button,1342242819
Control14=IDC_PTIET7,button,1342242819
Control15=IDC_PTIET8,button,1342242819
Control16=IDC_PTIET9,button,1342242819
Control17=IDC_PTIET10,button,1342242819
Control18=IDC_PTIET11,button,1342242819
Control19=IDC_PTIET12,button,1342242819
Control20=IDC_STATICNGAYTHI,button,1342177287
Control21=IDC_PTATCATIET,button,1342242819
Control22=IDC_STATIC_NGAY,static,1342308352
Control23=IDC_BUTTON_DONGY,button,1342242816

[DLG:IDD_DLGPHONGBAN (English (U.S.))]
Type=1
Class=CDlgPhongban
ControlCount=3
Control1=IDOK,button,1342242817
Control2=IDCANCEL,button,1073807360
Control3=IDC_TREE,SysTreeView32,1350631431

[TB:IDR_MAINFRAME (English (U.S.))]
Type=1
Class=?
Command1=ID_FILE_OPEN
Command2=ID_FILE_SAVE
Command3=ID_FILE_PRINT
CommandCount=3

[ACL:IDR_MAINFRAME (English (U.S.))]
Type=1
Class=?
Command1=ID_FILE_NEW
Command2=ID_FILE_OPEN
Command3=ID_FILE_SAVE
Command4=ID_FILE_PRINT
Command5=ID_EDIT_UNDO
Command6=ID_EDIT_CUT
Command7=ID_EDIT_COPY
Command8=ID_EDIT_PASTE
Command9=ID_EDIT_UNDO
Command10=ID_EDIT_CUT
Command11=ID_EDIT_COPY
Command12=ID_EDIT_PASTE
Command13=ID_NEXT_PANE
Command14=ID_PREV_PANE
CommandCount=14

[CLS:CSetBangdungdo]
Type=0
HeaderFile=SetBangdungdo.h
ImplementationFile=SetBangdungdo.cpp
BaseClass=CDaoRecordset
Filter=N
VirtualFilter=x

[DB:CSetBangdungdo]
DB=1
DBType=DAO
ColumnCount=3
Column1=[MaMon1], 12, 50
Column2=[MaMon2], 12, 50
Column3=[SoDungDo], 4, 4

[CLS:CSetNhom]
Type=0
HeaderFile=SetNhom.h
ImplementationFile=SetNhom.cpp
BaseClass=CDaoRecordset
Filter=N
VirtualFilter=x
LastObject=CSetNhom

[DB:CSetNhom]
DB=1
DBType=DAO
ColumnCount=4
Column1=[Mamonhoc], 12, 50
Column2=[Nhom], 12, 10
Column3=[To], 12, 10
Column4=[Soluong], 4, 4

[DLG:IDD_DLGHIENTHIKQ]
Type=1
Class=CDlgHienthikq
ControlCount=6
Control1=IDOK,button,1342242817
Control2=IDC_LIST_KQ,listbox,1084293377
Control3=IDC_HAOPHI,button,1073807361
Control4=IDC_HAOPHI2,button,1342242817
Control5=IDC_MSFLEXGRID1,{6262D3A0-531B-11CF-91F6-C2863C385E30},1342242816
Control6=IDC_EDIT_TGCHAY,edit,1484849281

[CLS:CDlgHienthikq]
Type=0
HeaderFile=DlgHienthikq.h
ImplementationFile=DlgHienthikq.cpp
BaseClass=CBDialog
Filter=D
LastObject=IDC_MSFLEXGRID1
VirtualFilter=dWC

[DLG:IDD_DLGCANHBAO]
Type=1
Class=CDlgCanhbao
ControlCount=2
Control1=IDOK,button,1342242817
Control2=IDC_STATIC,static,1342308352

[CLS:CDlgCanhbao]
Type=0
HeaderFile=DlgCanhbao.h
ImplementationFile=DlgCanhbao.cpp
BaseClass=CDialog
Filter=D
LastObject=CDlgCanhbao

[DLG:IDD_DLGCACHCHAY]
Type=1
Class=CDlgCachchay
ControlCount=28
Control1=IDOK,button,1342242817
Control2=IDCANCEL,button,1073807360
Control3=IDC_CHECK_MOSTING,button,1342252035
Control4=IDC_CHECK_MOSTED,button,1342252035
Control5=IDC_CHECK_MOSTSTUDENT,button,1342252035
Control6=IDC_STATIC,button,1342177287
Control7=IDC_STATIC,static,1342308352
Control8=IDC_SLIDER_MOSTED,msctls_trackbar32,1342242820
Control9=IDC_SLIDER_MOSTING,msctls_trackbar32,1342242820
Control10=IDC_SLIDER_MOSTSTUDENT,msctls_trackbar32,1342242820
Control11=IDC_STATIC,static,1342308352
Control12=IDC_STATIC,static,1342308352
Control13=IDC_BUTTON_MACDINH,button,1342242817
Control14=IDC_STATIC,button,1342177287
Control15=IDC_STATIC,static,1342308352
Control16=IDC_BESTFIT,button,1342308361
Control17=IDC_FIRSTFIT,button,1342177289
Control18=IDC_KHOANGCACH,button,1342242819
Control19=IDC_STATIC,button,1342177287
Control20=IDC_STATIC,static,1342308352
Control21=IDC_EDIT_KCTIET,edit,1082196096
Control22=IDC_SLIDER_KCTIET,msctls_trackbar32,1342242840
Control23=IDC_STATIC,static,1342308352
Control24=IDC_STATIC,static,1342308352
Control25=IDC_STATIC,static,1342308352
Control26=IDC_STATIC,static,1342308352
Control27=IDC_STATIC,static,1342308352
Control28=IDC_STATIC,static,1342308352

[CLS:CDlgCachchay]
Type=0
HeaderFile=DlgCachchay.h
ImplementationFile=DlgCachchay.cpp
BaseClass=CBitmapDialog
Filter=D
VirtualFilter=dWC
LastObject=CDlgCachchay

[DLG:IDD_DLGTIENDO]
Type=1
Class=CDlgTiendo
ControlCount=8
Control1=IDOK,button,1073807361
Control2=IDCANCEL,button,1073807360
Control3=IDC_PROGRESS,msctls_progress32,1350565888
Control4=IDC_STATIC_MON,static,1342308352
Control5=IDC_STATIC_DEM,static,1342308352
Control6=IDC_STATIC_M,static,1342308352
Control7=IDC_STATIC_T,static,1342308352
Control8=IDC_STATIC_STATUS,static,1342308352

[CLS:CDlgTiendo]
Type=0
HeaderFile=DlgTiendo.h
ImplementationFile=DlgTiendo.cpp
BaseClass=CBitmapDialog
Filter=D
LastObject=ID_EDIT_COPY
VirtualFilter=dWC

