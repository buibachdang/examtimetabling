// DlgCachchay.cpp : implementation file
//

#include "stdafx.h"
#include "xeplichthi.h"
#include "DlgCachchay.h"
#include "Xeplt.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CDlgCachchay dialog

extern CXeplt Xeplt;
CDlgCachchay::CDlgCachchay(CWnd* pParent /*=NULL*/)
	: CBitmapDialog(CDlgCachchay::IDD, pParent)
{
	//{{AFX_DATA_INIT(CDlgCachchay)
	bMosted = FALSE;
	bMosting = FALSE;
	iMosted = 0;
	iMosting = 0;
	bMoststu = FALSE;
	iMoststu = 0;
	iKctiet = 0;
	iKct = 0;
	//}}AFX_DATA_INIT
	tooltip=new CToolTipCtrl;
}


void CDlgCachchay::DoDataExchange(CDataExchange* pDX)
{
	CBitmapDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDlgCachchay)
	DDX_Control(pDX, IDC_SLIDER_KCTIET, c_Tuc);
	DDX_Control(pDX, IDC_FIRSTFIT, m_First);
	DDX_Control(pDX, IDC_KHOANGCACH, m_khoangcach);
	DDX_Control(pDX, IDC_BESTFIT, m_bestfit);
	DDX_Control(pDX, IDC_CHECK_MOSTSTUDENT, c_Moststucheck);
	DDX_Control(pDX, IDC_CHECK_MOSTING, c_Mostingcheck);
	DDX_Control(pDX, IDC_CHECK_MOSTED, c_Mostedcheck);
	DDX_Control(pDX, IDC_SLIDER_MOSTSTUDENT, c_Moststu);
	DDX_Control(pDX, IDC_SLIDER_MOSTING, c_Mosting);
	DDX_Control(pDX, IDC_SLIDER_MOSTED, c_Mosted);
	DDX_Check(pDX, IDC_CHECK_MOSTED, bMosted);
	DDX_Check(pDX, IDC_CHECK_MOSTING, bMosting);	
	DDX_Slider(pDX, IDC_SLIDER_MOSTED, iMosted);
	DDX_Slider(pDX, IDC_SLIDER_MOSTING, iMosting);
	DDX_Check(pDX, IDC_CHECK_MOSTSTUDENT, bMoststu);
	DDX_Slider(pDX, IDC_SLIDER_MOSTSTUDENT, iMoststu);
	DDX_Text(pDX, IDC_EDIT_KCTIET, iKctiet);
	DDX_Slider(pDX, IDC_SLIDER_KCTIET, iKct);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CDlgCachchay, CBitmapDialog)
	//{{AFX_MSG_MAP(CDlgCachchay)
	ON_BN_CLICKED(IDC_BUTTON_MACDINH, OnButtonMacdinh)
	ON_WM_CHAR()
	ON_WM_CTLCOLOR()
	ON_NOTIFY(NM_RELEASEDCAPTURE, IDC_SLIDER_KCTIET, OnReleasedcaptureSliderKctiet)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDlgCachchay message handlers

void CDlgCachchay::OnOK() 
{
	UpdateData(TRUE);
	//Xeplt.Cachchay	
	Xeplt.Cachchay.mosted.sudung=bMosted;
	Xeplt.Cachchay.mosted.giatri=iMosted;
	Xeplt.Cachchay.mosting.sudung=bMosting;
	Xeplt.Cachchay.mosting.giatri=iMosting;
	Xeplt.Cachchay.moststu.sudung=bMoststu;
	Xeplt.Cachchay.moststu.giatri=iMoststu;
	Xeplt.Cachchay.kctiet=iKct;
	if (m_khoangcach.GetCheck()==1)
		Xeplt.m_khoangcach =TRUE;
	else
		Xeplt.m_khoangcach =FALSE;
	if (m_bestfit.GetCheck()==1)
		Xeplt.m_bestfit=TRUE;
	else
		Xeplt.m_bestfit=FALSE;	
	
	CBitmapDialog::OnOK();
}

BOOL CDlgCachchay::OnInitDialog() 
{
	CBitmapDialog::OnInitDialog();
	SetBitmap(IDB_BITMAP1,BITMAP_TILE);
	
	
	/*
	CToolTipCtrl * p=c_Mosted.GetToolTips();
	if (p==NULL) 
	{
		MessageBox("haha");
		p = new CToolTipCtrl;
	}
	
	p->Create(this,TTS_ALWAYSTIP);
	c_Mosted.SetToolTips(p);
	BOOL d = p->AddTool(GetDlgItem(IDC_SLIDER_MOSTED), _T("fghfsdh")); 
	CToolTipCtrl * q=c_Mosted.GetToolTips();
	if (q!=NULL) 
	{
		MessageBox("hihi");
		//p = new CToolTipCtrl;
	}
	if (d!=0) {MessageBox("SUCCESS");}
	*/
	/*CSliderCtrl sl;
	RECT e;
	e.top=0;e.left=0;e.right=100;e.bottom=200;
	BOOL b=sl.Create( TBS_TOOLTIPS, e, this, 9);
	*/



		//	TBS_TOOLTIPS 
	//CToolTipCtrl  t;

	tooltip->Create(this,TTS_ALWAYSTIP);
	c_Tuc.SetToolTips(tooltip);	
	tooltip->SetDelayTime(200);	
	

	c_Mosted.SetRange(1,100);
	c_Mosting.SetRange(1,100);
	c_Moststu.SetRange(1,100);
	c_Tuc.SetRange(0,12);
	OnButtonMacdinh();


	
	//tooltip=new CToolTipCtrl;
	//tooltip->Create(this,TTS_ALWAYSTIP);
	//c_Tuc.SetToolTips(tooltip);
	//tooltip->SetDelayTime(200);
	//tooltip->AddTool(GetDlgItem(IDC_SLIDER_KCTIET), _T("uble click")); 
//	tooltip->Activate(TRUE);
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CDlgCachchay::OnButtonMacdinh() 
{
	c_Mostedcheck.SetCheck(1);	
	c_Mostingcheck.SetCheck(0);
	c_Moststucheck.SetCheck(0);
	c_Mosted.SetPos(100);
	c_Mosting.SetPos(0);
	c_Moststu.SetPos(0);
	c_Tuc.SetPos(0);

	m_bestfit.SetCheck(1);
	m_First.SetCheck(0);

	m_khoangcach.SetCheck(1);
}

void CDlgCachchay::OnChar(UINT nChar, UINT nRepCnt, UINT nFlags) 
{
	// TODO: Add your message handler code here and/or call default
	
	CBitmapDialog::OnChar(nChar, nRepCnt, nFlags);
}

CDlgCachchay::~CDlgCachchay()
{
	delete tooltip;
}

HBRUSH CDlgCachchay::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor) 
{
	HBRUSH hbr = CBitmapDialog::OnCtlColor(pDC, pWnd, nCtlColor);
	
	// TODO: Change any attributes of the DC here

	
	// TODO: Return a different brush if the default is not desired
	return hbr;
}

BOOL CDlgCachchay::PreTranslateMessage(MSG* pMsg) 
{
		// TODO: Add your specialized code here and/or call the base class
	tooltip->RelayEvent(pMsg);
	return CBitmapDialog::PreTranslateMessage(pMsg);
}

void CDlgCachchay::OnReleasedcaptureSliderKctiet(NMHDR* pNMHDR, LRESULT* pResult) 
{

	int pos=c_Tuc.GetPos();
	CString strpos; strpos.Format("%d",pos);
	tooltip->AddTool(GetDlgItem(IDC_SLIDER_KCTIET), strpos); 
	tooltip->Activate(TRUE);
	//MessageBox("gg");
	*pResult = 0;
}
