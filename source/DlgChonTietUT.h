#if !defined(AFX_DLGCHONTIETUT_H__6A292493_F441_4647_B8EC_2D5C61DEC35F__INCLUDED_)
#define AFX_DLGCHONTIETUT_H__6A292493_F441_4647_B8EC_2D5C61DEC35F__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DlgChonTietUT.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CDlgChonTietUT dialog
#include "BitmapDialog.h"
class CDlgChonTietUT : public CBitmapDialog
{
// Construction
public:
	int bflag;
	CString mamon;
	CDlgChonTietUT(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CDlgChonTietUT)
	enum { IDD = IDD_DLGCHONTIETUT };
	CDateTimeCtrl	c_kt;
	CDateTimeCtrl	c_cd;
	CDateTimeCtrl	c_bd;
	CButton	bt_ngaycd;
	CButton	bt_ngaytd;
	CButton	m_ctlchon;
	CString	m_strMamon;
	CTime	m_bd;
	CTime	m_cd;
	CTime	m_kt;
	int		m_iTiet;
	CString	str_chon;
	BOOL	bRangbuoc;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDlgChonTietUT)
	public:
	virtual BOOL Create(LPCTSTR lpszClassName, LPCTSTR lpszWindowName, DWORD dwStyle, const RECT& rect, CWnd* pParentWnd, UINT nID, CCreateContext* pContext = NULL);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CDlgChonTietUT)
	virtual BOOL OnInitDialog();
	afx_msg void OnRadioNgaytd();
	afx_msg void OnRadioNgaycd();
	virtual void OnOK();
	afx_msg void OnCloseupDatetimepickerNgaybd(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnCloseupDatetimepickerNgaykt(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnCloseupDatetimepickerNgaycd(NMHDR* pNMHDR, LRESULT* pResult);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DLGCHONTIETUT_H__6A292493_F441_4647_B8EC_2D5C61DEC35F__INCLUDED_)
