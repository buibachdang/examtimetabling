// DlgPhongban.cpp : implementation file
//

#include "stdafx.h"
#include "xeplichthi.h"
#include "DlgPhongban.h"
#include "khaibao.h"
//#include "SetDaynha.h"
//#include "SetPhong.h"
#include "DlgThongtinPhong.h"
#include "Xeplt.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif
extern bool bflagPhongban;
//extern int gTongsoPhong;
//extern int gTongsoTiet;
//extern CPhonghoc Phonghoc;
//extern STRDaynha Daynha[15];
extern CXeplt Xeplt;

/////////////////////////////////////////////////////////////////////////////
// CDlgPhongban dialog


CDlgPhongban::CDlgPhongban(CWnd* pParent /*=NULL*/)
	: CBDialog(CDlgPhongban::IDD, pParent)
{
	//{{AFX_DATA_INIT(CDlgPhongban)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	tooltip=new CToolTipCtrl;
	
}


void CDlgPhongban::DoDataExchange(CDataExchange* pDX)
{
	CBDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDlgPhongban)
	DDX_Control(pDX, IDC_TREE, m_tree);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CDlgPhongban, CBDialog)
	//{{AFX_MSG_MAP(CDlgPhongban)
	ON_NOTIFY(NM_DBLCLK, IDC_TREE, OnDblclkTree)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDlgPhongban message handlers

BOOL CDlgPhongban::OnInitDialog() 
{

	CBDialog::OnInitDialog();
	SetBitmap(IDB_BITMAP1);
	SetBitmapStyle(StyleTile);

//	SetBitmap(IDB_BITMAP1,BITMAP_TILE);
	bflagPhongban=true;	
	int i,j;
	

	//Khoi dong cay chua Phong hoc
	
	HTREEITEM daynha[15];
	for (i=0;i<15;i++)
	{
		daynha[i]=m_tree.InsertItem(Xeplt.Daynha[i].daynha);
		for (j=0;j<Xeplt.gTongsoPhong;j++)
			if (Xeplt.Phonghoc[j].daynha ==Xeplt.Daynha[i].daynha )
				m_tree.InsertItem(Xeplt.Phonghoc[j].tenphong,daynha[i]) ;
	}
	
	
	tooltip->Create(this,TTS_ALWAYSTIP);
	m_tree.SetToolTips(tooltip);
	tooltip->SetDelayTime(200);
	tooltip->AddTool(GetDlgItem(IDC_TREE), _T("Double click"));  
	
	//tooltip->Activate(TRUE);
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE

}

void CDlgPhongban::OnDblclkTree(NMHDR* pNMHDR, LRESULT* pResult) 
{
	HTREEITEM sel;
	sel=m_tree.GetSelectedItem();
	if (m_tree.ItemHasChildren(sel))
	{
		CString kq=m_tree.GetItemText(sel);
		//kq="Ban chon day nha "+kq;
		//MessageBox(kq);
	}
	else
	{
	
		CDlgThongtinPhong dlgPhong;
		dlgPhong.m_tenphong  =m_tree.GetItemText(sel);
		dlgPhong.DoModal ();
	}
	
	*pResult = 0;
	
}

CDlgPhongban::~CDlgPhongban()
{
delete tooltip;
}
