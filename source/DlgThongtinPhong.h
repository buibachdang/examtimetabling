#if !defined(AFX_DLGTHONGTINPHONG_H__FBDBEC48_47AD_4331_A924_3FE4F772744B__INCLUDED_)
#define AFX_DLGTHONGTINPHONG_H__FBDBEC48_47AD_4331_A924_3FE4F772744B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DlgThongtinPhong.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CDlgThongtinPhong dialog
#include "BitmapDialog.h"
//#include "BDialog.h"
class CDlgThongtinPhong : public CBitmapDialog
{
// Construction
public:
	int m_stt;
	CString m_tenphong;
	CDlgThongtinPhong(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CDlgThongtinPhong)
	enum { IDD = IDD_DLGTHONGTINPHONG };
	CStatic	c_ngay;
	CButton	m_staticngaythi;
	CButton	m_all;
	CListBox	m_listngaythi;
	CString	m_daynha;
	int		m_succhua;
	CString	m_etenphong;
	//CString	str_ngay;
	//}}AFX_DATA
	CButton m_ptiet[12];
	CString m_strSelection;


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDlgThongtinPhong)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CDlgThongtinPhong)
	virtual BOOL OnInitDialog();
	afx_msg void OnSelchangePdanhsachngaythi();
	afx_msg void OnButtonDongy();
	//}}AFX_MSG
	afx_msg void OnPTiet(UINT nID);
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DLGTHONGTINPHONG_H__FBDBEC48_47AD_4331_A924_3FE4F772744B__INCLUDED_)
