#if !defined(AFX_DLGNHAPTIETBAN_H__9A7D758D_4284_4D12_99BC_68C334144293__INCLUDED_)
#define AFX_DLGNHAPTIETBAN_H__9A7D758D_4284_4D12_99BC_68C334144293__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DlgNhaptietban.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CDlgNhaptietban dialog
#include "BitmapDialog.h"
class CDlgNhaptietban : public CBitmapDialog
{
// Construction
public:
	CString title;
	bool select;
	DWORD ngaythiban;
	CDlgNhaptietban(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CDlgNhaptietban)
	enum { IDD = IDD_DLGNHAPTIETBAN };
	CButton	m_tatcatiet;
	//}}AFX_DATA
	CButton m_tiet[12];


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDlgNhaptietban)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CDlgNhaptietban)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	//}}AFX_MSG
	afx_msg void OnTiet(UINT nID);
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DLGNHAPTIETBAN_H__9A7D758D_4284_4D12_99BC_68C334144293__INCLUDED_)
