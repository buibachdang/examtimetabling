// XepLichThiDoc.cpp : implementation of the CXepLichThiDoc class
//

#include "stdafx.h"
#include "XepLichThi.h"

#include "XepLichThiDoc.h"
#include "Xeplt.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CXepLichThiDoc
extern CXeplt Xeplt;
IMPLEMENT_DYNCREATE(CXepLichThiDoc, CDocument)

BEGIN_MESSAGE_MAP(CXepLichThiDoc, CDocument)
	//{{AFX_MSG_MAP(CXepLichThiDoc)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CXepLichThiDoc construction/destruction

CXepLichThiDoc::CXepLichThiDoc()
{
	// TODO: add one-time construction code here

}

CXepLichThiDoc::~CXepLichThiDoc()
{
}

BOOL CXepLichThiDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: add reinitialization code here
	// (SDI documents will reuse this document)

	return TRUE;
}



/////////////////////////////////////////////////////////////////////////////
// CXepLichThiDoc serialization

void CXepLichThiDoc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		//AfxMessageBox(Xeplt.in);
		Xeplt.in="\r\n"+Xeplt.in;
		ar<<Xeplt.in;
	}
	else
	{

		ar>>Xeplt.in;
	}
}

/////////////////////////////////////////////////////////////////////////////
// CXepLichThiDoc diagnostics

#ifdef _DEBUG
void CXepLichThiDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CXepLichThiDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CXepLichThiDoc commands
