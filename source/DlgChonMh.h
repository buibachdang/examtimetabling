#if !defined(AFX_DLGCHONMH_H__5A41C5A0_EC20_4FDA_9901_D7FEE1497590__INCLUDED_)
#define AFX_DLGCHONMH_H__5A41C5A0_EC20_4FDA_9901_D7FEE1497590__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DlgChonMh.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CDlgChonMh dialog
#include "BitmapDialog.h"
class CDlgChonMh : public CBitmapDialog
{
// Construction
public:	
	
	int sopt2;
	int sopt1;
	CString khoahoc;
	CDlgChonMh(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CDlgChonMh)
	enum { IDD = IDD_DLGCHONMH };
	CListBox	m_ctlMonList2;
	CListBox	m_ctlMonList1;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDlgChonMh)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CDlgChonMh)
	virtual BOOL OnInitDialog();
	afx_msg void OnButtonQuaphai();
	afx_msg void OnButtonQuatrai();
	virtual void OnOK();
	afx_msg void OnButtonChitiet2();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DLGCHONMH_H__5A41C5A0_EC20_4FDA_9901_D7FEE1497590__INCLUDED_)
