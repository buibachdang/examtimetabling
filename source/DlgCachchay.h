#if !defined(AFX_DLGCACHCHAY_H__6299FB2F_7823_43B1_88F9_D6FFFCCEAD22__INCLUDED_)
#define AFX_DLGCACHCHAY_H__6299FB2F_7823_43B1_88F9_D6FFFCCEAD22__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DlgCachchay.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CDlgCachchay dialog

#include "BitmapDialog.h"
#include "MySliderControl.h "
class CDlgCachchay : public CBitmapDialog
{
// Construction
public:
	~CDlgCachchay();
	CToolTipCtrl * tooltip;
	CDlgCachchay(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CDlgCachchay)
	enum { IDD = IDD_DLGCACHCHAY };
	CButton	m_First;
	CButton	m_khoangcach;
	CButton	m_bestfit;
	CButton	c_Moststucheck;
	CButton	c_Mostingcheck;
	CButton	c_Mostedcheck;
	CMySliderControl	c_Moststu;
	CMySliderControl	c_Mosting;
	CMySliderControl	c_Mosted;
	CMySliderControl	c_Tuc;
	BOOL	bMosted;
	BOOL	bMosting;
	int		iMosted;
	int		iMosting;
	BOOL	bMoststu;
	int		iMoststu;
	int		iKctiet;
	int		iKct;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDlgCachchay)
	public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CDlgCachchay)
	virtual void OnOK();
	virtual BOOL OnInitDialog();
	afx_msg void OnButtonMacdinh();
	afx_msg void OnChar(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	afx_msg void OnReleasedcaptureSliderKctiet(NMHDR* pNMHDR, LRESULT* pResult);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DLGCACHCHAY_H__6299FB2F_7823_43B1_88F9_D6FFFCCEAD22__INCLUDED_)
