#if !defined(AFX_DLGGIOITHIEU_H__6EC52F4E_B31A_421A_9CEB_05A8FC5754DD__INCLUDED_)
#define AFX_DLGGIOITHIEU_H__6EC52F4E_B31A_421A_9CEB_05A8FC5754DD__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DlgGioithieu.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CDlgGioithieu dialog
#include "BitmapDialog.h"
class CDlgGioithieu : public CBitmapDialog
{
// Construction
public:
	CBitmapButton b_Ok;
	CDlgGioithieu(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CDlgGioithieu)
	enum { IDD = IDD_DLGGIOITHIEU };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDlgGioithieu)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CDlgGioithieu)
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DLGGIOITHIEU_H__6EC52F4E_B31A_421A_9CEB_05A8FC5754DD__INCLUDED_)
