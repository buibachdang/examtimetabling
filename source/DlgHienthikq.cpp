// DlgHienthikq.cpp : implementation file
//

#include "stdafx.h"
#include "xeplichthi.h"
#include "DlgHienthikq.h"
#include "Xeplt.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CDlgHienthikq dialog
extern CXeplt Xeplt;

CDlgHienthikq::CDlgHienthikq(CWnd* pParent /*=NULL*/)
	: CBDialog(CDlgHienthikq::IDD, pParent)
{
	//{{AFX_DATA_INIT(CDlgHienthikq)
	strTgchay = _T("");
	//}}AFX_DATA_INIT
}


void CDlgHienthikq::DoDataExchange(CDataExchange* pDX)
{
	CBDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDlgHienthikq)
	DDX_Control(pDX, IDC_LIST_KQ, m_ctlkqlist);
	DDX_Control(pDX, IDC_MSFLEXGRID1, c_Grid);
	DDX_Text(pDX, IDC_EDIT_TGCHAY, strTgchay);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CDlgHienthikq, CBDialog)
	//{{AFX_MSG_MAP(CDlgHienthikq)
	ON_BN_CLICKED(IDC_HAOPHI, OnHaophi)
	ON_BN_CLICKED(IDC_HAOPHI2, OnHaophi2)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDlgHienthikq message handlers

BOOL CDlgHienthikq::OnInitDialog() 
{
	//KHAI BAO
	int i,j;
	CString str,t;	
	CBDialog::OnInitDialog();
	//SetBitmap(IDB_BITMAP1,BITMAP_TILE);
	SetBitmap(IDB_BITMAP1);
	SetBitmapStyle(StyleTile);
	
	CString nhomvaphong;	
	int k=0;
	// sua soan Grid
	c_Grid.SetCols(6);
	c_Grid.SetRows(Xeplt.gTongsoNhom+20);
	//CString o;
	//o.Format("%d",Xeplt.gTongsoNhom+2);
	//AfxMessageBox(o);
	c_Grid.SetColWidth(0,1200);
	c_Grid.SetColWidth(5,650);
	c_Grid.SetRow(0);
	c_Grid.SetCol(0);
	c_Grid.SetText("   M� m�n h�c");	
	c_Grid.SetCol(1);
	c_Grid.SetColWidth(1,1200);
	c_Grid.SetText("    Ng�y thi");	
	c_Grid.SetCol(2);
	c_Grid.SetText("     Ti�t thi");	
	c_Grid.SetCol(3);
	c_Grid.SetText("     Nh�m");	
	c_Grid.SetCol(4);
	c_Grid.SetText("   Ph�ng thi");	
	c_Grid.SetCol(5);
	c_Grid.SetText("S� tthi");
	int iRow=1;
	
/*
	CString tde;
	CString strMamonhoc="M� m�n h�c", strNgaythi="Ng�y thi",\
		strGiothi="Gi� thi", strPhongthi="Ph�ng thi", strSotietthi="S� ti�t thi";

	tde=strMamonhoc + "   " + strNgaythi + "        " + strGiothi + "        " + strPhongthi + "        " + strSotietthi;
	
	m_ctlkqlist.AddString(tde);
	int chieudaitde=tde.GetLength();
	CString strdong("",chieudaitde);
	int vtNgaythi,vtMamonhoc,vtGiothi,vtPhongthi,vtSotietthi;
	vtMamonhoc=tde.Find("M�"); 
	vtNgaythi=tde.Find("Ng�y");
	vtGiothi=tde.Find("Gi�");
	vtPhongthi=tde.Find("Ph�ng");
	vtSotietthi=tde.Find("S�");
*/
	for(i=0;i<Xeplt.gTongsoMon;i++)
	{
		int stt=i;// cach nay khong duoc nua :Xeplt.DSttxet[i]; (do phai tim UMon)
		
		while (Xeplt.UMon[k].mamon!=Xeplt.Monhoc[stt].mamon)
			k++;
//
		//int ryu=Xeplt.Monhoc[stt].tietbd;
		if (stt>323) MessageBox("bi trong hien thi");
		if ((Xeplt.Monhoc[stt].tietbd+1)%12==0)
			Xeplt.UMon[k].tietbd=12;
		else
			Xeplt.UMon[k].tietbd=(Xeplt.Monhoc[stt].tietbd+1)%12;

		CString strmamon=Xeplt.Monhoc[stt].mamon;		
		int tietbd=Xeplt.UMon[k].tietbd;
		CString strtietbd;
		strtietbd.Format("        %d",tietbd);
		int sotietthi=Xeplt.Monhoc[stt].sotietthi;
		CString strsotietthi;
		strsotietthi.Format("     %d",sotietthi);
		t.Format("%d      %d",tietbd,sotietthi);
		CString strngaythi="     " + Xeplt.Tietrangay(Xeplt.ngaybatdau,Xeplt.Monhoc[stt].tietbd,Xeplt.UMon[k].ngaythi);
		str=strmamon + "   " + t + "                                            "+strngaythi;
		Xeplt.in+=str+"\r\n";
		m_ctlkqlist.AddString(str);	
		int r=Xeplt.Monhoc[stt].nhom.GetSize();
		for(j=0;j<r;j++)
		{
			c_Grid.SetRow(iRow);
			if (j==0)
			{
				c_Grid.SetCol(0);
				strmamon= "       " +strmamon;
				c_Grid.SetText(strmamon);
			}			
			c_Grid.SetCol(1);
			//strngaythi="     " + strngaythi;
			c_Grid.SetText(strngaythi);
			c_Grid.SetCol(2);
			//strtietbd="        " +strtietbd;
			c_Grid.SetText(strtietbd);
			c_Grid.SetCol(3);
			CString strnhom;

			strnhom="        " + Xeplt.Monhoc[stt].nhom[j].manhom;
	
			c_Grid.SetText(strnhom);
			c_Grid.SetCol(4);
			CString strtenphong;
			strtenphong="     " + Xeplt.Monhoc[stt].nhom[j].tenphong;
			c_Grid.SetText(strtenphong);
			c_Grid.SetCol(5);
			//strsotietthi= "    "+strsotietthi;
			c_Grid.SetText(strsotietthi);
			nhomvaphong="                                "+Xeplt.Monhoc[stt].nhom[j].manhom+"        "+Xeplt.Monhoc[stt].nhom[j].tenphong;			
			m_ctlkqlist.AddString(nhomvaphong);	
			Xeplt.in+=nhomvaphong+"\r\n";
			iRow++;
		}
	
	}
//	int sox;
//	sox=Monhoc[0].nhom.GetSize();
	//DWORD tgkt=GetTickCount();
	//CString tam;
	//tam.Format("%ld ms",tgkt-tgbd);	
	//MessageBox(tam);
	//MessageBox(strTgchay);
	UpdateData(FALSE);

	

	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CDlgHienthikq::OnHaophi() 
{/*
	int i,j,k,siso=0,haophi=0,tongsucchua=0;
	CString phong;
	CArray<CString,CString> Dayphong;
	for (i=0;i<Xeplt.gTongsoMon;i++)
	{
		siso=0;
		int succhuamon=0;
		int sonhom=Xeplt.Monhoc[i].nhom.GetSize();
		Dayphong.RemoveAll ();
		Dayphong.SetSize(sonhom);
		for (j=0;j<sonhom;j++)
		{
			siso+=Xeplt.Monhoc[i].nhom[j].siso ;
			Dayphong[j]=Xeplt.Monhoc[i].nhom [j].tenphong ;
		}

		for (j=0;j<sonhom;j++)
			for (k=j+1;k<sonhom;k++)
				if (Dayphong[k]==Dayphong[j]) Dayphong[k]="";

		for (j=0;j<sonhom;j++)
		{
			if (Dayphong[j]!="")
				for (k=0;k<Xeplt.gTongsoPhong;k++)
					if (Xeplt.Phonghoc[k].tenphong ==Dayphong[j]) 
					{
						succhuamon+=Xeplt.Phonghoc[k].succhua ;
						break;
					}
		}
		int dem=0;
		int test=Dayphong.GetSize();
		for (k=0;k<sonhom;k++)
			if (Dayphong[k]!="") dem++;
		
		tongsucchua+=succhuamon;
		haophi=haophi+ (succhuamon-siso);
			
	
	}
	CString kq;
	double tam=((double)haophi/(double)tongsucchua)*100;
	kq.Format ("Tong suc chua su dung: %d, Tong hao phi : %d, Ti le : %.3f",tongsucchua,haophi,tam);
	MessageBox(kq);	
	*/
}

void CDlgHienthikq::OnOK() 
{
	Xeplt.Hauxuly();	
	CBDialog::OnOK();
}

void CDlgHienthikq::OnHaophi2() 
{
		int i,j,k,siso=0,haophi=0,tongsucchua=0;
	CString phong;
	CArray<CString,CString> Dayphong;
	for (i=0;i<Xeplt.gTongsoMon;i++)
	{
		siso=0;
		int succhuamon=0;
		int sonhom=Xeplt.Monhoc[i].nhom.GetSize();
		Dayphong.RemoveAll ();
		Dayphong.SetSize(sonhom);
		for (j=0;j<sonhom;j++)
		{
			siso+=Xeplt.Monhoc[i].nhom[j].siso ;
			Dayphong[j]=Xeplt.Monhoc[i].nhom [j].tenphong ;
		}

		for (j=0;j<sonhom;j++)
			for (k=j+1;k<sonhom;k++)
				if (Dayphong[k]==Dayphong[j]) Dayphong[k]="";

		for (j=0;j<sonhom;j++)
		{
			if (Dayphong[j]!="")
				for (k=0;k<Xeplt.gTongsoPhong;k++)
					if (Xeplt.Phonghoc[k].tenphong ==Dayphong[j]) 
					{
						succhuamon+=Xeplt.Phonghoc[k].succhua ;
						break;
					}
		}
		int dem=0;
		int test=Dayphong.GetSize();
		for (k=0;k<sonhom;k++)
			if (Dayphong[k]!="") dem++;
		
		tongsucchua+=succhuamon;
		haophi=haophi+ (succhuamon-siso);			
	}
	CString kq;
	double tam=((double)haophi/(double)tongsucchua)*100;
	kq.Format ("Tong suc chua su dung: %d, Tong hao phi : %d, Ti le : %.3f",tongsucchua,haophi,tam);
	MessageBox(kq);	
	
}
