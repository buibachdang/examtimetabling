// Xeplt.h: interface for the CXeplt class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_XEPLT_H__9E5859D8_8BF5_423E_B8EF_450A08F113C5__INCLUDED_)
#define AFX_XEPLT_H__9E5859D8_8BF5_423E_B8EF_450A08F113C5__INCLUDED_

#include "khaibao.h"	// Added by ClassView
#include "SetNhom.h"	// Added by ClassView
#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class CXeplt  
{
public:	
	int gTongsoNhom;
	BOOL FwChecking2(int kctiet, CMh *mon, int somt, int tietbd, int sotietthi);
	CFile myFile;
//	HANDLE hFile;
	BOOL bDungct;
	void UnloadPhong(int mon);
	int khoangcach[15][15];
	bool m_bestfit;
	bool m_khoangcach;
	void Sort(int stt);
	void ResetPhong();
	void Reset(int stt, int tbd, int sotiet);
	int Next(int current,int nhommax,int tbd, int sotiet);
	int Firstfit(int sosv, int tbd, int sotiet);
	int Firstfit(int stt, int sosv, int tbd, int sotiet);
	bool Empty(int stt,int tbd, int sotiet);
	bool Empty(int stt);
	bool Chonphong(int stt,int tietbd,int sotiet);
	int Bestfit(int stt, int sosv, int tbd, int sotiet);
	int Bestfit(int sosv, int tbd, int sotiet);
	bool Ban(int stt, int tbd, int sotiet);
	CString in;
	STRCachchay Cachchay;
	void Hauxuly();
	CString Tietrangay(CTime ngaybd, int tiet, CTime &ngay);
	CTrangthai Trangthai;
	void TinhdiemUT(CMonhoc &Mh, int bd);
	void qSortttxet(CUIntArray &a,int lo, int hi);
	void Khoidong();
	void qSortex(int a[], int stage, int lo, int hi);
	BOOL FwChecking(CMh *mon, int somt, int tietbd, int sotietthi);
	bool Laytiet(int mon, int phu);
	int Timvttietbd(CTime ngaybd, CTime ngaykt, CTime ngaycx, int tietcx);
	int Maraso(int loai, CString const &ma);
	CSetNhom m_Nhom;
	int * arr;
	int Dempt(int mon, int sotietthi, int btrai, int bphai);
	CTrangthaiSopt TrangthaiSopt;
	CDemsopt Demsopt;
	CMientri D;
	CMonhoc Monhoc;
	CTrangthaiDSttxet TrangthaiDSttxet;
	CUIntArray DSttxet;
	int ChayGT(CEdit * ht);
	STRDaynha Daynha[15];
	CTietthi Tietthi;
	CTime ngayketthuc;
	CTime ngaybatdau;
	int gTongsoPhong;
	CPhonghoc Phonghoc;
	int gTongsoKhoahoc;
	CUMH UMon;
	int gUTongsoMon;
	int gTongsoMon;
	int gTongsoTiet;
	CKhoahoc Khoahoc;
	CXeplt();
	virtual ~CXeplt();

};

#endif // !defined(AFX_XEPLT_H__9E5859D8_8BF5_423E_B8EF_450A08F113C5__INCLUDED_)
