#if !defined(AFX_SETNHOM_H__E000A5B4_4990_4E5B_A3AB_4D4D81C4C4E3__INCLUDED_)
#define AFX_SETNHOM_H__E000A5B4_4990_4E5B_A3AB_4D4D81C4C4E3__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// SetNhom.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CSetNhom DAO recordset

class CSetNhom : public CDaoRecordset
{
public:
	CSetNhom(CDaoDatabase* pDatabase = NULL);
	DECLARE_DYNAMIC(CSetNhom)

// Field/Param Data
	//{{AFX_FIELD(CSetNhom, CDaoRecordset)
	CString	m_Mamonhoc;
	CString	m_Nhom;
	CString	m_To;
	long	m_Soluong;
	//}}AFX_FIELD

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSetNhom)
	public:
	virtual CString GetDefaultDBName();		// Default database name
	virtual CString GetDefaultSQL();		// Default SQL for Recordset
	virtual void DoFieldExchange(CDaoFieldExchange* pFX);  // RFX support
	//}}AFX_VIRTUAL

// Implementation
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SETNHOM_H__E000A5B4_4990_4E5B_A3AB_4D4D81C4C4E3__INCLUDED_)
