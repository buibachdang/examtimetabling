#if !defined(AFX_DLGNHAPTHOIGIAN_H__68373BBF_00BF_4974_98E3_7C5FFE9C1F59__INCLUDED_)
#define AFX_DLGNHAPTHOIGIAN_H__68373BBF_00BF_4974_98E3_7C5FFE9C1F59__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DlgNhapthoigian.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CDlgNhapthoigian dialog
#include "BitmapDialog.h"
#include "ToolTipWnd.h"
class CDlgNhapthoigian : public CBitmapDialog
{
// Construction
public:
	CToolTipWnd * m_BalloonToolTip;
	~CDlgNhapthoigian();
	CToolTipCtrl * tooltip;
	CDlgNhapthoigian(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CDlgNhapthoigian)
	enum { IDD = IDD_DLGNHAPTHOIGIAN };
	CButton	m_thubay;
	CButton	m_chunhat;
	CButton	m_allday;
	CListBox	m_listngaythi;
	CDateTimeCtrl	m_ngayketthuc;
	CDateTimeCtrl	m_ngaybatdau;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDlgNhapthoigian)
	public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CDlgNhapthoigian)
	virtual void OnOK();
	virtual BOOL OnInitDialog();
	afx_msg void OnDatetimechangeDatetimepicker1(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnDatetimechangeDatetimepicker2(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnDblclkDanhsachngaythi();
	afx_msg void OnAllday();
	afx_msg void OnThubay();
	afx_msg void OnChunhat();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DLGNHAPTHOIGIAN_H__68373BBF_00BF_4974_98E3_7C5FFE9C1F59__INCLUDED_)
