// XepLichThiDoc.h : interface of the CXepLichThiDoc class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_XEPLICHTHIDOC_H__BF5FA09B_101B_428B_9D20_687DB228ACB7__INCLUDED_)
#define AFX_XEPLICHTHIDOC_H__BF5FA09B_101B_428B_9D20_687DB228ACB7__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


class CXepLichThiDoc : public CDocument
{
protected: // create from serialization only
	CXepLichThiDoc();
	DECLARE_DYNCREATE(CXepLichThiDoc)

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CXepLichThiDoc)
	public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CXepLichThiDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CXepLichThiDoc)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_XEPLICHTHIDOC_H__BF5FA09B_101B_428B_9D20_687DB228ACB7__INCLUDED_)
