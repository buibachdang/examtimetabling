// DlgThongtinPhong.cpp : implementation file
//

#include "stdafx.h"
#include "xeplichthi.h"
#include "DlgThongtinPhong.h"
#include "khaibao.h"
#include "Xeplt.h"
//extern CPhonghoc Phonghoc;
//extern CTime ngaybatdau,ngayketthuc;
//extern int gTongsoPhong;
extern CXeplt Xeplt;


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CDlgThongtinPhong dialog


CDlgThongtinPhong::CDlgThongtinPhong(CWnd* pParent /*=NULL*/)
	: CBitmapDialog(CDlgThongtinPhong::IDD, pParent)
{
	//{{AFX_DATA_INIT(CDlgThongtinPhong)
	m_daynha = _T("");
	m_succhua = 0;
	m_etenphong = _T("");
	//str_ngay = _T("");
	//}}AFX_DATA_INIT
}


void CDlgThongtinPhong::DoDataExchange(CDataExchange* pDX)
{
	CBitmapDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDlgThongtinPhong)
	DDX_Control(pDX, IDC_STATIC_NGAY, c_ngay);
	DDX_Control(pDX, IDC_STATICNGAYTHI, m_staticngaythi);
	DDX_Control(pDX, IDC_PTATCATIET, m_all);
	DDX_Control(pDX, IDC_PDANHSACHNGAYTHI, m_listngaythi);
	DDX_Text(pDX, IDC_EDAYNHA, m_daynha);
	DDX_Text(pDX, IDC_ESUCCHUA, m_succhua);
	DDX_Text(pDX, IDC_EPHONG, m_etenphong);
	//DDX_Text(pDX, IDC_STATIC_NGAY, str_ngay);
	//}}AFX_DATA_MAP
	DDX_LBString(pDX, IDC_PDANHSACHNGAYTHI, m_strSelection);
	for (int i=0;i<12;i++)
	{
		//DDX_Check(pDX,IDC_TIET1+i,m_tiet[i]);
		DDX_Control(pDX,IDC_PTIET1+i,m_ptiet[i]);
	}
}


BEGIN_MESSAGE_MAP(CDlgThongtinPhong, CBitmapDialog)
	//{{AFX_MSG_MAP(CDlgThongtinPhong)
	ON_LBN_SELCHANGE(IDC_PDANHSACHNGAYTHI, OnSelchangePdanhsachngaythi)
	ON_BN_CLICKED(IDC_BUTTON_DONGY, OnButtonDongy)
	//}}AFX_MSG_MAP
	ON_CONTROL_RANGE(BN_CLICKED,IDC_PTIET1,IDC_PTATCATIET,OnPTiet)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDlgThongtinPhong message handlers

BOOL CDlgThongtinPhong::OnInitDialog() 
{
	CBitmapDialog::OnInitDialog();
	SetBitmap(IDB_BITMAP1,BITMAP_TILE);
	
	CTime ngay;
	CTimeSpan tam=Xeplt.ngayketthuc-Xeplt.ngaybatdau;
	
	ngay=Xeplt.ngaybatdau;
	int songay=atoi(tam.Format("%D")) +1;
	CTime t1( 1999, 3, 19, 22, 15, 0 );
    CTime t2( 1999, 3, 20, 22, 15, 0 );
    CTimeSpan ts = t2 - t1;

	m_listngaythi.ResetContent ();
	for (int i=0;i<songay;i++)
	{	
		int idx=m_listngaythi.AddString (ngay.Format ("%d-%m-%Y"));
		m_listngaythi.SetItemData(idx,i+1);
		ngay+=ts;
	}
	
	this->m_etenphong =m_tenphong;
	for (i=0;i<Xeplt.gTongsoPhong;i++)
		if (Xeplt.Phonghoc[i].tenphong ==m_tenphong)
		{
			m_daynha=Xeplt.Phonghoc[i].daynha ;
			m_succhua=Xeplt.Phonghoc[i].succhua ;
			m_stt=i;
			break;
		}
	m_listngaythi.SetCurSel (0); 
//MISSING: hien thi 
	
	UpdateData(FALSE);
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CDlgThongtinPhong::OnPTiet(UINT nID) 
{	
	int idx=m_listngaythi.GetCurSel ();
	int index=nID-IDC_PTIET1;
	int i;
	if (nID==IDC_PTATCATIET)
	{
		if (m_all.GetCheck()==1) 
			for (i=0;i<12;i++)
			{
				m_ptiet[i].SetCheck(1);
				Xeplt.Phonghoc[m_stt].tiet[idx*12+i]=0; 
			}
		else
			for (i=0;i<12;i++)
			{
				m_ptiet[i].SetCheck(0);
				Xeplt.Phonghoc[m_stt].tiet[idx*12+i]=1; 
			}
	}
	else
	{
		if (m_ptiet[index].GetCheck ()==1)
		{
			Xeplt.Phonghoc[m_stt].tiet[idx*12+index]=0; 
		}
		else Xeplt.Phonghoc[m_stt].tiet[idx*12+index]=1; 
	}
	UpdateData(FALSE);
}

void CDlgThongtinPhong::OnSelchangePdanhsachngaythi() 
{
	UpdateData();
	int i,tam,dem=0;
	CRect rect;
	int idx=m_listngaythi.GetCurSel ();
	CString str="C�c ti�t c�a ng�y : "+m_strSelection;
	//UpdateData(FALSE);
	//m_staticngaythi.SetWindowText (str);	
	Invalidate();
	//c_ngay.GetClientRect(&rect);
	//InvalidateRect(&rect,false);
	c_ngay.SetWindowText(str);
	//c_ngay.UpdateWindow();
	if (idx==0)
	{
		for (i=0;i<12;i++)
			if (Xeplt.Phonghoc[m_stt].tiet[i]==0)
			{
				dem++;
				m_ptiet[i].SetCheck(1);
			}
			else m_ptiet[i].SetCheck(0);
		if (dem==12) m_all.SetCheck (1);
		else m_all.SetCheck (0);
	}
	else
	{
		for (i=idx*12;i<idx*12+12;i++)
		{	
			tam=i%12;
			if (Xeplt.Phonghoc[m_stt].tiet[i]==0)
			{
				dem++;
				m_ptiet[tam].SetCheck(1);
			}
			else m_ptiet[tam].SetCheck(0);
		}
		if (dem==12) m_all.SetCheck (1);
		else m_all.SetCheck (0);
	}

	UpdateData(FALSE);
	
}

void CDlgThongtinPhong::OnButtonDongy() 
{
	//Xeplt.Phonghoc	
	for (int i=0;i<Xeplt.gTongsoPhong;i++)
		if (Xeplt.Phonghoc[i].tenphong ==m_tenphong)
		{
			UpdateData(TRUE);			
			Xeplt.Phonghoc[i].succhua=m_succhua ;			
			break;
		}
		OnOK();
}
