// DlgNhapthoigian.cpp : implementation file
//

#include "stdafx.h"
#include "xeplichthi.h"
#include "DlgNhapthoigian.h"
#include "khaibao.h"
#include "DlgNhaptietban.h"
#include "Xeplt.h"

//extern CTime ngaybatdau,ngayketthuc;
//extern CTietthi Tietthi;
//extern int gTongsoTiet;
#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CDlgNhapthoigian dialog
extern CXeplt Xeplt;

CDlgNhapthoigian::CDlgNhapthoigian(CWnd* pParent /*=NULL*/)
	: CBitmapDialog(CDlgNhapthoigian::IDD, pParent)
{
	//{{AFX_DATA_INIT(CDlgNhapthoigian)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
//	tooltip = new CToolTipCtrl;
	m_BalloonToolTip= new CToolTipWnd;
}


void CDlgNhapthoigian::DoDataExchange(CDataExchange* pDX)
{
	CBitmapDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDlgNhapthoigian)
	DDX_Control(pDX, IDC_THUBAY, m_thubay);
	DDX_Control(pDX, IDC_CHUNHAT, m_chunhat);
	DDX_Control(pDX, IDC_ALLDAY, m_allday);
	DDX_Control(pDX, IDC_DANHSACHNGAYTHI, m_listngaythi);
	DDX_Control(pDX, IDC_DATETIMEPICKER2, m_ngayketthuc);
	DDX_Control(pDX, IDC_DATETIMEPICKER1, m_ngaybatdau);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CDlgNhapthoigian, CBitmapDialog)
	//{{AFX_MSG_MAP(CDlgNhapthoigian)
	ON_NOTIFY(DTN_DATETIMECHANGE, IDC_DATETIMEPICKER1, OnDatetimechangeDatetimepicker1)
	ON_NOTIFY(DTN_DATETIMECHANGE, IDC_DATETIMEPICKER2, OnDatetimechangeDatetimepicker2)
	ON_LBN_DBLCLK(IDC_DANHSACHNGAYTHI, OnDblclkDanhsachngaythi)
	ON_BN_CLICKED(IDC_ALLDAY, OnAllday)
	ON_BN_CLICKED(IDC_THUBAY, OnThubay)
	ON_BN_CLICKED(IDC_CHUNHAT, OnChunhat)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDlgNhapthoigian message handlers

void CDlgNhapthoigian::OnOK() 
{
	
	if (Xeplt.ngayketthuc-Xeplt.ngaybatdau<0)
		MessageBox("Ngay bat dau phai truoc ngay ket thuc");
	else
	{
		for (int u=0; u<Xeplt.gTongsoPhong; u++)
		{
			Xeplt.Phonghoc[u].tiet.SetSize(Xeplt.gTongsoTiet);
			for(int k=0;k<Xeplt.gTongsoTiet;k++)
				Xeplt.Phonghoc[u].tiet[k]=1;
		}
		CBitmapDialog::OnOK();
	}
}

BOOL CDlgNhapthoigian::OnInitDialog() 
{
	CBitmapDialog::OnInitDialog();
	SetBitmap(IDB_BITMAP1,BITMAP_TILE);
	CString  Thu[7];
	Thu[0]="Ch� Nh�t";
	Thu[1]="Th� Hai";
	Thu[2]="Th� Ba";
	Thu[3]="Th� T�";
	Thu[4]="Th� N�m";
	Thu[5]="Th� S�u";
	Thu[6]="Th� B�y";
	CTime ngay;//,batdau,ketthuc;
	m_ngaybatdau.SetTime(&Xeplt.ngaybatdau); 
	m_ngayketthuc.SetTime(&Xeplt.ngayketthuc);
	CTimeSpan tam=Xeplt.ngayketthuc-Xeplt.ngaybatdau;
	ngay=Xeplt.ngaybatdau;

	int songay=atoi(tam.Format("%D")) +1;
	CTime t1( 1999, 3, 19, 22, 15, 0 );
    CTime t2( 1999, 3, 20, 22, 15, 0 );
    CTimeSpan ts = t2 - t1;

	m_listngaythi.ResetContent ();
	for (int i=0;i<songay;i++)
	{	
		int thu= atoi(ngay.Format ("%w"));
		CString hienthi=ngay.Format ("%d-%m-%Y   ")+Thu[thu];
		int idx=m_listngaythi.AddString (ngay.Format (hienthi));
		m_listngaythi.SetItemData(idx,i+1);
		ngay+=ts;
	}
	m_allday.SetCheck(0);
	
	m_BalloonToolTip->SetWidth(60);
	m_BalloonToolTip->SetHeight(45);
	m_BalloonToolTip->Create(this);
	m_BalloonToolTip->AddTool(GetDlgItem(IDC_DANHSACHNGAYTHI),"Double Click", RGB(255,0,0));


	 
	
	/*
	//Khoi dong tiet
	UpdateData(TRUE);
	int songaythi, sobuoithi,sotietthi;
	
	m_ngaybatdau.GetTime(Xeplt.ngaybatdau);
	m_ngayketthuc.GetTime(Xeplt.ngayketthuc);
	
	CTimeSpan tmp=Xeplt.ngayketthuc-Xeplt.ngaybatdau;		
	songaythi=atoi(tmp.Format("%D")) +1;		
	sobuoithi=songaythi*2;
	sotietthi=songaythi*12;
	Xeplt.gTongsoTiet=sotietthi;
	Xeplt.Tietthi.SetSize(Xeplt.gTongsoTiet,-1);
	*/
	/*
	bool flag=true;
	
	for (i=0;i<sotietthi;i++)
	{
		Xeplt.Tietthi[i].ban = FALSE;
		if (flag) 
			Xeplt.Tietthi[i].buoisang=true;
		else	
			Xeplt.Tietthi[i].buoisang=false;
		if ((i +1)% 6 ==0) 
			flag=!(flag);
	}
		//Ket thuc phan khoi dong tiet
	*/
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}


void CDlgNhapthoigian::OnDatetimechangeDatetimepicker1(NMHDR* pNMHDR, LRESULT* pResult) 
{
	

	CTime ngay,batdau,ketthuc;
	m_ngaybatdau.GetTime(batdau);
	m_ngayketthuc.GetTime(ketthuc);	
	CTimeSpan tam=ketthuc-batdau;
	ngay=batdau;
	CString  Thu[7];
	Thu[0]="Ch� Nh�t";
	Thu[1]="Th� Hai";
	Thu[2]="Th� Ba";
	Thu[3]="Th� T�";
	Thu[4]="Th� N�m";
	Thu[5]="Th� S�u";
	Thu[6]="Th� B�y";	
	int songay=atoi(tam.Format("%D")) +1;
	CTime t1( 1999, 3, 19, 22, 15, 0 );
    CTime t2( 1999, 3, 20, 22, 15, 0 );
    CTimeSpan ts = t2 - t1;

	m_listngaythi.ResetContent ();
	for (int i=0;i<songay;i++)
	{	
		int thu= atoi(ngay.Format ("%w"));
		CString hienthi=ngay.Format ("%d-%m-%Y   ")+Thu[thu];	
		int idx=m_listngaythi.AddString (hienthi);
		m_listngaythi.SetItemData(idx,i+1);
		ngay+=ts;
	}
	m_allday.SetCheck(0);
	//Khoi dong tiet
	UpdateData(TRUE);
	int songaythi,sobuoithi,sotietthi;
	
	m_ngaybatdau.GetTime(Xeplt.ngaybatdau);
	m_ngayketthuc.GetTime(Xeplt.ngayketthuc);
	
	CTimeSpan tmp=Xeplt.ngayketthuc-Xeplt.ngaybatdau;		
	songaythi=atoi(tmp.Format("%D")) +1;	
	if(songaythi<0) 
	{
	}
	else
	{	
		sobuoithi=songaythi*2;
		sotietthi=songaythi*12;
		Xeplt.gTongsoTiet=sotietthi;
		Xeplt.Tietthi.SetSize(sotietthi,-1);

		bool flag=true;
	
		for (i=0;i<sotietthi;i++)
		{
			Xeplt.Tietthi[i].ban = FALSE;
			if (flag) 
				Xeplt.Tietthi[i].buoisang=true;
			else	
				Xeplt.Tietthi[i].buoisang=false;
			if ((i +1)% 6 ==0) 
				flag=!(flag);
		}
		//Ket thuc phan khoi dong tiet
	}
	m_chunhat.SetCheck(0);
	m_thubay.SetCheck(0);
	*pResult = 0;
}

void CDlgNhapthoigian::OnDatetimechangeDatetimepicker2(NMHDR* pNMHDR, LRESULT* pResult) 
{
//REMIND :2 bien nay co du khong?
	CTime ngay,batdau,ketthuc; 
	m_ngaybatdau.GetTime(batdau);
	m_ngayketthuc.GetTime(ketthuc);
	CTimeSpan tam=ketthuc-batdau;
	ngay=batdau;
	
	CString  Thu[7];
	
	Thu[0]="Ch� Nh�t";
	Thu[1]="Th� Hai";
	Thu[2]="Th� Ba";
	Thu[3]="Th� T�";
	Thu[4]="Th� N�m";
	Thu[5]="Th� S�u";
	Thu[6]="Th� B�y";
	int songay=atoi(tam.Format("%D")) +1;
	CTime t1( 1999, 3, 19, 22, 15, 0 );
    CTime t2( 1999, 3, 20, 22, 15, 0 );
    CTimeSpan ts = t2 - t1;

	m_listngaythi.ResetContent ();
	for (int i=0;i<songay;i++)
	{	
		int thu= atoi(ngay.Format ("%w"));
		CString hienthi=ngay.Format ("%d-%m-%Y   ")+Thu[thu];	
		int idx=m_listngaythi.AddString (hienthi);
		m_listngaythi.SetItemData(idx,i+1);
		ngay+=ts;
	}
	m_allday.SetCheck(0);
	//Khoi dong tiet
	UpdateData(TRUE);
	int  songaythi,sobuoithi,sotietthi;
	
	m_ngaybatdau.GetTime(Xeplt.ngaybatdau);
	m_ngayketthuc.GetTime(Xeplt.ngayketthuc);
	
	CTimeSpan tmp=Xeplt.ngayketthuc-Xeplt.ngaybatdau;		
	songaythi=atoi(tmp.Format("%D")) +1;
	
	if(songaythi<0) 
	{
	}
	else
	{
		sobuoithi=songaythi*2;
		sotietthi=songaythi*12;
		Xeplt.gTongsoTiet=sotietthi;
		Xeplt.Tietthi.SetSize(sotietthi,-1);

		bool flag=true;
	
		for (i=0;i<sotietthi;i++)
		{
			Xeplt.Tietthi[i].ban = FALSE;
			if (flag) 
				Xeplt.Tietthi[i].buoisang=true;
			else
				Xeplt.Tietthi[i].buoisang=false;
			if ((i +1)% 6 ==0) 
				flag=!(flag);
		}
		//Ket thuc phan khoi dong tiet
	}
	m_chunhat.SetCheck(0);
	m_thubay.SetCheck(0);
	*pResult = 0;
}

void CDlgNhapthoigian::OnDblclkDanhsachngaythi() 
{
	CDlgNhaptietban dlgTietban;
	int idx=m_listngaythi.GetCurSel ();
	
	dlgTietban.ngaythiban = m_listngaythi.GetItemData (idx);
	m_listngaythi.GetText(idx,dlgTietban.title );
	dlgTietban.title =dlgTietban.title.Left(10); 
	//dlgTietban.title
	dlgTietban.select=true;
	dlgTietban.DoModal ();
}

void CDlgNhapthoigian::OnAllday() 
{
	CDlgNhaptietban dlgTietban;
	if (m_allday.GetCheck ()==1)
	{
		dlgTietban.select =false;
		dlgTietban.DoModal ();
	}	
}


void CDlgNhapthoigian::OnThubay() 
{
	CTime ngay;
	int tietorder,i,j;
	CTimeSpan tam=Xeplt.ngayketthuc-Xeplt.ngaybatdau;
	ngay=Xeplt.ngaybatdau;
	int thu;
	
	int songay=atoi(tam.Format("%D")) +1;
	CTime t1( 1999, 3, 19, 22, 15, 0 );
    CTime t2( 1999, 3, 20, 22, 15, 0 );
    CTimeSpan ts = t2 - t1;

	for (i=0;i<songay;i++)
	{	
		thu= atoi(ngay.Format ("%w"));
		if (thu==6)
			if (m_thubay.GetCheck ()==1)
				for (j=0;j<12;j++)
					{
						tietorder=i*12+j;
						Xeplt.Tietthi[tietorder].ban=true;
					}
			else
				for (j=0;j<12;j++)
					{
						tietorder=i*12+j;
						Xeplt.Tietthi[tietorder].ban=false;
					}
				
		
		ngay+=ts;
	}
	
	
}

void CDlgNhapthoigian::OnChunhat() 
{
	CTime ngay;
	int tietorder,i,j;
	CTimeSpan tam=Xeplt.ngayketthuc-Xeplt.ngaybatdau;
	ngay=Xeplt.ngaybatdau;
	int thu;
	
	int songay=atoi(tam.Format("%D")) +1;
	CTime t1( 1999, 3, 19, 22, 15, 0 );
    CTime t2( 1999, 3, 20, 22, 15, 0 );
    CTimeSpan ts = t2 - t1;

	for (i=0;i<songay;i++)
	{	
		thu= atoi(ngay.Format ("%w"));
		if (thu==0)
			if (m_chunhat.GetCheck ()==1)
				for (j=0;j<12;j++)
					{
						tietorder=i*12+j;
						Xeplt.Tietthi[tietorder].ban=true;
					}
			else
				for (j=0;j<12;j++)
					{
						tietorder=i*12+j;
						Xeplt.Tietthi[tietorder].ban=false;
					}
		
		ngay+=ts;
	}
	
}



CDlgNhapthoigian::~CDlgNhapthoigian()
{
//	delete tooltip;
	delete m_BalloonToolTip;
}

BOOL CDlgNhapthoigian::PreTranslateMessage(MSG* pMsg) 
{
	if(m_BalloonToolTip) 
	m_BalloonToolTip->RelayEvent(pMsg); 
	
	return CBitmapDialog::PreTranslateMessage(pMsg);
}
