#if !defined(AFX_DLGCANHBAO_H__2099A8F7_23BF_4BD0_86AB_321522616261__INCLUDED_)
#define AFX_DLGCANHBAO_H__2099A8F7_23BF_4BD0_86AB_321522616261__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DlgCanhbao.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CDlgCanhbao dialog

class CDlgCanhbao : public CDialog
{
// Construction
public:
	CDlgCanhbao(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CDlgCanhbao)
	enum { IDD = IDD_DLGCANHBAO };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDlgCanhbao)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CDlgCanhbao)
		// NOTE: the ClassWizard will add member functions here
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DLGCANHBAO_H__2099A8F7_23BF_4BD0_86AB_321522616261__INCLUDED_)
