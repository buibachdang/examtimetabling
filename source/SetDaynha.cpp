// SetDaynha.cpp : implementation file
//

#include "stdafx.h"
#include "xeplichthi.h"
#include "SetDaynha.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

extern CString dgdan;
/////////////////////////////////////////////////////////////////////////////
// CSetDaynha

IMPLEMENT_DYNAMIC(CSetDaynha, CDaoRecordset)

CSetDaynha::CSetDaynha(CDaoDatabase* pdb)
	: CDaoRecordset(pdb)
{
	//{{AFX_FIELD_INIT(CSetDaynha)
	m_Daynha = _T("");
	m_Sophong = 0;
	m_Succhuaday = 0;
	m_nFields = 3;
	//}}AFX_FIELD_INIT
	m_nDefaultType = dbOpenDynaset;
}


CString CSetDaynha::GetDefaultDBName()
{
	return dgdan;
}

CString CSetDaynha::GetDefaultSQL()
{
	return _T("[Daynha]");
}

void CSetDaynha::DoFieldExchange(CDaoFieldExchange* pFX)
{
	//{{AFX_FIELD_MAP(CSetDaynha)
	pFX->SetFieldType(CDaoFieldExchange::outputColumn);
	DFX_Text(pFX, _T("[Daynha]"), m_Daynha);
	DFX_Short(pFX, _T("[Sophong]"), m_Sophong);
	DFX_Short(pFX, _T("[Succhuaday]"), m_Succhuaday);
	//}}AFX_FIELD_MAP
}

/////////////////////////////////////////////////////////////////////////////
// CSetDaynha diagnostics

#ifdef _DEBUG
void CSetDaynha::AssertValid() const
{
	CDaoRecordset::AssertValid();
}

void CSetDaynha::Dump(CDumpContext& dc) const
{
	CDaoRecordset::Dump(dc);
}
#endif //_DEBUG
