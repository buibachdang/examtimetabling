#if !defined(AFX_SETPHONG_H__8517C96F_A700_4F3C_872F_BBDF87E2D90A__INCLUDED_)
#define AFX_SETPHONG_H__8517C96F_A700_4F3C_872F_BBDF87E2D90A__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// SetPhong.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CSetPhong DAO recordset

class CSetPhong : public CDaoRecordset
{
public:
	CSetPhong(CDaoDatabase* pDatabase = NULL);
	DECLARE_DYNAMIC(CSetPhong)

// Field/Param Data
	//{{AFX_FIELD(CSetPhong, CDaoRecordset)
	CString	m_Maphong;
	CString	m_Daynha;
	short	m_Succhua;	
	//}}AFX_FIELD

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSetPhong)
	public:
	virtual CString GetDefaultDBName();		// Default database name
	virtual CString GetDefaultSQL();		// Default SQL for Recordset
	virtual void DoFieldExchange(CDaoFieldExchange* pFX);  // RFX support
	//}}AFX_VIRTUAL

// Implementation
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SETPHONG_H__8517C96F_A700_4F3C_872F_BBDF87E2D90A__INCLUDED_)
