#if !defined(AFX_DLGTIENDO_H__9AD7D4FA_7857_4C48_BECE_2FC175B0437C__INCLUDED_)
#define AFX_DLGTIENDO_H__9AD7D4FA_7857_4C48_BECE_2FC175B0437C__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DlgTiendo.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CDlgTiendo dialog
#include "BitmapDialog.h"
//#include "BDialog.h"
class CDlgTiendo : public CBitmapDialog
{
// Construction
public:
	CDlgTiendo(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CDlgTiendo)
	enum { IDD = IDD_DLGTIENDO };
	CStatic	c_Status;
	CStatic	c_T;
	CStatic	c_M;
	CStatic	c_Mon;
	CStatic	c_Dem;
	CProgressCtrl	c_Tiendo;
	CString	strMon;
	CString	strDem;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDlgTiendo)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CDlgTiendo)
	virtual BOOL OnInitDialog();
	afx_msg void OnChar(UINT nChar, UINT nRepCnt, UINT nFlags);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DLGTIENDO_H__9AD7D4FA_7857_4C48_BECE_2FC175B0437C__INCLUDED_)
