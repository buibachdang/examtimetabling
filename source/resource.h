//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by XepLichThi.rc
//
#define IDHAOPHI                        2
#define IDC_HAOPHI                      2
#define IDC_HAOPHI2                     3
#define IDD_ABOUTBOX                    100
#define IDD_XEPLICHTHI_FORM             101
#define IDR_MAINFRAME                   128
#define IDR_XEPLICTYPE                  129
#define IDD_DLGGIOITHIEU                130
#define IDD_DLGCHONMH                   131
#define IDD_DLGCHONTIETUT               132
#define IDD_DLGPHONGBAN                 133
#define IDD_DLGTHONGTINPHONG            134
#define IDD_DLGNHAPTHOIGIAN             135
#define IDD_DLGNHAPTIETBAN              136
#define IDD_DLGHIENTHIKQ                137
#define IDD_DLGCANHBAO                  138
#define IDB_BITMAP1                     140
#define IDB_BITMAP2                     147
#define IDB_BITMAP3                     148
#define IDB_XLT                         151
#define IDB_CHAYUP                      152
#define IDB_CHAYDOWN                    153
#define IDB_FORM                        154
#define IDD_DLGCACHCHAY                 155
#define IDD_DLGTIENDO                   156
#define IDB_PUSHUP                      157
#define IDB_PUSHDOWN                    158
#define IDB_BITMAP_DAYB6                160
#define IDB_BITMAP_GIOITHIEU2           161
#define IDC_BUTTON_DUONGDAN             1000
#define IDC_EDIT_DUONGDAN               1002
#define IDC_LIST_CHONKHOA               1003
#define IDC_STATIC_CHONKHOA             1004
#define IDC_BUTTON_KHOIDONG             1005
#define IDC_BUTTON_CHAY                 1006
#define IDC_LIST_MON1                   1006
#define IDC_LIST_MON2                   1007
#define IDC_BUTTON_RESET                1007
#define IDC_BUTTON_QUAPHAI              1008
#define IDC_BUTTON_QUATRAI              1009
#define IDC_BUTTON_CHITIET2             1010
#define IDC_BUTTON_CHITIET              1011
#define IDC_RADIO_NGAYTD                1012
#define IDC_RADIO_NGAYCD                1013
#define IDC_DATETIMEPICKER_NGAYBD       1015
#define IDC_DATETIMEPICKER_NGAYKT       1016
#define IDC_STATIC_NGAYKT               1017
#define IDC_STATIC_NGAYBD               1018
#define IDC_DATETIMEPICKER_NGAYCD       1019
#define IDC_STATIC_NGAYCD               1020
#define IDC_STATIC_TIETBD               1021
#define IDC_EDIT_TIETBATDAU             1022
#define IDC_STATIC_MAMON                1023
#define IDC_BUTTON_NHAPTIETBAN          1024
#define IDC_TREE                        1025
#define IDC_BUTTON_NHAPTIETBAN2         1025
#define IDC_BUTTON_PTCHAY               1025
#define IDC_EDIT_NGAYBATDAU             1027
#define IDC_EDIT_NGAYKETTHUC            1028
#define IDC_BUTTON_NHAPTHOIGIAN         1029
#define IDC_DATETIMEPICKER1             1030
#define IDC_DATETIMEPICKER2             1031
#define IDC_DANHSACHNGAYTHI             1032
#define IDC_TIET1                       1033
#define IDC_TIET2                       1034
#define IDC_TIET3                       1035
#define IDC_TIET4                       1036
#define IDC_TIET5                       1037
#define IDC_TIET6                       1038
#define IDC_TIET7                       1039
#define IDC_TIET8                       1040
#define IDC_TIET9                       1041
#define IDC_TIET10                      1042
#define IDC_TIET11                      1043
#define IDC_TIET12                      1044
#define IDC_TATCATIET                   1045
#define IDC_ALLDAY                      1046
#define IDC_EDAYNHA                     1047
#define IDC_EPHONG                      1048
#define IDC_ESUCCHUA                    1049
#define IDC_PDANHSACHNGAYTHI            1050
#define IDC_PTIET1                      1051
#define IDC_PTIET2                      1052
#define IDC_PTIET3                      1053
#define IDC_PTIET4                      1054
#define IDC_PTIET5                      1055
#define IDC_PTIET6                      1056
#define IDC_PTIET7                      1057
#define IDC_PTIET8                      1058
#define IDC_PTIET9                      1059
#define IDC_PTIET10                     1060
#define IDC_PTIET11                     1061
#define IDC_PTIET12                     1062
#define IDC_PTATCATIET                  1063
#define IDC_STATICNGAYTHI               1064
#define IDC_STATIC_CHON                 1065
#define IDC_CHECK1                      1067
#define IDC_THUBAY                      1067
#define IDC_CHECK_RANGBUOC              1067
#define IDC_CHECK_MOSTING               1067
#define IDC_CHECK2                      1068
#define IDC_LIST_KQ                     1068
#define IDC_CHUNHAT                     1068
#define IDC_CHECK_MOSTED                1068
#define IDC_CHECK_MOSTSTUDENT           1069
#define IDC_SLIDER1                     1072
#define IDC_SLIDER_MOSTED               1072
#define IDC_STATIC_NGAY                 1074
#define IDC_STATIC_CHONUT               1075
#define IDC_SLIDER_MOSTING              1078
#define IDC_SLIDER_MOSTSTUDENT          1079
#define IDC_BUTTON_MACDINH              1082
#define IDC_BESTFIT                     1083
#define IDC_FIRSTFIT                    1084
#define IDC_KHOANGCACH                  1085
#define IDC_EDIT1                       1086
#define IDC_PROGRESS                    1088
#define IDC_STATIC_MON                  1089
#define IDC_STATIC_DEM                  1090
#define IDC_STATIC_M                    1091
#define IDC_STATIC_T                    1092
#define IDC_BUTTON1                     1093
#define IDC_EDIT_KCTIET                 1094
#define IDC_SLIDER_KCTIET               1095
#define IDC_BUTTON_DONGY                1096
#define IDC_MSFLEXGRID1                 1097
#define IDC_EDIT_TGCHAY                 1099
#define IDC_STATIC_S                    1100
#define IDC_STATIC_A                    1101
#define IDC_STATIC_STATUS               1102

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_3D_CONTROLS                     1
#define _APS_NEXT_RESOURCE_VALUE        162
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1103
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
